<?php

declare(strict_types=1);

namespace spec\AppBundle\Factory;

use GuzzleHttp\Client;
use PhpSpec\ObjectBehavior;
use Symfony\Component\HttpFoundation\RequestStack;

final class ElviAppGuzzleClientFactorySpec extends ObjectBehavior
{
    function it_creates_an_guzzle_client_instance_for_elvi_app(): void
    {
        $this::create(new RequestStack(), 'http://elviapp.local', 'v1')->shouldBeAnInstanceOf(Client::class);
    }
}
