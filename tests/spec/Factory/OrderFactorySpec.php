<?php

declare(strict_types=1);

namespace spec\AppBundle\Factory;

use AppBundle\Entity\Order;
use AppBundle\Entity\ValueObject\OrderAppId;
use AppBundle\Entity\ValueObject\OrderUuid;
use AppBundle\Exception\OrderAlreadyExistsException;
use AppBundle\Validator\OrderUuidSpecificationInterface;
use ElviERP\Test\MotherObject\Entity\FulFillerMother;
use ElviERP\Test\MotherObject\Entity\ValueObject\OrderAppIdMother;
use PhpSpec\ObjectBehavior;

final class OrderFactorySpec extends ObjectBehavior
{
    function it_creates_an_order_when_order_does_not_exists_uuid(OrderUuidSpecificationInterface $specification): void
    {
        $uuid = new OrderUuid(self::ORDER_UUID);

        $specification->isSatisfiedBy($uuid)->willReturn(true);
        $this->beConstructedWith($specification);

        $fulFiller = FulFillerMother::create();
        $appId = OrderAppIdMother::create('12334');
        $completedAt = \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', '2018-12-12 10:00:00');

        $this
            ->create($uuid, $appId, $fulFiller, $completedAt)
            ->shouldBeAnInstanceOf(Order::class);
    }

    function it_throws_an_exception_when_order_with_uuid_exists(OrderUuidSpecificationInterface $specification): void
    {
        $uuid = new OrderUuid(self::ORDER_UUID);
        $orderAppId = new OrderAppId('12334');
        $fulFiller = FulFillerMother::create();
        $orderCheckoutCompletedAt = \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', '2018-12-12 10:00:00');

        $specification->isSatisfiedBy($uuid)->willReturn(false);
        $this->beConstructedWith($specification);

        $this
            ->shouldThrow(OrderAlreadyExistsException::class)
            ->during('create', [$uuid, $orderAppId, $fulFiller, $orderCheckoutCompletedAt]);
    }

    private const ORDER_UUID = 'f6c46b96-fd1e-11e8-9741-f2801f1b9fd1';
}
