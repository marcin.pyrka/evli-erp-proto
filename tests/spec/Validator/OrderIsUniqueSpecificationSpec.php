<?php

declare(strict_types=1);

namespace spec\AppBundle\Validator;

use AppBundle\Doctrine\ORM\Repository\OrderRepositoryInterface;
use ElviERP\Test\MotherObject\Entity\FulFillerMother;
use ElviERP\Test\MotherObject\Entity\OrderMother;
use ElviERP\Test\MotherObject\Entity\ValueObject\OrderUuidMother;
use PhpSpec\ObjectBehavior;

final class OrderIsUniqueSpecificationSpec extends ObjectBehavior
{
    function it_returns_true_when_order_does_not_exists_by_uuid(OrderRepositoryInterface $repository): void
    {
        $orderUuid = OrderUuidMother::create(self::ORDER_UUID);

        $repository->findOneByUuid($orderUuid)->willReturn(null);
        $this->beConstructedWith($repository);

        $this->isSatisfiedBy($orderUuid)->shouldBe(true);
    }

    function it_returns_false_when_order_exists_by_uuid(OrderRepositoryInterface $repository): void
    {
        $orderUuid = OrderUuidMother::create(self::ORDER_UUID);
        $fulFiller = FulFillerMother::create();
        $order = OrderMother::withAppId(self::ORDER_UUID, '123', $fulFiller, '2018-12-14 14:39:00');

        $repository->findOneByUuid($orderUuid)->willReturn($order);
        $this->beConstructedWith($repository);

        $this->isSatisfiedBy($orderUuid)->shouldBe(false);
    }

    private const ORDER_UUID = 'f6c46b96-fd1e-11e8-9741-f2801f1b9fd1';
}
