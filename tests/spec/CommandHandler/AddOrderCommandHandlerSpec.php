<?php

declare(strict_types=1);

namespace spec\AppBundle\CommandHandler;

use AppBundle\Command\AddOrderCommand;
use AppBundle\Factory\OrderFactoryInterface;
use AppBundle\Fixture\FulFillerFixture;
use AppBundle\Doctrine\ORM\Repository\FulFillerRepositoryInterface;
use AppBundle\Doctrine\ORM\Repository\OrderRepositoryInterface;
use ElviERP\Test\MotherObject\Entity\FulFillerMother;
use ElviERP\Test\MotherObject\Entity\OrderMother;
use ElviERP\Test\MotherObject\Entity\ValueObject\OrderAppIdMother;
use ElviERP\Test\MotherObject\Entity\ValueObject\OrderUuidMother;
use PhpSpec\ObjectBehavior;

final class AddOrderCommandHandlerSpec extends ObjectBehavior
{
    function it_saves_order_on_handle_command(
        OrderRepositoryInterface $orderRepository,
        OrderFactoryInterface $factory,
        FulFillerRepositoryInterface $fulFillerRepository
    ): void {
        $date = '2018-12-12 10:00:00';
        $appId = '12345';

        $orderAppId = OrderAppIdMother::create($appId);
        $orderUuid = OrderUuidMother::create(self::ORDER_UUID);
        $fulFiller = FulFillerMother::withIdTitle(self::FUL_FILLER_ID, 'LensVision');
        $completedAt = \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $date);

        $orderEntity = OrderMother::create($orderUuid, $orderAppId, $fulFiller, $completedAt);

        $factory->create($orderUuid, $orderAppId, $fulFiller, $completedAt)->willReturn($orderEntity);

        $fulFillerRepository->findOneById(self::FUL_FILLER_ID)->willReturn($fulFiller);

        $orderRepository->save($orderEntity)->shouldBeCalled();

        $this->beConstructedWith($orderRepository, $factory, $fulFillerRepository);

        $command = AddOrderCommand::create($appId, self::ORDER_UUID, self::FUL_FILLER_ID, $date);
        $this->__invoke($command);
    }

    function it_throws_an_exception_when_ful_filler_not_found(
        OrderRepositoryInterface $orderRepository,
        OrderFactoryInterface $factory,
        FulFillerRepositoryInterface $fulFillerRepository
    ): void {
        $date = '2018-12-12 10:00:00';
        $appId = '12345';

        $this->beConstructedWith($orderRepository, $factory, $fulFillerRepository);

        $command = AddOrderCommand::create($appId, self::ORDER_UUID, FulFillerFixture::DEFAULT_FUL_FILLER_ID, $date);
        $this->shouldThrow(\InvalidArgumentException::class)->during('__invoke', [$command]);
    }

    private const ORDER_UUID = 'f6c46b96-fd1e-11e8-9741-f2801f1b9fd1';
    private const FUL_FILLER_ID = 'f6c46b96-fd1e-11e8-9741-f2801f1b9fd2';
}
