<?php

declare(strict_types=1);

namespace spec\AppBundle\Transformer;

use AppBundle\DTO\InternalOrderVariant;
use AppBundle\Query\FindFulFillerProductOptionValueQueryInterface;
use AppBundle\Query\Model\FulFillerOrderProductVariantOption;
use PhpSpec\ObjectBehavior;

final class OrderProductOptionValueTransformerSpec extends ObjectBehavior
{
    function it_transforms_internal_option_representation_to_external_option_representation(
        FindFulFillerProductOptionValueQueryInterface $variantOptionQuery
    ): void {
        $variantOptionQuery
            ->__invoke(self::PRODUCT_CODE, self::OPTION_NAME, self::OPTION_VALUE)
            ->willReturn(
                [
                    'external_product_id'    => '841216051f490f2e090017d478e61539',
                    'product_title'          => 'Air Optix Plus HydraGlyde for Astigmatism - 1 Probelinse',
                    'fulfiller_option'       => 'sphere',
                    'fulfiller_option_value' => '-0.50',
                ]
            )
        ;

        $this->beConstructedWith($variantOptionQuery);

        $productData = $this->productData();

        $variant = InternalOrderVariant::fromProductData(
            $productData['variant']['product'],
            $productData['variant']['option']
        );

        $this->__invoke($variant)->shouldBeAnInstanceOf(FulFillerOrderProductVariantOption::class);
        $this->__invoke($variant)->jsonSerialize()->shouldBeArray();
    }

    private function productData(): array
    {
        return json_decode(
            file_get_contents(__DIR__ . '/../../Resources/fixtures/order/internal_order_variants.json'),
            true
        );
    }

    private const OPTION_NAME = 'sphere';

    private const OPTION_VALUE = '-0.50';

    private const PRODUCT_CODE = '21';
}
