<?php

declare(strict_types=1);

namespace spec\AppBundle\Service;

use AppBundle\Service\ElviAppHttpClientInterface;
use PhpSpec\ObjectBehavior;

final class GetOrderByOrderOrderAppIdHttpRequestSpec extends ObjectBehavior
{
    function it_returns_order_data_from_elvi_app_by_order_app_id(ElviAppHttpClientInterface $client): void
    {
        $client->get('order/22')->willReturn($this->readResponseFromFile());
        $this->beConstructedWith($client);

        $this->__invoke('22')->shouldBeArray();
    }

    function it_throws_an_exception_when_response_returns_no_json_data(ElviAppHttpClientInterface $client): void
    {
        $client->get('order/22')->willReturn('');
        $this->beConstructedWith($client);

        $this->shouldThrow(\InvalidArgumentException::class)->during('__invoke', ['22']);
    }

    function it_throws_an_exception_when_response_does_not_contains_results(ElviAppHttpClientInterface $client): void
    {
        $client->get('order/22')->willReturn(json_encode([]));
        $this->beConstructedWith($client);

        $this->shouldThrow(\InvalidArgumentException::class)->during('__invoke', ['22']);
    }

    private function readResponseFromFile(): string
    {
        return file_get_contents(__DIR__ . '/../../../tests/Resources/fixtures/order/order_get_app_response.json');
    }
}
