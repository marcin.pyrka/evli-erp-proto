<?php

declare(strict_types=1);

namespace spec\AppBundle\Service;

use GuzzleHttp\ClientInterface;
use PhpSpec\ObjectBehavior;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

final class ElviAppHttpClientSpec extends ObjectBehavior
{
    function it_returns_an_response_from_request_by_path(
        ClientInterface $client,
        ResponseInterface $response,
        StreamInterface $stream
    ): void {
        $stream->getContents()->willReturn(json_encode(['order_id' => '12345']));
        $response->getBody()->willReturn($stream);

        $client->request('GET', 'internal/order/22')->willReturn($response);

        $this->beConstructedWith($client);

        $this->get('order/22')->shouldBe('{"order_id":"12345"}');
    }
}
