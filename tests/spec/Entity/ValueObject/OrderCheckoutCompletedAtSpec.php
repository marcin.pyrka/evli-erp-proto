<?php
/*
 * This file has been created by developers from BitBag.
 * Feel free to contact us once you face any issues or want to start
 * another great project.
 * You can find more information about us on https://bitbag.shop and write us
 * an email on mikolaj.krol@bitbag.pl.
 */

declare(strict_types=1);

namespace spec\AppBundle\Entity\ValueObject;

use PhpSpec\ObjectBehavior;

final class OrderCheckoutCompletedAtSpec extends ObjectBehavior
{
    function it_returns_an_date_as_string_when_object_is_converted_to_string(): void
    {
        $this->beConstructedWith(new \DateTime('2018-12-11 07:47:00'));
        $this->__toString()->shouldBe('2018-12-11 07:47:00');
    }

    function it_creates_date_from_string(): void
    {
        $this->beConstructedThrough('fromDateAsString', ['2018-12-11 07:47:00']);
        $this->__toString()->shouldBe('2018-12-11 07:47:00');
    }
}
