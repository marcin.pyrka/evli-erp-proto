<?php

declare(strict_types=1);

namespace spec\AppBundle\Entity\ValueObject;

use AppBundle\Exception\FulFillerOrderIdException;
use PhpSpec\ObjectBehavior;

final class FulFillerOrderIdSpec extends ObjectBehavior
{
    function it_throws_an_exception_when_external_id_is_empty(): void
    {
        $this->beConstructedWith('');
        $this->shouldThrow(FulFillerOrderIdException::class)->duringInstantiation();
    }

    function it_returns_value_of_id_when_object_is_converted_to_string(): void
    {
        $this->beConstructedWith('ABCD123');
        $this->__toString()->shouldBe('ABCD123');
    }
}
