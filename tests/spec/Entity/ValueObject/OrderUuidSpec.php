<?php

declare(strict_types=1);

namespace spec\AppBundle\Entity\ValueObject;

use AppBundle\Exception\OrderTokenException;
use PhpSpec\ObjectBehavior;

final class OrderUuidSpec extends ObjectBehavior
{
    function it_throws_an_exception_when_token_is_empty(): void
    {
        $this->beConstructedWith('');
        $this->shouldThrow(OrderTokenException::class)->duringInstantiation();
    }

    function it_returns_token_value_when_object_is_converted_to_string(): void
    {
        $this->beConstructedWith(self::TOKEN_UUID);
        $this->__toString()->shouldBe(self::TOKEN_UUID);
    }

    function it_throws_an_exception_when_token_is_not_an_uuid_string(): void
    {
        $this->beConstructedWith('invalid-uuid-string');
        $this->shouldThrow(OrderTokenException::class)->duringInstantiation();
    }

    private const TOKEN_UUID = 'f6c46b96-fd1e-11e8-9741-f2801f1b9fd1';
}
