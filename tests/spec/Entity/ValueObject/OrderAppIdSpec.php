<?php

declare(strict_types=1);

namespace spec\AppBundle\Entity\ValueObject;

use AppBundle\Exception\OrderAppIdException;
use PhpSpec\ObjectBehavior;

final class OrderAppIdSpec extends ObjectBehavior
{
    function it_throws_an_exception_when_order_app_id_is_empty(): void
    {
        $this->beConstructedWith('');
        $this->shouldThrow(OrderAppIdException::class)->duringInstantiation();
    }

    function it_return_value_of_id_when_object_is_converted_to_string(): void
    {
        $this->beConstructedWith('1234');
        $this->__toString()->shouldBe('1234');
    }
}
