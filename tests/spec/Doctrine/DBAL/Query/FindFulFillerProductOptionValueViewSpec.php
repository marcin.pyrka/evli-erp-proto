<?php

declare(strict_types=1);

namespace spec\AppBundle\Doctrine\DBAL\Query;

use Doctrine\DBAL\Connection;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

final class FindFulFillerProductOptionValueViewSpec extends ObjectBehavior
{
    function it_throws_an_exception_when_product_and_options_not_found_by_id(Connection $connection): void
    {
        $connection->fetchAssoc(
            Argument::any(),
            ['productCode' => 'non_exist_product_code', 'optionName' => 'Power', 'optionValue' => '-2.00']
        )->willReturn(false);

        $this->beConstructedWith($connection);

        $this->shouldThrow(\InvalidArgumentException::class)
            ->during(
                '__invoke',
                ['non_exist_product_code', 'Power', '-2.00']
            )
        ;
    }
}
