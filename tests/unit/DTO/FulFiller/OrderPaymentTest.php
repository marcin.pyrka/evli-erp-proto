<?php

declare(strict_types=1);

namespace AppBundle\DTO\FulFiller;

use AppBundle\DTO\OrderPayment;
use AppBundle\DTO\OrderPaymentDetail;
use PHPUnit\Framework\TestCase;

final class OrderPaymentTest extends TestCase
{
    /** @test */
    public function it_serializes_object_to_json(): void
    {
        $paymentDetail = new OrderPaymentDetail(
            'elvi',
            'elvi',
            '010 0000+045002>'
        );

        $order = new OrderPayment('invoice', 'open', $paymentDetail);

        $expected = '{"option":"invoice","status":"open","detail":{"sender":"elvi","collector":"elvi","payment_id":"010 0000+045002>"}}';

        $this->assertSame($expected, json_encode($order));
    }
}
