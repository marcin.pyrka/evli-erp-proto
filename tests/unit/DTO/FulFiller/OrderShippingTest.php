<?php

declare(strict_types=1);

namespace AppBundle\DTO\FulFiller;

use AppBundle\DTO\OrderShipping;
use AppBundle\DTO\OrderShippingDetail;
use PHPUnit\Framework\TestCase;

final class OrderShippingTest extends TestCase
{
    /** @test */
    public function it_returns_order_shipping_when_object_is_serialize_to_json(): void
    {
        $shippingDetails = new OrderShippingDetail('996000296400207129');
        $shipping = new OrderShipping('swisspost', 'a-post', 'shipped', $shippingDetails);

        $jsonEncode = json_encode($shipping);

        $this->assertJson($jsonEncode);

        $this->assertSame(
            '{"carrier":"swisspost","option":"a-post","status":"shipped","detail":{"tracking":"996000296400207129"}}',
            $jsonEncode
        );
    }
}
