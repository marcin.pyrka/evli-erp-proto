<?php

declare(strict_types=1);

namespace ElviERP\Test\Integration\Doctrine\ORM\Repository;

use AppBundle\Doctrine\ORM\Repository\FulFillerRepository;
use AppBundle\Doctrine\ORM\Repository\FulFillerRepositoryInterface;
use AppBundle\Entity\Fulfiller\Fulfiller;
use ElviERP\Test\Integration\TestCase\DoctrineTestCase;
use ElviERP\Test\MotherObject\Entity\FulFillerMother;

final class FulFillerRepositoryTest extends DoctrineTestCase
{
    /** @test */
    public function it_saves_ful_filler(): void
    {
        $this->addFulFiller();

        $this->assertExists(Fulfiller::class, ['id' => self::DEFAULT_FUL_FILLER_ID]);
    }

    /** @test */
    public function it_returns_ful_filler_when_find_by_given_id(): void
    {
        $this->addFulFiller();

        $fulFiller = $this->repository->findOneById(self::DEFAULT_FUL_FILLER_ID);

        $this->assertNotNull($fulFiller);
    }

    /** @test */
    public function it_returns_null_when_ful_filler_not_found(): void
    {
        $nonExistsFulFillerId = 'c0c8a348-021b-11e9-8eb2-f2801f1b9fe2';

        $fulFiller = $this->repository->findOneById($nonExistsFulFillerId);

        $this->assertNull($fulFiller);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new FulFillerRepository($this->entityManager());
    }

    /** @var FulFillerRepositoryInterface */
    private $repository;
    private const DEFAULT_FUL_FILLER_ID = 'c0c8a348-021b-11e9-8eb2-f2801f1b9fd1';

    private function addFulFiller(): void
    {
        $this->repository->save(FulFillerMother::withIdTitle(self::DEFAULT_FUL_FILLER_ID, 'LentsVision'));
    }
}
