<?php

declare(strict_types=1);

namespace ElviERP\Test\Integration\Doctrine\ORM\Repository;

use AppBundle\Entity\Order;
use AppBundle\Fixture\FulFillerFixture;
use AppBundle\Doctrine\ORM\Repository\FulFillerRepository;
use AppBundle\Doctrine\ORM\Repository\FulFillerRepositoryInterface;
use AppBundle\Doctrine\ORM\Repository\OrderRepository;
use AppBundle\Doctrine\ORM\Repository\OrderRepositoryInterface;
use ElviERP\Test\Integration\TestCase\DoctrineTestCase;
use ElviERP\Test\MotherObject\Entity\OrderMother;
use ElviERP\Test\MotherObject\Entity\ValueObject\OrderUuidMother;

final class OrderRepositoryTest extends DoctrineTestCase
{
    /** @test */
    public function it_saves_order_with_app_id_without_ful_filler_id(): void
    {
        $this->saveOrder();

        $this->assertExists(Order::class, ['appId.value' => '1234']);
    }

    /** @test */
    public function it_returns_order_when_exists_by_order_uuid(): void
    {
        $this->saveOrder();

        $order = $this->orderRepository->findOneByUuid(OrderUuidMother::create(self::ORDER_UUID));

        $this->assertNotNull($order);
    }

    /** @test */
    public function it_returns_null_when_order_does_not_exists_by_uuid(): void
    {
        $order = $this->orderRepository->findOneByUuid(OrderUuidMother::create(self::ORDER_UUID));

        $this->assertNull($order);
    }

    private const ORDER_UUID = '1bd1793c-fde3-11e8-8eb2-f2801f1b9fd1';

    /** @var OrderRepositoryInterface */
    private $orderRepository;

    /** @var FulFillerRepositoryInterface */
    private $fulFillerRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->loadFixture();

        $this->orderRepository = new OrderRepository($this->entityManager());
        $this->fulFillerRepository = new FulFillerRepository($this->entityManager());
    }

    private function saveOrder(): void
    {
        $fulFiller = $this->fulFillerRepository->findOneById(FulFillerFixture::DEFAULT_FUL_FILLER_ID);

        $order = OrderMother::withAppId(
            self::ORDER_UUID,
            '1234',
            $fulFiller,
            '2018-12-12 09:00:00'
        );
        $this->orderRepository->save($order);
    }
}
