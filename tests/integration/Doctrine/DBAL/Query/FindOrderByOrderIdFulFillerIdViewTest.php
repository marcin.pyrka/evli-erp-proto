<?php

declare(strict_types=1);

namespace ElviERP\Test\Integration\Doctrine\DBAL\Query;

use AppBundle\Doctrine\DBAL\Query\FindOrderByOrderIdFulFillerIdView;
use AppBundle\Exception\OrderNotFoundException;
use AppBundle\Query\FindOrderByOrderIdFulFillerIdQueryInterface;
use ElviERP\Test\Integration\TestCase\DBALTestCase;

final class FindOrderByOrderIdFulFillerIdViewTest extends DBALTestCase
{
    /** @test */
    public function it_finds_order_by_order_id_and_ful_filler_id(): void
    {
        $order = $this->query->__invoke(self::ORDER_UUID, self::FUL_FILLER_ID);

        $this->assertSame(self::ORDER_APP_ID, $order->appId());
    }

    /** @test */
    public function it_throws_an_exception_when_order_not_found_by_order_id(): void
    {
        $this->expectException(OrderNotFoundException::class);

        $this->query->__invoke('non-exists-order-id', self::FUL_FILLER_ID);
    }

    /** @test */
    public function it_throws_an_exception_when_order_not_found_by_ful_filler_id(): void
    {
        $this->expectException(OrderNotFoundException::class);

        $this->query->__invoke(self::ORDER_UUID, 'non-exists-ful-filler-id');
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->loadFixture();

        $this->query = new FindOrderByOrderIdFulFillerIdView($this->connection());
    }

    /** @var FindOrderByOrderIdFulFillerIdQueryInterface */
    private $query;

    private const ORDER_APP_ID  = '78acd390-0fef-11e9-881a-0a0027000004';
    private const FUL_FILLER_ID = 'lensvision';
    private const ORDER_UUID    = 'f18b2c20-0389-11e9-8eb2-f2801f1b9fd1';
}
