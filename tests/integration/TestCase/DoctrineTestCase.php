<?php

declare(strict_types=1);

namespace ElviERP\Test\Integration\TestCase;

use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

abstract class DoctrineTestCase extends KernelTestCase
{
    use FixtureLoaderTestCase;

    /** @var EntityManager */
    private $entityManager;

    protected function setUp(): void
    {
        parent::setUp();
        parent::bootKernel();

        $this->entityManager = self::$container->get('doctrine.orm.entity_manager');
        $this->clearDb();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->clearDb();
        $this->entityManager->close();
        $this->entityManager = null;
    }

    private function clearDb(): void
    {
        $purge = new ORMPurger($this->entityManager);
        $purge->purge();
    }

    protected function entityManager(): EntityManager
    {
        return $this->entityManager;
    }

    protected function assertExists(string $entity, array $criteria): void
    {
        $actualEntity = $this->entityManager()->getRepository($entity)->findOneBy($criteria);
        $this->assertNotNull($actualEntity);
    }

    protected function assertNonExists(string $entity, array $criteria): void
    {
        $actualEntity = $this->entityManager()->getRepository($entity)->findOneBy($criteria);
        $this->assertNull($actualEntity);
    }
}
