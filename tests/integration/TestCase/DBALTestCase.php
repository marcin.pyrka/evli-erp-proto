<?php

declare(strict_types=1);

namespace ElviERP\Test\Integration\TestCase;

use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

abstract class DBALTestCase extends KernelTestCase
{
    use FixtureLoaderTestCase;

    /** @var Connection */
    private $connection;

    /** @var EntityManagerInterface */
    private $entityManager;

    protected function setUp(): void
    {
        parent::setUp();
        parent::bootKernel();

        $this->connection = self::$container->get(Connection::class);
        $this->entityManager = self::$container->get(EntityManagerInterface::class);

        $this->clearDb();
    }

    protected function connection(): Connection
    {
        return $this->connection;
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->clearDb();
        $this->connection->close();
    }

    private function clearDb(): void
    {
        $purge = new ORMPurger($this->entityManager);
        $purge->purge();
    }
}
