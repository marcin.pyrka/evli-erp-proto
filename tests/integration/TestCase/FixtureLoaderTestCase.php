<?php

declare(strict_types=1);

namespace ElviERP\Test\Integration\TestCase;

use Sylius\Bundle\FixturesBundle\Loader\SuiteLoaderInterface;
use Sylius\Bundle\FixturesBundle\Suite\SuiteRegistryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

trait FixtureLoaderTestCase
{
    /** @var ContainerInterface */
    protected static $container;

    protected function loadFixture(string $suitName = 'default'): void
    {
        $this->loadSuites($suitName);
    }

    private function loadSuites(string $suiteName): void
    {
        $suite = $this->getSuiteRegistry()->getSuite($suiteName);

        $this->getSuiteLoader()->load($suite);
    }

    private function getSuiteRegistry(): SuiteRegistryInterface
    {
        return self::$container->get('sylius_fixtures.suite_registry');
    }

    private function getSuiteLoader(): SuiteLoaderInterface
    {
        return self::$container->get('sylius_fixtures.suite_loader');
    }
}
