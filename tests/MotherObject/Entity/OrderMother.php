<?php

declare(strict_types=1);

namespace ElviERP\Test\MotherObject\Entity;

use AppBundle\Entity\Fulfiller\Fulfiller;
use AppBundle\Entity\Order;
use AppBundle\Entity\ValueObject\OrderAppId;
use AppBundle\Entity\ValueObject\OrderUuid;
use ElviERP\Test\MotherObject\Entity\ValueObject\OrderAppIdMother;
use ElviERP\Test\MotherObject\Entity\ValueObject\OrderUuidMother;

final class OrderMother
{
    public static function create(
        OrderUuid $orderUuid,
        OrderAppId $appId,
        FulFiller $fulFiller,
        \DateTimeImmutable $completedAt
    ): Order {
        return new Order($orderUuid, $appId, $fulFiller, $completedAt);
    }

    public static function withAppId(
        string $orderUuid,
        string $appId,
        FulFiller $fulFiller,
        string $completedAt
    ): Order {
        return self::create(
            OrderUuidMother::create($orderUuid),
            OrderAppIdMother::create($appId),
            $fulFiller,
            \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $completedAt)
        );
    }
}
