<?php

declare(strict_types=1);

namespace ElviERP\Test\MotherObject\Entity;

use AppBundle\Entity\Fulfiller\Fulfiller;

final class FulFillerMother
{
    public static function create(): Fulfiller
    {
        return new Fulfiller();
    }

    public static function withIdTitle(string $fulFillerId, string $title): Fulfiller
    {
        $entity = self::create();
        $entity->setId($fulFillerId);
        $entity->setTitle($title);
        $entity->setMatchedProductsCount(0);
        $entity->setTotalProductsCount(0);

        return $entity;
    }
}
