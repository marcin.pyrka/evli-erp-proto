<?php

declare(strict_types=1);

namespace ElviERP\Test\MotherObject\Entity\ValueObject;

use AppBundle\Entity\ValueObject\FulFillerId;

final class FulFillerIdMother
{
    public static function create(string $uuid): FulFillerId
    {
        return new FulFillerId($uuid);
    }
}
