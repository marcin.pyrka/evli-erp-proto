<?php

declare(strict_types=1);

namespace ElviERP\Test\MotherObject\Entity\ValueObject;

use AppBundle\Entity\ValueObject\OrderUuid;

final class OrderUuidMother
{
    public static function create(string $uuid): OrderUuid
    {
        return new OrderUuid($uuid);
    }
}
