<?php

declare(strict_types=1);

namespace ElviERP\Test\MotherObject\Entity\ValueObject;

use AppBundle\Entity\ValueObject\OrderAppId;

final class OrderAppIdMother
{
    public static function create(string $appId): OrderAppId
    {
        return new OrderAppId($appId);
    }
}
