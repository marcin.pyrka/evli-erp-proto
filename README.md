# TEST


# VM Setup

**Recommended setup**

Use instructions from [https://gitlab.com/elviapp/elvi-vm](https://gitlab.com/elviapp/elvi-vm)

This will run all manual commands from already for you.


# Manual Setup 

**Not recommended: Only run this if you want to configure your environment manually**


## Basic system requirements

Basic system requirements are described in Sylius official documentation 
page [here](https://docs.sylius.com/en/1.2/book/installation/requirements.html).

## Environment configuration Linux Debian/Ubuntu

Before everyone has an environment from the Docker, such packages/apps are needed to kickstart the application. Example commands for Debian/Ubuntu OS.

```bash
PACKAGES:
apt-get install curl
apt-get install git
apt-get install php7.2-fpm
apt-get install php7.2-xml
apt-get install php7.2-curl
apt-get install mysql-server
apt-get install php7.2-mysql
apt-get install php7.2-imagick
apt-get install php7.2-gd
apt-get install php7.2-intl
apt-get install php7.2-mbstring
apt-get install php7.2-soap
apt-get install php7.2-memcached

YARN:
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
apt-get update && apt-get install yarn

Packages if some errors from yarn suggest missing:
apt-get install python-dev
apt-get install build-essential
apt-get install npm

COMPOSER:
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php
php -r "unlink('composer-setup.php');"
mv composer.phar /usr/local/bin/composer

ELASTICSEARCH:
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.2.2.deb
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | tee -a /etc/apt/sources.list.d/elastic-6.x.list
apt-get update
apt-cache policy elasticsearch
apt-get install openjdk-8-jre apt-transport-https
apt-get install elasticsearch 
service elasticsearch start

```

## Installation

Make sure you configured your local SSH to work with GitLab. Read more in [the official documentation](https://docs.gitlab.com/ee/ssh/).

```bash
$ composer install
$ bin/cleanup 
```

### Cleaning up

Executing the `bin/cleanup` command cleans up the local environment, which includes following tasks:

* drop existing database
* create a new one
* reload schema info
* load CSV files

**Note: the cleanup file needs to have exec privileges. To grant them to this file, run `sudo chmod +x bin/cleanup` command**

### Data import

In order to migrate data from CSV, execute the below command:

```bash
$ bin/console elvi:import csv [resource_id] [path/to/your/csv/file.csv]
```

For instance, to import brands, execute below command:

```bash
$ bin/console elvi:import csv brand etc/csv/brands.csv
```

Under [`/etc/csv`](etc/csv) location, you will find CSV files for the data migration. For security reason,
**NEVER INCLUDE** sensitive user data in this folder or any place of the application.

To import CSV files run all of the below commands in the **exact order**:

```bash
$ bin/console elvi:import csv product_option etc/csv/product_options.csv
$ bin/console elvi:import csv product_option_value etc/csv/product_options_values.csv
$ bin/console elvi:import csv brand etc/csv/brands.csv
$ bin/console elvi:import csv product_family etc/csv/product_families.csv
$ bin/console elvi:import csv product_group etc/csv/product_groups.csv 
$ bin/console elvi:import csv product etc/csv/products.csv
$ bin/console elvi:import csv product_variant etc/csv/product_variants.csv
$ bin/console elvi:import csv product_variant_option_value etc/csv/product_variant_option_values.csv
$ bin/console elvi:import csv product_attribute etc/csv/product_attributes.csv
$ bin/console elvi:import csv product_attribute_value etc/csv/product_attribute_values.csv
$ bin/console elvi:import csv attribute_value etc/csv/attribute_values.csv
$ bin/console elvi:import csv taxon etc/csv/taxons.csv
```

Where first argument is the importer resource id and the second one is a CSV file.

### Currently supported importers:

- brand
- product_family
- product_group
- product
- product_variant
- product_variant_option_value
- product_option
- product_option_value
- product_attribute
- taxon

## Data export

In order to export resources, execute below command:

```bash
$ bin/console elvi:export csv
```

You can also export a single resource just by adding it's code to the command. For instance:

```bash
$ bin/console elvi:export csv brand
```

## Currently supported exporters:

- brand
- product_family
- product_group
- product_option
- product_option_value
- product_attribute
- product_attribute_value
- product
- product_variant
- product_variant_option_value
- product_variant_option_value_index
- option_to_product

### Files location

All exported CSV files will be saved under `var/csv` location.
