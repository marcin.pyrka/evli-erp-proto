<?php

declare(strict_types=1);

namespace ElviERP\Common\Collection;

abstract class AbstractCollection implements \Iterator, \Countable
{
    /** @var array */
    protected $elements = [];

    /** @var int */
    protected $key = 0;

    public function next(): void
    {
        ++$this->key;
    }

    public function key(): int
    {
        return $this->key;
    }

    public function valid(): bool
    {
        return isset($this->elements[$this->key()]);
    }

    public function rewind(): void
    {
        $this->key = 0;
    }

    public function count(): int
    {
        return count($this->elements);
    }

    /** @param object $element */
    protected function doAdd($element): void
    {
        $this->elements[] = $element;
    }

    /** @return object */
    protected function returnCurrent()
    {
        if (!$this->valid()) {
            throw new \RuntimeException('Current element not fount in collection!');
        }

        return $this->elements[$this->key()];
    }
}
