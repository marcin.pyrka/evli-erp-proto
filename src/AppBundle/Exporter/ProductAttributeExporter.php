<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use Elvi\ImportExportBundle\Exporter\AbstractExporter;
use Sylius\Component\Product\Model\ProductAttributeInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

final class ProductAttributeExporter extends AbstractExporter implements ProductAttributeExporterInterface
{
    /** @var RepositoryInterface */
    private $productAttributeRepository;

    /** @var array */
    private $availableLocales;

    public function __construct(RepositoryInterface $productAttributeRepository, array $availableLocales)
    {
        $this->productAttributeRepository = $productAttributeRepository;
        $this->availableLocales = $availableLocales;
    }

    public function export(ResourceInterface $productAttribute = null): array
    {
        $data = [];
        $data[] = $this->getColumns();

        if ($productAttribute instanceof ProductAttributeInterface) {
            $data[] = $this->exportRow($productAttribute);

            return $data;
        }

        /** @var ProductAttributeInterface $productAttribute */
        foreach ($this->productAttributeRepository->findAll() as $productAttribute) {
            $data[] = $this->exportRow($productAttribute);
        }

        return $data;
    }

    public function getResourceCode(): string
    {
        return 'product_attribute';
    }

    public function getColumns(): array
    {
        $columns = [
            self::CODE_COLUMN,
            self::STORAGE_TYPE_COLUMN,
            self::CONFIGURATION_COLUMN,
            self::CREATED_AT_COLUMN,
            self::UPDATED_AT_COLUMN,
            self::POSITION_COLUMN,
        ];

        foreach ($this->availableLocales as $locale) {
            $columns[] = $this->getTranslatableColumn(self::NAME_COLUMN, $locale);
        }

        return $columns;
    }

    private function exportRow(ProductAttributeInterface $productAttribute): array
    {
        $row = [];

        $row[self::CODE_COLUMN] = $productAttribute->getCode();
        $row[self::STORAGE_TYPE_COLUMN] = $productAttribute->getStorageType();
        $row[self::CONFIGURATION_COLUMN] = json_encode($productAttribute->getConfiguration());
        $row[self::CREATED_AT_COLUMN] = $productAttribute->getCreatedAt()->format('Y-m-d H:i:s');
        $row[self::UPDATED_AT_COLUMN] = $productAttribute->getCreatedAt()->format('Y-m-d H:i:s');
        $row[self::POSITION_COLUMN] = $productAttribute->getPosition();

        foreach ($this->availableLocales as $locale) {
            $productAttribute->setCurrentLocale($locale);
            $row[$this->getTranslatableColumn(self::NAME_COLUMN, $locale)] = $productAttribute->getName();
        }

        return $row;
    }
}
