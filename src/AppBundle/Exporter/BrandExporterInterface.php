<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use Elvi\ImportExportBundle\Exporter\ExporterInterface;

interface BrandExporterInterface extends ExporterInterface
{
    public const CODE_COLUMN = 'CODE';
    public const NAME_COLUMN = 'NAME';
}
