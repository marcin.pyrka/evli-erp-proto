<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use Elvi\ImportExportBundle\Exporter\ExporterInterface;

interface ProductGroupExporterInterface extends ExporterInterface
{
    public const CODE_COLUMN = 'CODE';
    public const NAME_COLUMN = 'NAME';
    public const PRODUCT_FAMILY_CODE = 'PRODUCT_FAMILY_CODE';
}
