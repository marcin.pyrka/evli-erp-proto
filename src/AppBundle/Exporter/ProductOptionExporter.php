<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use Elvi\ImportExportBundle\Exporter\AbstractExporter;
use Sylius\Component\Product\Model\ProductOptionInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

final class ProductOptionExporter extends AbstractExporter implements ProductOptionExporterInterface
{
    /** @var RepositoryInterface */
    private $productOptionRepository;

    /** @var array */
    private $availableLocales;

    public function __construct(RepositoryInterface $productOptionRepository, array $availableLocales)
    {
        $this->productOptionRepository = $productOptionRepository;
        $this->availableLocales = $availableLocales;
    }

    public function export(ResourceInterface $productOption = null): array
    {
        $data = [];
        $data[] = $this->getColumns();

        if ($productOption instanceof ProductOptionInterface) {
            $data[] = $this->exportRow($productOption);

            return $data;
        }

        /** @var ProductOptionInterface $productOption */
        foreach ($this->productOptionRepository->findAll() as $productOption) {
            $data[] = $this->exportRow($productOption);
        }

        return $data;
    }

    public function getResourceCode(): string
    {
        return 'product_option';
    }

    public function getColumns(): array
    {
        $columns = [
            self::CODE_COLUMN,
            self::POSITION_COLUMN,
            self::CREATED_AT_COLUMN,
            self::UPDATED_AT_COLUMN,
        ];

        foreach ($this->availableLocales as $locale) {
            $columns[] = $this->getTranslatableColumn(self::NAME_COLUMN, $locale);
        }

        return $columns;
    }

    private function exportRow(ProductOptionInterface $productOption): array
    {
        $row = [];

        $row[self::CODE_COLUMN] = $productOption->getCode();
        $row[self::POSITION_COLUMN] = $productOption->getPosition();
        $row[self::CREATED_AT_COLUMN] = $productOption->getCreatedAt()->format('Y-m-d H:i:s');
        $row[self::UPDATED_AT_COLUMN] = $productOption->getUpdatedAt()->format('Y-m-d H:i:s');

        foreach ($this->availableLocales as $locale) {
            $productOption->setCurrentLocale($locale);

            $row[$this->getTranslatableColumn(self::NAME_COLUMN, $locale)] = $productOption->getName();
        }

        return $row;
    }
}
