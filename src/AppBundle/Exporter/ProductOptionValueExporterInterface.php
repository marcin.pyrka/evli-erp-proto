<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use Elvi\ImportExportBundle\Exporter\ExporterInterface;

interface ProductOptionValueExporterInterface extends ExporterInterface
{
    public const CODE_COLUMN = 'CODE';
    public const OPTION_CODE_COLUMN = 'OPTION_CODE';
    public const OPTION_VALUE_COLUMN = 'VALUE__locale__';
}
