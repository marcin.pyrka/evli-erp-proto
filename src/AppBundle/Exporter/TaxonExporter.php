<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use Elvi\ImportExportBundle\Exporter\AbstractExporter;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Sylius\Component\Taxonomy\Model\TaxonInterface;

final class TaxonExporter extends AbstractExporter implements TaxonExporterInterface
{
    /** @var RepositoryInterface */
    private $taxonRepository;

    /** @var array */
    private $availableLocales;

    public function __construct(RepositoryInterface $taxonRepository, array $availableLocales)
    {
        $this->taxonRepository = $taxonRepository;
        $this->availableLocales = $availableLocales;
    }

    public function export(ResourceInterface $taxon = null): array
    {
        $data = [];
        $data[] = $this->getColumns();

        if ($taxon instanceof TaxonInterface) {
            $data[] = $this->exportRow($taxon);

            return $data;
        }

        /** @var TaxonInterface $taxon */
        foreach ($this->taxonRepository->findAll() as $taxon) {
            $data[] = $this->exportRow($taxon);
        }

        return $data;
    }

    public function getResourceCode(): string
    {
        return 'taxon';
    }

    public function getColumns(): array
    {
        $columns = [
            self::CODE_COLUMN,
            self::LEFT_COLUMN,
            self::RIGHT_COLUMN,
            self::TREE_LEVEL_COLUMN,
            self::POSITION_COLUMN,
        ];

        foreach ($this->availableLocales as $locale) {
            $columns[] = $this->getTranslatableColumn(self::NAME_COLUMN, $locale);
            $columns[] = $this->getTranslatableColumn(self::SLUG_COLUMN, $locale);
            $columns[] = $this->getTranslatableColumn(self::DESCRIPTION_COLUMN, $locale);
        }

        return $columns;
    }

    private function exportRow(TaxonInterface $taxon): array
    {
        $row = [];

        $row[self::CODE_COLUMN] = $taxon->getCode();
        $row[self::LEFT_COLUMN] = $taxon->getLeft();
        $row[self::RIGHT_COLUMN] = $taxon->getRight();
        $row[self::TREE_LEVEL_COLUMN] = $taxon->getLevel();
        $row[self::POSITION_COLUMN] = $taxon->getPosition();

        foreach ($this->availableLocales as $locale) {
            $taxon->setCurrentLocale($locale);

            $row[$this->getTranslatableColumn(self::NAME_COLUMN, $locale)] = $taxon->getName();
            $row[$this->getTranslatableColumn(self::SLUG_COLUMN, $locale)] = $taxon->getSlug();
            $row[$this->getTranslatableColumn(self::DESCRIPTION_COLUMN, $locale)] = $taxon->getDescription();
        }

        return $row;
    }
}
