<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use AppBundle\Entity\ProductAttributeValueInterface;
use Elvi\ImportExportBundle\Exporter\AbstractExporter;
use Sylius\Component\Attribute\AttributeType\TextAttributeType;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Webmozart\Assert\Assert;

final class ProductAttributeValueExporter extends AbstractExporter implements ProductAttributeValueExporterInterface
{
    /** @var RepositoryInterface */
    private $productAttributeValueRepository;

    /** @var RepositoryInterface */
    private $productAttributeRepository;

    /** @var RepositoryInterface */
    private $productRepository;

    public function __construct(
        RepositoryInterface $productAttributeValueRepository,
        RepositoryInterface $productAttributeRepository,
        RepositoryInterface $productRepository
    ) {
        $this->productAttributeValueRepository = $productAttributeValueRepository;
        $this->productAttributeRepository = $productAttributeRepository;
        $this->productRepository = $productRepository;
    }

    public function export(ResourceInterface $productAttributeValue = null): array
    {
        $data = [];
        $data[] = $this->getColumns();

        if ($productAttributeValue instanceof ProductAttributeValueInterface) {
            $data[] = $this->exportRow($productAttributeValue);

            return $data;
        }

        /** @var ProductAttributeValueInterface $productAttributeValue */
        foreach ($this->productAttributeValueRepository->findAll() as $productAttributeValue) {
            $data[] = $this->exportRow($productAttributeValue);
        }

        return $data;
    }

    public function getResourceCode(): string
    {
        return 'product_attribute_value';
    }

    public function getColumns(): array
    {
        return [
            self::CODE_COLUMN,
            self::PRODUCT_CODE_COLUMN,
            self::ATTRIBUTE_CODE_COLUMN,
            self::LOCALE_CODE_COLUMN,
            self::TEXT_VALUE_COLUMN,
        ];
    }

    private function exportRow(ProductAttributeValueInterface $productAttributeValue): array
    {
        $row = [];
        $product = $this->productRepository->findOneBy(['code' => $productAttributeValue->getProduct()->getCode()]);
        $productAttribute = $this->productAttributeRepository->findOneBy(['code' => $productAttributeValue->getAttribute()->getCode()]);

        $row[self::CODE_COLUMN] = $productAttributeValue->getCode();
        $row[self::PRODUCT_CODE_COLUMN] = $product->getCode();
        $row[self::ATTRIBUTE_CODE_COLUMN] = $productAttribute->getCode();
        $row[self::LOCALE_CODE_COLUMN] = $productAttributeValue->getLocaleCode();
        Assert::eq($productAttributeValue->getType(), TextAttributeType::TYPE);
        $row[self::TEXT_VALUE_COLUMN] = $productAttributeValue->getValue();

        return $row;
    }
}
