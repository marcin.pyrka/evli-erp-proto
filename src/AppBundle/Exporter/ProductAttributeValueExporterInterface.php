<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use Elvi\ImportExportBundle\Exporter\ExporterInterface;

interface ProductAttributeValueExporterInterface extends ExporterInterface
{
    public const CODE_COLUMN = 'CODE';
    public const PRODUCT_CODE_COLUMN = 'PRODUCT_CODE';
    public const ATTRIBUTE_CODE_COLUMN = 'ATTRIBUTE_CODE';
    public const LOCALE_CODE_COLUMN = 'LOCALE_CODE';
    public const TEXT_VALUE_COLUMN = 'TEXT_VALUE';
}
