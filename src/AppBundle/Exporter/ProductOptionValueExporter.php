<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use Elvi\ImportExportBundle\Exporter\AbstractExporter;
use Sylius\Component\Product\Model\ProductOptionValueInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Webmozart\Assert\Assert;

final class ProductOptionValueExporter extends AbstractExporter implements ProductOptionValueExporterInterface
{
    /** @var RepositoryInterface */
    private $productOptionValueRepository;

    /** @var RepositoryInterface */
    private $optionRepository;

    /** @var array */
    private $availableLocales;

    public function __construct(
        RepositoryInterface $productOptionValueRepository,
        RepositoryInterface $optionRepository,
        array $availableLocales
    ) {
        $this->productOptionValueRepository = $productOptionValueRepository;
        $this->optionRepository = $optionRepository;
        $this->availableLocales = $availableLocales;
    }

    public function export(ResourceInterface $productOptionValue = null): array
    {
        $data = [];
        $data[] = $this->getColumns();

        if ($productOptionValue instanceof ProductOptionValueInterface) {
            $data[] = $this->exportRow($productOptionValue);

            return $data;
        }

        /** @var ProductOptionValueInterface $productOptionValue */
        foreach ($this->productOptionValueRepository->findAll() as $productOptionValue) {
            $data[] = $this->exportRow($productOptionValue);
        }

        return $data;
    }

    public function getResourceCode(): string
    {
        return 'product_option_value';
    }

    public function getColumns(): array
    {
        $columns = [
            self::CODE_COLUMN,
            self::OPTION_CODE_COLUMN,
        ];

        foreach ($this->availableLocales as $locale) {
            $columns[] = $this->getTranslatableColumn(self::OPTION_VALUE_COLUMN, $locale);
        }

        return $columns;
    }

    private function exportRow(ProductOptionValueInterface $productOptionValue): array
    {
        $row = [];
        $option = $this->optionRepository->findOneBy(['code' => $productOptionValue->getOption()->getCode()]);
        Assert::notNull($option);

        $row[self::CODE_COLUMN] = $productOptionValue->getCode();
        $row[self::OPTION_CODE_COLUMN] = $option->getCode();

        foreach ($this->availableLocales as $locale) {
            $productOptionValue->setCurrentLocale($locale);

            $row[$this->getTranslatableColumn(self::OPTION_VALUE_COLUMN, $locale)] = $productOptionValue->getValue();
        }

        return $row;
    }
}
