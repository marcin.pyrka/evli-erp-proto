<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use Elvi\ImportExportBundle\Exporter\ExporterInterface;

interface ProductExporterInterface extends ExporterInterface
{
    public const CODE_COLUMN = 'CODE';
    public const PRODUCT_GROUP_ID_COLUMN = 'PRODUCT_GROUP_ID';
    public const CREATED_AT_COLUMN = 'CREATED_AT';
    public const UPDATED_AT_COLUMN = 'UPDATED_AT';
    public const ENABLED_COLUMN = 'ENABLED';
    public const TYPE_COLUMN = 'TYPE';
    public const PRICE_COLUMN = 'PRICE';
    public const ORIGINAL_PRICE_COLUMN = 'ORIGINAL_PRICE';
    public const QUANTITY_COLUMN = 'QUANTITY';
    public const UNIT_COLUMN = 'UNIT';
    public const SAMPLE_COLUMN = 'SAMPLE';
    public const NAME_COLUMN = 'NAME__locale__';
    public const SLUG_COLUMN = 'SLUG__locale__';
    public const META_KEYWORDS_COLUMN = 'META_KEYWORDS__locale__';
    public const META_DESCRIPTION_COLUMN = 'META_DESCRIPTION__locale__';
}
