<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use Elvi\ImportExportBundle\Exporter\ExporterInterface;

interface ProductVariantOptionValueIndexExporterInterface extends ExporterInterface
{
    public const PRODUCT_CODE_COLUMN = 'PRODUCT';
    public const VARIANT_CODE_COLUMN = 'VARIANT';
    public const POWER_COLUMN = 'POWER';
    public const AXIS_COLUMN = 'AXIS';
    public const RADIUS_COLUMN = 'RADIUS';
    public const CYLINDER_COLUMN = 'CYLINDER';
    public const DIAMETER_COLUMN = 'DIAMETER';
    public const ADD_COLUMN = 'ADD';
    public const COLOUR_COLUMN = 'COLOUR';
}
