<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use AppBundle\Entity\BrandInterface;
use Elvi\ImportExportBundle\Exporter\AbstractExporter;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

final class BrandExporter extends AbstractExporter implements BrandExporterInterface
{
    /** @var RepositoryInterface */
    private $brandRepository;

    public function __construct(RepositoryInterface $brandRepository)
    {
        $this->brandRepository = $brandRepository;
    }

    public function export(ResourceInterface $brand = null): array
    {
        $data = [];
        $data[] = $this->getColumns();

        if ($brand instanceof BrandInterface) {
            $data[] = $this->exportRow($brand);

            return $data;
        }

        /** @var BrandInterface $brand */
        foreach ($this->brandRepository->findAll() as $brand) {
            $data[] = $this->exportRow($brand);
        }

        return $data;
    }

    public function getResourceCode(): string
    {
        return 'brand';
    }

    public function getColumns(): array
    {
        return [
            self::CODE_COLUMN,
            self::NAME_COLUMN,
        ];
    }

    private function exportRow(BrandInterface $brand): array
    {
        $row = [];
        $row[self::CODE_COLUMN] = $brand->getCode();
        $row[self::NAME_COLUMN] = $brand->getName();

        return $row;
    }
}
