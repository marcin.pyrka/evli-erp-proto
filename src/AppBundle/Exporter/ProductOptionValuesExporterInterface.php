<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use Elvi\ImportExportBundle\Exporter\ExporterInterface;

interface ProductOptionValuesExporterInterface extends ExporterInterface
{
    public const PRODUCT_ID_COLUMN = 'PRODUCT_ID_COLUMN';
    public const PRODUCT_OPTION_VALUE_ID_COLUMN = 'PRODUCT_OPTION_VALUE_ID_COLUMN';
}
