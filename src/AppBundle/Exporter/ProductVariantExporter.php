<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use AppBundle\Entity\ProductVariantInterface;
use Elvi\ImportExportBundle\Exporter\AbstractExporter;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

final class ProductVariantExporter extends AbstractExporter implements ProductVariantExporterInterface
{
    /** @var RepositoryInterface */
    private $productVariantRepository;

    public function __construct(RepositoryInterface $productVariantRepository)
    {
        $this->productVariantRepository = $productVariantRepository;
    }

    public function export(ResourceInterface $productVariant = null): array
    {
        $data = [];
        $data[] = $this->getColumns();

        if ($productVariant instanceof ProductVariantInterface) {
            $data[] = $this->exportRow($productVariant);

            return $data;
        }

        /** @var ProductVariantInterface $productVariant */
        foreach ($this->productVariantRepository->findAll() as $productVariant) {
            $data[] = $this->exportRow($productVariant);
        }

        return $data;
    }

    public function getResourceCode(): string
    {
        return 'product_variant';
    }

    public function getColumns(): array
    {
        return [
            self::CODE_COLUMN,
            self::PRODUCT_CODE_COLUMN,
            self::CREATED_AT_COLUMN,
            self::UPDATED_AT_COLUMN,
            self::POSITION_COLUMN,
            self::EAN_COLUMN,
        ];
    }

    private function exportRow(ProductVariantInterface $productVariant): array
    {
        $row = [];

        $row[self::CODE_COLUMN] = $productVariant->getCode();
        $row[self::PRODUCT_CODE_COLUMN] = $productVariant->getProduct()->getCode();
        $row[self::CREATED_AT_COLUMN] = $productVariant->getCreatedAt()->format('Y-m-d H:i:s');
        $row[self::UPDATED_AT_COLUMN] = $productVariant->getCreatedAt()->format('Y-m-d H:i:s');
        $row[self::POSITION_COLUMN] = $productVariant->getPosition();
        $row[self::EAN_COLUMN] = $productVariant->getEan();

        return $row;
    }
}
