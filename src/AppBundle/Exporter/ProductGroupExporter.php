<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use AppBundle\Entity\ProductGroupInterface;
use Elvi\ImportExportBundle\Exporter\AbstractExporter;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

final class ProductGroupExporter extends AbstractExporter implements ProductGroupExporterInterface
{
    /** @var RepositoryInterface */
    private $productGroupRepository;

    public function __construct(RepositoryInterface $productGroupRepository)
    {
        $this->productGroupRepository = $productGroupRepository;
    }

    public function export(ResourceInterface $productGroup = null): array
    {
        $data = [];
        $data[] = $this->getColumns();

        if ($productGroup instanceof ProductGroupInterface) {
            $data[] = $this->exportRow($productGroup);

            return $data;
        }

        $productGroups = $this->productGroupRepository->findAll();
        if (count($productGroups)) {
            /** @var ProductGroupInterface $productGroup */
            foreach ($productGroups as $productGroup) {
                $data[] = $this->exportRow($productGroup);
            }
        }

        return $data;
    }

    public function getResourceCode(): string
    {
        return 'product_group';
    }

    public function getColumns(): array
    {
        return [
            self::CODE_COLUMN,
            self::NAME_COLUMN,
            self::PRODUCT_FAMILY_CODE,
        ];
    }

    private function exportRow(ProductGroupInterface $productGroup): array
    {
        $row = [];
        $row[self::CODE_COLUMN] = $productGroup->getCode();
        $row[self::NAME_COLUMN] = $productGroup->getName();

        if (null !== ($productFamily = $productGroup->getFamily())) {
            $row[self::PRODUCT_FAMILY_CODE] = $productGroup->getFamily()->getCode();
        }

        return $row;
    }
}
