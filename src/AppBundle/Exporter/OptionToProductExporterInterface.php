<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use Elvi\ImportExportBundle\Exporter\ExporterInterface;

interface OptionToProductExporterInterface extends ExporterInterface
{
    public const PRODUCT_CODE_COLUMN = 'product_code';
    public const OPTION_CODE_COLUMN = 'option_code';
}
