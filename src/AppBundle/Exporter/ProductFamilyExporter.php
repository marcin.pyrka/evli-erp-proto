<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use AppBundle\Entity\ProductFamilyInterface;
use Elvi\ImportExportBundle\Exporter\AbstractExporter;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

final class ProductFamilyExporter extends AbstractExporter implements ProductFamilyExporterInterface
{
    /** @var RepositoryInterface */
    private $productFamilyRepository;

    public function __construct(RepositoryInterface $productFamilyRepository)
    {
        $this->productFamilyRepository = $productFamilyRepository;
    }

    public function export(ResourceInterface $productFamily = null): array
    {
        $data = [];
        $data[] = $this->getColumns();

        if ($productFamily instanceof ProductFamilyInterface) {
            $data[] = $this->exportRow($productFamily);

            return $data;
        }

        /** @var ProductFamilyInterface $productFamily */
        foreach ($this->productFamilyRepository->findAll() as $productFamily) {
            $data[] = $this->exportRow($productFamily);
        }

        return $data;
    }

    public function getResourceCode(): string
    {
        return 'product_family';
    }

    public function getColumns(): array
    {
        return [
            self::CODE_COLUMN,
            self::NAME_COLUMN,
            self::BRAND_CODE_COLUMN,
        ];
    }

    private function exportRow(ProductFamilyInterface $productFamily): array
    {
        $row = [];

        $row[self::CODE_COLUMN] = $productFamily->getCode();
        $row[self::NAME_COLUMN] = $productFamily->getName();
        $row[self::BRAND_CODE_COLUMN] = $productFamily->getBrand()->getCode();

        return $row;
    }
}
