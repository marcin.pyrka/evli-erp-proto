<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use Elvi\ImportExportBundle\Exporter\ExporterInterface;

interface ProductFamilyExporterInterface extends ExporterInterface
{
    public const CODE_COLUMN = 'CODE';
    public const NAME_COLUMN = 'NAME';
    public const BRAND_CODE_COLUMN = 'BRAND_CODE';
}
