<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use Elvi\ImportExportBundle\Exporter\ExporterInterface;

interface TaxonExporterInterface extends ExporterInterface
{
    public const CODE_COLUMN = 'CODE';
    public const LEFT_COLUMN = 'LEFT';
    public const RIGHT_COLUMN = 'RIGHT';
    public const TREE_LEVEL_COLUMN = 'TREE_LEVEL';
    public const POSITION_COLUMN = 'POSITION';
    public const NAME_COLUMN = 'NAME__locale__';
    public const SLUG_COLUMN = 'SLUG__locale__';
    public const DESCRIPTION_COLUMN = 'DESCRIPTION__locale__';
}
