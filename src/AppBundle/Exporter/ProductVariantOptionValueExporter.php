<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use AppBundle\Entity\ProductVariantInterface;
use Elvi\ImportExportBundle\Exporter\AbstractExporter;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

final class ProductVariantOptionValueExporter extends AbstractExporter implements ProductVariantOptionValueExporterInterface
{
    /** @var RepositoryInterface */
    private $productVariantRepository;

    public function __construct(RepositoryInterface $productVariantRepository)
    {
        $this->productVariantRepository = $productVariantRepository;
    }

    public function export(ResourceInterface $resource = null): array
    {
        $data = [];
        $data[] = $this->getColumns();

        /** @var ProductVariantInterface $productVariant */
        foreach ($this->productVariantRepository->findAll() as $productVariant) {
            foreach ($productVariant->getOptionValues() as $productOptionValue) {
                $row = [];
                $row[self::VARIANT_CODE_COLUMN] = $productVariant->getCode();
                $row[self::OPTION_VALUE_CODE] = $productOptionValue->getCode();

                $data[] = $row;
            }
        }

        return $data;
    }

    public function getResourceCode(): string
    {
        return 'product_variant_option_value';
    }

    public function getColumns(): array
    {
        return [
            self::VARIANT_CODE_COLUMN,
            self::OPTION_VALUE_CODE,
        ];
    }
}
