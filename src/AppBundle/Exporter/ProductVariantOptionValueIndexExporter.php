<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use AppBundle\Entity\ProductVariantOptionValueInterface;
use Elvi\ImportExportBundle\Exporter\AbstractExporter;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

final class ProductVariantOptionValueIndexExporter extends AbstractExporter implements ProductVariantOptionValueIndexExporterInterface
{
    /** @var RepositoryInterface */
    private $productVariantOptionValueRepository;

    public function __construct(RepositoryInterface $productVariantOptionValueRepository)
    {
        $this->productVariantOptionValueRepository = $productVariantOptionValueRepository;
    }

    public function export(ResourceInterface $resource = null): array
    {
        $data = [];
        $data[] = $this->getColumns();

        /** @var ProductVariantOptionValueInterface $productVariantOptionValue */
        foreach ($this->productVariantOptionValueRepository->findAll() as $productVariantOptionValue) {
            $row = [];
//                @TODO: Maybe product redundant? All products have variant?
            $row[self::PRODUCT_CODE_COLUMN] = $productVariantOptionValue->getProduct()->getCode();
            $row[self::VARIANT_CODE_COLUMN] = $productVariantOptionValue->getVariant()->getCode();
            $row[self::POWER_COLUMN] = $productVariantOptionValue->getPower();
            $row[self::ADD_COLUMN] = $productVariantOptionValue->getAdd();
            $row[self::AXIS_COLUMN] = $productVariantOptionValue->getAxis();
            $row[self::CYLINDER_COLUMN] = $productVariantOptionValue->getCylinder();
            $row[self::DIAMETER_COLUMN] = $productVariantOptionValue->getDiameter();
            $row[self::RADIUS_COLUMN] = $productVariantOptionValue->getRadius();
            $row[self::COLOUR_COLUMN] = $productVariantOptionValue->getColour();

            $data[] = $row;
        }

        return $data;
    }

    public function getResourceCode(): string
    {
        return 'product_variant_option_value_index';
    }

    public function getColumns(): array
    {
        return [
            self::PRODUCT_CODE_COLUMN,
            self::VARIANT_CODE_COLUMN,
            self::POWER_COLUMN,
            self::ADD_COLUMN,
            self::AXIS_COLUMN,
            self::CYLINDER_COLUMN,
            self::DIAMETER_COLUMN,
            self::RADIUS_COLUMN,
            self::COLOUR_COLUMN,
        ];
    }
}
