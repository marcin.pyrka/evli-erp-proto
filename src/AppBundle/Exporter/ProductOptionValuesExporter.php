<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use AppBundle\Entity\ProductInterface;
use AppBundle\Doctrine\ORM\Repository\ProductRepositoryInterface;
use Elvi\ImportExportBundle\Exporter\AbstractExporter;
use Sylius\Component\Product\Model\ProductOptionValueInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

final class ProductOptionValuesExporter extends AbstractExporter implements ProductOptionValuesExporterInterface
{
    /** @var ProductRepositoryInterface */
    private $productRepository;

    /** @var RepositoryInterface */
    private $productOptionValueRepository;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        RepositoryInterface $productOptionValueRepository
    ) {
        $this->productRepository = $productRepository;
        $this->productOptionValueRepository = $productOptionValueRepository;
    }

    public function export(ResourceInterface $resource = null): array
    {
        $data = [];
        $data[] = $this->getColumns();

        /** @var ProductInterface $product */
        foreach ($this->productRepository->findAll() as $product) {
            /** @var ProductOptionValueInterface $productOptionValue */
            foreach ($product->getOptionValues() as $productOptionValue) {
                $data[] = $this->exportRow($product, $productOptionValue);
            }
        }

        return $data;
    }

    public function getResourceCode(): string
    {
        return 'product_option_values';
    }

    public function getColumns(): array
    {
        return [
            self::PRODUCT_ID_COLUMN,
            self::PRODUCT_OPTION_VALUE_ID_COLUMN,
        ];
    }

    private function exportRow(ProductInterface $product, ProductOptionValueInterface $productOptionValue): array
    {
        $row = [];
        $row[self::PRODUCT_ID_COLUMN] = $product->getId();
        $row[self::PRODUCT_OPTION_VALUE_ID_COLUMN] = $productOptionValue->getId();

        return $row;
    }
}
