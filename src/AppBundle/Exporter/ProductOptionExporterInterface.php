<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use Elvi\ImportExportBundle\Exporter\ExporterInterface;

interface ProductOptionExporterInterface extends ExporterInterface
{
    public const CODE_COLUMN = 'CODE';
    public const POSITION_COLUMN = 'POSITION';
    public const CREATED_AT_COLUMN = 'CREATED_AT';
    public const UPDATED_AT_COLUMN = 'UPDATED_AT';
    public const NAME_COLUMN = 'NAME__locale__';
}
