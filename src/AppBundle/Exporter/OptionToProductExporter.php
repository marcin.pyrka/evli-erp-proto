<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use AppBundle\Entity\ProductInterface;
use Elvi\ImportExportBundle\Exporter\AbstractExporter;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

final class OptionToProductExporter extends AbstractExporter implements OptionToProductExporterInterface
{
    /** @var RepositoryInterface */
    private $productRepository;

    public function __construct(RepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function export(ResourceInterface $resource = null): array
    {
        $data = [];
        $data[] = $this->getColumns();

        /** @var ProductInterface $product */
        foreach ($this->productRepository->findAll() as $product) {
            foreach ($product->getOptions() as $option) {
                $row = [];
                $row[self::PRODUCT_CODE_COLUMN] = $product->getCode();
                $row[self::OPTION_CODE_COLUMN] = $option->getCode();

                $data[] = $row;
            }
        }

        return $data;
    }

    public function getResourceCode(): string
    {
        return 'option_to_product';
    }

    public function getColumns(): array
    {
        return [
            self::PRODUCT_CODE_COLUMN,
            self::OPTION_CODE_COLUMN,
        ];
    }
}
