<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use AppBundle\Entity\ProductInterface;
use Elvi\ImportExportBundle\Exporter\AbstractExporter;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

final class ProductExporter extends AbstractExporter implements ProductExporterInterface
{
    /** @var RepositoryInterface */
    private $productRepository;

    /** @var array */
    private $availableLocales;

    public function __construct(RepositoryInterface $productRepository, array $availableLocales)
    {
        $this->productRepository = $productRepository;
        $this->availableLocales = $availableLocales;
    }

    public function export(ResourceInterface $product = null): array
    {
        $data = [];
        $data[] = $this->getColumns();

        if ($product instanceof ProductInterface) {
            $data[] = $this->exportRow($product);

            return $data;
        }

        /** @var ProductInterface $product */
        foreach ($this->productRepository->findAll() as $product) {
            $data[] = $this->exportRow($product);
        }

        return $data;
    }

    public function getResourceCode(): string
    {
        return 'product';
    }

    public function getColumns(): array
    {
        $columns = [
            self::CODE_COLUMN,
            self::PRODUCT_GROUP_ID_COLUMN,
            self::CREATED_AT_COLUMN,
            self::UPDATED_AT_COLUMN,
            self::ENABLED_COLUMN,
            self::TYPE_COLUMN,
            self::PRICE_COLUMN,
            self::ORIGINAL_PRICE_COLUMN,
            self::QUANTITY_COLUMN,
            self::UNIT_COLUMN,
            self::SAMPLE_COLUMN,
        ];

        foreach ($this->availableLocales as $locale) {
            $columns[] = $this->getTranslatableColumn(self::NAME_COLUMN, $locale);
            $columns[] = $this->getTranslatableColumn(self::SLUG_COLUMN, $locale);
            $columns[] = $this->getTranslatableColumn(self::META_KEYWORDS_COLUMN, $locale);
            $columns[] = $this->getTranslatableColumn(self::META_DESCRIPTION_COLUMN, $locale);
        }

        return $columns;
    }

    private function exportRow(ProductInterface $product): array
    {
        $row = [];

        $row[self::CODE_COLUMN] = $product->getCode();
        $row[self::PRODUCT_GROUP_ID_COLUMN] = $product->getGroup() ? $product->getGroup()->getCode() : '';
        $row[self::CREATED_AT_COLUMN] = $product->getCreatedAt()->format('Y-m-d H:i:s');
        $row[self::UPDATED_AT_COLUMN] = $product->getUpdatedAt()->format('Y-m-d H:i:s');
        $row[self::ENABLED_COLUMN] = (int) $product->isEnabled();
        $row[self::TYPE_COLUMN] = $product->getProductType();
        $row[self::PRICE_COLUMN] = $product->getPrice();
        $row[self::ORIGINAL_PRICE_COLUMN] = $product->getOriginalPrice();
        $row[self::QUANTITY_COLUMN] = $product->getQuantity();
        $row[self::UNIT_COLUMN] = $product->getUnit();
        $row[self::SAMPLE_COLUMN] = (int) $product->getTrial();

        foreach ($this->availableLocales as $locale) {
            $product->setCurrentLocale($locale);

            $row[$this->getTranslatableColumn(self::NAME_COLUMN, $locale)] = $product->getName();
            $row[$this->getTranslatableColumn(self::SLUG_COLUMN, $locale)] = $product->getSlug();
            $row[$this->getTranslatableColumn(self::META_KEYWORDS_COLUMN, $locale)] = $product->getMetaKeywords();
            $row[$this->getTranslatableColumn(self::META_DESCRIPTION_COLUMN, $locale)] = $product->getMetaDescription();
        }

        return $row;
    }
}
