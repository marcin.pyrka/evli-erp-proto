<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use Elvi\ImportExportBundle\Exporter\ExporterInterface;

interface ProductVariantExporterInterface extends ExporterInterface
{
    public const CODE_COLUMN = 'CODE';
    public const PRODUCT_CODE_COLUMN = 'PRODUCT_CODE';
    public const CREATED_AT_COLUMN = 'CREATED_AT';
    public const UPDATED_AT_COLUMN = 'UPDATED_AT';
    public const POSITION_COLUMN = 'POSITION';
    public const EAN_COLUMN = 'EAN';
    public const NAME_COLUMN = 'NAME__locale__';
}
