<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use Elvi\ImportExportBundle\Exporter\ExporterInterface;

interface ProductAttributeExporterInterface extends ExporterInterface
{
    public const CODE_COLUMN = 'CODE';
    public const STORAGE_TYPE_COLUMN = 'STORAGE_TYPE';
    public const CONFIGURATION_COLUMN = 'CONFIGURATION';
    public const CREATED_AT_COLUMN = 'CREATED_AT';
    public const UPDATED_AT_COLUMN = 'UPDATED_AT';
    public const POSITION_COLUMN = 'POSITION';
    public const NAME_COLUMN = 'NAME__locale__';
}
