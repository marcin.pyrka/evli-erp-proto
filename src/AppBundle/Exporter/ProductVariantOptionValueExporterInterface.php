<?php

declare(strict_types=1);

namespace AppBundle\Exporter;

use Elvi\ImportExportBundle\Exporter\ExporterInterface;

interface ProductVariantOptionValueExporterInterface extends ExporterInterface
{
    public const VARIANT_CODE_COLUMN = 'VARIANT_CODE';
    public const OPTION_VALUE_CODE = 'OPTION_VALUE';
}
