<?php

declare(strict_types=1);

namespace AppBundle\Validator;

use AppBundle\Doctrine\ORM\Repository\OrderRepositoryInterface;
use AppBundle\Entity\ValueObject\OrderUuid;

final class OrderIsUniqueSpecification implements OrderUuidSpecificationInterface
{
    /** @var OrderRepositoryInterface */
    private $repository;

    public function __construct(OrderRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function isSatisfiedBy(OrderUuid $orderUuid): bool
    {
        if (null === $this->repository->findOneByUuid($orderUuid)) {
            return true;
        }

        return false;
    }
}
