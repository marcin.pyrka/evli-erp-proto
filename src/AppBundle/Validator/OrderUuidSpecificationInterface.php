<?php

declare(strict_types=1);

namespace AppBundle\Validator;

use AppBundle\Entity\ValueObject\OrderUuid;

interface OrderUuidSpecificationInterface
{
    public function isSatisfiedBy(OrderUuid $orderUuid): bool;
}
