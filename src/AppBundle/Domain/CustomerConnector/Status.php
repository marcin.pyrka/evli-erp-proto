<?php declare(strict_types=1);

namespace AppBundle\Domain\CustomerConnector;

use MabeEnum\Enum;

/**
 * @method static Status TODO()
 * @method static Status PROGRESS()
 * @method static Status DONE()
 * @method static Status FAILED()
 */
class Status extends Enum
{
    public const TODO = 'todo';
    public const PROGRESS = 'progress';
    public const DONE = 'done';
    public const FAILED = 'failed';
}
