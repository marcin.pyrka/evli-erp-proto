<?php declare(strict_types=1);

namespace AppBundle\Domain\CustomerConnector;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\Application\CustomerConnector\Model\HttpResource;
use AppBundle\Domain\SharedKernel\Client;
use AppBundle\Infrastructure\Helper\Json;

class Request
{
    /** @var string */
    private $id;

    /** @var string */
    private $status;

    /** @var string */
    private $client;

    /** @var string */
    private $endpoint;

    /** @var string */
    private $urlParams;

    /** @var string */
    private $requestData;

    /** @var string */
    private $modifiedData;

    /** @var string */
    private $responseData;

    /** @var \DateTimeImmutable */
    private $createdAt;

    /**
     * @param string $id
     * @param Client $client
     * @param HttpResource $resource
     * @param \DateTimeImmutable $createdAt
     *
     * @return self
     */
    public static function create(
        string $id,
        Client $client,
        HttpResource $resource,
        \DateTimeImmutable $createdAt
    ): self {
        $self = new self();
        $self->id = $id;
        $self->client = $client;
        $self->endpoint = $resource->getEndpoint();
        $self->urlParams = Json::encode($resource->getUrlParams());
        $self->requestData = Json::encode($resource->getData());
        $self->modifiedData = '[]';
        $self->responseData = '[]';
        $self->status = Status::TODO();
        $self->createdAt = $createdAt;

        return $self;
    }

    /**
     * @return HttpEndpoint
     */
    public function getEndpoint(): HttpEndpoint
    {
        return HttpEndpoint::byName($this->endpoint);
    }

    /**
     * @return array
     */
    public function getRequestData(): array
    {
        return Json::decode($this->requestData);
    }

    /**
     * @param array $data
     */
    public function setModifiedData(array $data): void
    {
        $this->modifiedData = Json::encode($data);
    }

    /**
     * @param array $data
     */
    public function setResponseData(array $data): void
    {
        $this->responseData = Json::encode($data);
    }

    /**
     * @param Status $status
     */
    public function setStatus(Status $status): void
    {
        $this->status = $status;
    }

    /**
     * @return array
     */
    public function getUrlParams(): array
    {
        return Json::decode($this->urlParams);
    }

    private function __construct() { }
}
