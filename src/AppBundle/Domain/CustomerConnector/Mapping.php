<?php declare(strict_types=1);

namespace AppBundle\Domain\CustomerConnector;

use AppBundle\Domain\SharedKernel\Client;
use Ramsey\Uuid\UuidInterface;

class Mapping
{
    /** @var string */
    private $id;

    /** @var string */
    private $externalId;

    /** @var string */
    private $client;

    /** @var string */
    private $type;

    /**
     * @param UuidInterface $uuid
     * @param string $externalId
     * @param Client $client
     * @param MappingType $type
     *
     * @return self
     */
    public static function create(UuidInterface $uuid, string $externalId, Client $client, MappingType $type): self
    {
        $self = new self();
        $self->id = $uuid->toString();
        $self->externalId = $externalId;
        $self->client = $client;
        $self->type = $type;

        return $self;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getExternalId(): string
    {
        return $this->externalId;
    }

    private function __construct() { }
}
