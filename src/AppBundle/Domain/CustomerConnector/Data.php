<?php declare(strict_types=1);

namespace AppBundle\Domain\CustomerConnector;

class Data
{
    /** @var Mapping */
    private $mapping;

    /** @var Request */
    private $request;

    /**
     * @param Mapping $mapping
     * @param Request $request
     *
     * @return self
     */
    public static function create(Mapping $mapping, Request $request): self
    {
        $self = new self();
        $self->mapping = $mapping;
        $self->request = $request;

        return $self;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->request->getRequestData();
    }

    /**
     * @return string
     */
    public function getInternalId(): string
    {
        return $this->mapping->getId();
    }

    private function __construct() { }
}
