<?php declare(strict_types=1);

namespace AppBundle\Domain\CustomerConnector;

use MabeEnum\Enum;

/**
 * @method static MappingType CUSTOMER()
 * @method static MappingType NOTE()
 * @method static MappingType MEASUREMENT()
 */
class MappingType extends Enum
{
    public const CUSTOMER = 'customer';
    public const NOTE = 'note';
    public const MEASUREMENT = 'measurement';
}
