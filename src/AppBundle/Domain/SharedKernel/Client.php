<?php declare(strict_types=1);

namespace AppBundle\Domain\SharedKernel;

use MabeEnum\Enum;

/**
 * @method static Client WINOPTO()
 * @method static Client EYEWIN()
 * @method static Client ELVI()
 */
class Client extends Enum
{
    public const WINOPTO = 'winopto';
    public const EYEWIN = 'eyewin';
    public const ELVI = 'elvi';
}
