<?php declare(strict_types=1);

namespace AppBundle\Infrastructure\UseCase\ListenForException;

use Persist\PresenterBundle\Presenter\Presenter;

class ExceptionPresenter extends Presenter
{
    /**
     * {@inheritdoc}
     */
    protected function handleAction($object)
    {
        if ($object instanceof \Exception) {
            throw $object;
        }
    }
}
