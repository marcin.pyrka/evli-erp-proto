<?php declare(strict_types=1);

namespace AppBundle\Infrastructure\UseCase\ListenForException;

use Persist\LoggerBundle\Logger\ExceptionMessage;
use Persist\PresenterBundle\Controller\JsonPresenterTrait;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\EventListener\ExceptionListener as SymfonyExceptionListener;

class ExceptionListener
{
    use LoggerAwareTrait;
    use JsonPresenterTrait;

    /** @var SymfonyExceptionListener */
    private $exceptionListener;

    /** @var array */
    private $skipExceptions = [];

    /**
     * @param LoggerInterface $logger
     * @param SymfonyExceptionListener $listener
     * @param array $skipExceptions
     */
    public function __construct(LoggerInterface $logger, SymfonyExceptionListener $listener, array $skipExceptions)
    {
        $this->setLogger($logger);
        $this->exceptionListener = $listener;
        $this->skipExceptions = $skipExceptions;
    }

    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event): void
    {
        $exception = $event->getException();

        $this->exceptionListener->logKernelException($event);
        $this->shouldLogToSentry($exception) && $this->logger->alert(new ExceptionMessage($exception));

        $presenter = new ExceptionPresenter();
        $presenter->handle($exception);

        $event->setResponse($this->present($presenter));
    }

    /**
     * @param \Throwable $throwable
     *
     * @return bool
     */
    private function shouldLogToSentry(\Throwable $throwable): bool
    {
        foreach ($this->skipExceptions as $exception) {
            if ($throwable instanceof $exception) {
                return false;
            }
        }

        return true;
    }
}
