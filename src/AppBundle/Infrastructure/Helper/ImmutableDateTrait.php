<?php declare(strict_types=1);

namespace AppBundle\Infrastructure\Helper;

trait ImmutableDateTrait
{
    /** @var \DateTimeImmutable */
    protected $immutableDateTime;

    /**
     * @return \DateTimeImmutable
     */
    public function getImmutableDateTime(): \DateTimeImmutable
    {
        if ($this->immutableDateTime === null) {
            $this->immutableDateTime = new \DateTimeImmutable();
        }

        return $this->immutableDateTime;
    }

    /**
     * @param \DateTimeImmutable $immutableDateTime
     */
    public function setImmutableDateTime(\DateTimeImmutable $immutableDateTime): void
    {
        $this->immutableDateTime = $immutableDateTime;
    }
}
