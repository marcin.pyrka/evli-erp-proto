<?php declare(strict_types=1);

namespace AppBundle\Infrastructure\Helper;

class Json
{
    /**
     * @param string $input
     *
     * @return array
     */
    public static function decode(string $input): array
    {
        return is_array($response = json_decode($input, true)) ? $response : [$response];
    }

    /**
     * @param array $input
     * @param int $options
     *
     * @return string
     */
    public static function encode(array $input, int $options = JSON_UNESCAPED_UNICODE): string
    {
        return json_encode($input, $options);
    }
}
