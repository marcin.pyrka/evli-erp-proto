<?php declare(strict_types=1);

namespace AppBundle\Infrastructure\Auth;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class Auth implements EventSubscriberInterface
{
    /** @var string */
    private const KEY = '78d8b17e-025c-48b5-bb06-9ccba9ff6b8f';

    /** @var bool */
    private $canAuth;

    /**
     * @param bool $canAuth
     */
    public function __construct(bool $canAuth)
    {
        $this->canAuth = $canAuth;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [KernelEvents::REQUEST => ['onRequest']];
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onRequest(GetResponseEvent $event): void
    {
        if ($this->canAuth === true && $event->getRequest()->get('key') !== self::KEY) {
            throw new \Exception('Not authenticated');
        }
    }
}
