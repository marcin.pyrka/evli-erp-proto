<?php declare(strict_types=1);

namespace AppBundle\Infrastructure\Persistence\CustomerConnector;

use AppBundle\Domain\CustomerConnector\Request;

interface RequestRepositoryInterface
{
    /**
     * @param Request $request
     */
    public function store(Request $request): void;

    /**
     * @param Request $request
     */
    public function save(Request $request): void;

    /**
     * @param string $requestId
     *
     * @return Request
     * @throws RequestNotFoundException
     */
    public function get(string $requestId): Request;
}
