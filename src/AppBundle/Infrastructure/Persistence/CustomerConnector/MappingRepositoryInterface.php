<?php declare(strict_types=1);

namespace AppBundle\Infrastructure\Persistence\CustomerConnector;

use AppBundle\Domain\CustomerConnector\Mapping;
use AppBundle\Domain\CustomerConnector\MappingType;
use AppBundle\Domain\SharedKernel\Client;

interface MappingRepositoryInterface
{
    /**
     * @param Mapping $mapping
     */
    public function store(Mapping $mapping): void;

    /**
     * @param string $mappingId
     *
     * @return Mapping
     * @throws MappingNotFoundException
     */
    public function get(string $mappingId): Mapping;

    /**
     * @param Client $client
     * @param MappingType $type
     * @param string $externalId
     *
     * @return Mapping
     * @throws MappingNotFoundException
     */
    public function getByExternalId(Client $client, MappingType $type, string $externalId): Mapping;
}
