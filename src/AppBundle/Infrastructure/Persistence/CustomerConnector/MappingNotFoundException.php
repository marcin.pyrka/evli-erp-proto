<?php declare(strict_types=1);

namespace AppBundle\Infrastructure\Persistence\CustomerConnector;

class MappingNotFoundException extends \Exception
{
}
