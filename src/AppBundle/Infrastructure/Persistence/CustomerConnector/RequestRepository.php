<?php declare(strict_types=1);

namespace AppBundle\Infrastructure\Persistence\CustomerConnector;

use AppBundle\Domain\CustomerConnector\Request;
use AppBundle\Infrastructure\Helper\EntityManagerAwareTrait;
use Doctrine\ORM\EntityManagerInterface;

class RequestRepository implements RequestRepositoryInterface
{
    use EntityManagerAwareTrait;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->setEntityManager($entityManager);
    }

    /**
     * @inheritDoc
     */
    public function store(Request $request): void
    {
        $this->entityManager->persist($request);
        $this->entityManager->flush();
    }

    /**
     * @inheritDoc
     */
    public function save(Request $request): void
    {
        $this->entityManager->flush();
    }

    /**
     * @inheritDoc
     */
    public function get(string $requestId): Request
    {
        $entity = $this->entityManager->getRepository(Request::class)->find($requestId);

        if ( ! $entity instanceof Request) {
            throw new RequestNotFoundException();
        }

        return $entity;
    }
}
