<?php declare(strict_types=1);

namespace AppBundle\Infrastructure\Persistence\CustomerConnector;

use AppBundle\Domain\CustomerConnector\Mapping;
use AppBundle\Domain\CustomerConnector\MappingType;
use AppBundle\Domain\SharedKernel\Client;
use AppBundle\Infrastructure\Helper\EntityManagerAwareTrait;
use Doctrine\ORM\EntityManagerInterface;

class MappingRepository implements MappingRepositoryInterface
{
    use EntityManagerAwareTrait;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->setEntityManager($entityManager);
    }

    /**
     * @inheritDoc
     */
    public function store(Mapping $mapping): void
    {
        $this->entityManager->persist($mapping);
        $this->entityManager->flush();
    }

    /**
     * @inheritDoc
     */
    public function get(string $mappingId): Mapping
    {
        $entity = $this->entityManager->getRepository(Mapping::class)->find($mappingId);

        if ( ! $entity instanceof Mapping) {
            throw new MappingNotFoundException();
        }

        return $entity;
    }

    /**
     * @inheritDoc
     */
    public function getByExternalId(Client $client, MappingType $type, string $externalId): Mapping
    {
        $entity = $this->entityManager->getRepository(Mapping::class)->findOneBy([
            'externalId' => $externalId,
            'client' => $client,
            'type' => $type,
        ]);

        if ( ! $entity instanceof Mapping) {
            throw new MappingNotFoundException();
        }

        return $entity;
    }
}
