<?php

declare(strict_types=1);

namespace spec\AppBundle\Entity;

use AppBundle\Entity\ProductImage;
use AppBundle\Entity\ProductImageInterface;
use AppBundle\Entity\ProductInterface;
use PhpSpec\ObjectBehavior;
use Sylius\Component\Resource\Model\ResourceInterface;

final class ProductImageSpec extends ObjectBehavior
{
    function it_is_initializable(): void
    {
        $this->shouldHaveType(ProductImage::class);
    }

    function it_is_a_resource(): void
    {
        $this->shouldHaveType(ResourceInterface::class);
    }

    function it_implements_product_image_interface(): void
    {
        $this->shouldImplement(ProductImageInterface::class);
    }

    function it_has_no_id_by_default(): void
    {
        $this->getId()->shouldReturn(null);
    }

    function its_file_is_mutable(): void
    {
        $file = new \SplFileInfo(__FILE__);
        $this->setFile($file);
        $this->getFile()->shouldReturn($file);
    }

    function its_path_is_mutable(): void
    {
        $this->setPath(__FILE__);
        $this->getPath()->shouldReturn(__FILE__);
    }

    function its_product_is_mutable(ProductInterface $product): void
    {
        $this->setProduct($product);
        $this->getProduct()->shouldReturn($product);
    }

    function its_code_is_mutable(): void
    {
        $this->setCode('Tshirt');
        $this->getCode()->shouldReturn('Tshirt');
    }

    function it_adds_product_image(ProductInterface $product): void
    {
        $file = new \SplFileInfo(__FILE__);
        $this->setFile($file);
        $this->hasFile()->shouldReturn(true);

        $this->setPath(__FILE__);
        $this->hasPath()->shouldReturn(true);
    }
}
