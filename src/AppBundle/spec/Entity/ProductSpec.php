<?php

declare(strict_types=1);

namespace spec\AppBundle\Entity;

use AppBundle\Entity\Product;
use AppBundle\Entity\ProductImageInterface;
use AppBundle\Entity\ProductInterface;
use Doctrine\Common\Collections\Collection;
use PhpSpec\ObjectBehavior;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Taxonomy\Model\TaxonInterface;

final class ProductSpec extends ObjectBehavior
{
    function it_is_initializable(): void
    {
        $this->shouldHaveType(Product::class);
    }

    function it_is_a_resource(): void
    {
        $this->shouldHaveType(ResourceInterface::class);
    }

    function it_implements_product_interface(): void
    {
        $this->shouldImplement(ProductInterface::class);
    }

    function it_initializes_taxon_collection_by_default(): void
    {
        $this->getTaxons()->shouldHaveType(Collection::class);
    }

    function it_initializes_image_collection_by_default(): void
    {
        $this->getImages()->shouldHaveType(Collection::class);
    }

    function its_type_is_mutable(): void
    {
        $this->setType('Odziez');
        $this->getType()->shouldReturn('Odziez');
    }

    function it_adds_taxon(TaxonInterface $taxon): void
    {
        $this->addTaxon($taxon);
        $this->hasTaxon($taxon)->shouldReturn(true);
    }

    function it_adds_image(ProductImageInterface $image): void
    {
        $image->setProduct($this)->shouldBeCalled();

        $this->addImage($image);
        $this->hasImage($image)->shouldReturn(true);
    }

    function it_removes_image(ProductImageInterface $image): void
    {
        $image->setProduct($this)->shouldBeCalled();

        $this->addImage($image);
        $this->hasImage($image)->shouldReturn(true);

        $image->setProduct(null)->shouldBeCalled();

        $this->removeImage($image);
        $this->hasImage($image)->shouldReturn(false);
    }
}
