<?php

declare(strict_types=1);

namespace spec\AppBundle\Entity;

use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\ProductVariantInterface;
use PhpSpec\ObjectBehavior;
use Sylius\Component\Resource\Model\ResourceInterface;

final class ProductVariantSpec extends ObjectBehavior
{
    function it_is_initializable(): void
    {
        $this->shouldHaveType(ProductVariant::class);
    }

    function it_is_a_resource(): void
    {
        $this->shouldHaveType(ResourceInterface::class);
    }

    function it_should_implements_product_variant_interface(): void
    {
        $this->shouldImplement(ProductVariantInterface::class);
    }

    function its_ean_is_mutable(): void
    {
        $this->setEan('324856');
        $this->getEan()->shouldReturn('324856');
    }
}
