<?php

declare(strict_types=1);

namespace spec\AppBundle\Entity;

use AppBundle\Entity\ProductGroup;
use AppBundle\Entity\ProductGroupInterface;
use Elvi\SyliusProductComponent\Model\ProductFamilyInterface;
use Elvi\SyliusProductComponent\Model\ProductInterface;
use PhpSpec\ObjectBehavior;
use Sylius\Component\Resource\Model\ResourceInterface;

final class ProductGroupSpec extends ObjectBehavior
{
    function it_is_initializable(): void
    {
        $this->shouldHaveType(ProductGroup::class);
    }

    function it_is_a_resource(): void
    {
        $this->shouldHaveType(ResourceInterface::class);
    }

    function it_implements_product_group_interface(): void
    {
        $this->shouldImplement(ProductGroupInterface::class);
    }

    function it_has_no_id_by_default(): void
    {
        $this->getId()->shouldReturn(null);
    }

    function its_code_is_mutable(): void
    {
        $this->setCode('324856');
        $this->getCode()->shouldReturn('324856');
    }

    function its_name_is_mutable(): void
    {
        $this->setName('Adidas');
        $this->getName()->shouldReturn('Adidas');
    }

    function its_family_is_mutable(ProductFamilyInterface $family): void
    {
        $this->setFamily($family);
        $this->getFamily()->shouldReturn($family);
    }

    function it_adds_product(ProductInterface $product): void
    {
        $this->hasProduct($product)->shouldReturn(false);

        $this->addProduct($product);

        $this->hasProduct($product)->shouldReturn(true);
    }
}
