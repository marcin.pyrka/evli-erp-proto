<?php

declare(strict_types=1);

namespace spec\AppBundle\Entity;

use AppBundle\Entity\Brand;
use AppBundle\Entity\BrandInterface;
use PhpSpec\ObjectBehavior;
use Sylius\Component\Resource\Model\ResourceInterface;

final class BrandSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Brand::class);
    }

    function it_is_a_resource(): void
    {
        $this->shouldHaveType(ResourceInterface::class);
    }

    function it_implements_brand_interface(): void
    {
        $this->shouldImplement(BrandInterface::class);
    }

    function it_has_no_id_by_default(): void
    {
        $this->getId()->shouldReturn(null);
    }

    function its_code_is_mutable(): void
    {
        $this->setCode('324856');
        $this->getCode()->shouldReturn('324856');
    }

    function its_name_is_mutable(): void
    {
        $this->setName('Adidas');
        $this->getName()->shouldReturn('Adidas');
    }
}
