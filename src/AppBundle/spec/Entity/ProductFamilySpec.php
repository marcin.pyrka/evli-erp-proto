<?php

declare(strict_types=1);

namespace spec\AppBundle\Entity;

use AppBundle\Entity\ProductFamily;
use AppBundle\Entity\ProductFamilyInterface;
use Elvi\SyliusProductComponent\Model\BrandInterface;
use PhpSpec\ObjectBehavior;
use Sylius\Component\Resource\Model\ResourceInterface;

final class ProductFamilySpec extends ObjectBehavior
{
    function it_is_initializable(): void
    {
        $this->shouldHaveType(ProductFamily::class);
    }

    function it_is_a_resource(): void
    {
        $this->shouldHaveType(ResourceInterface::class);
    }

    function it_implements_product_family_interface(): void
    {
        $this->shouldImplement(ProductFamilyInterface::class);
    }

    function it_has_no_id_by_default(): void
    {
        $this->getId()->shouldReturn(null);
    }

    function its_code_is_mutable(): void
    {
        $this->setCode('324856');
        $this->getCode()->shouldReturn('324856');
    }

    function its_name_is_mutable(): void
    {
        $this->setName('Adidas');
        $this->getName()->shouldReturn('Adidas');
    }

    function its_brand_is_mutable(BrandInterface $brand): void
    {
        $this->setBrand($brand);
        $this->getBrand()->shouldReturn($brand);
    }
}
