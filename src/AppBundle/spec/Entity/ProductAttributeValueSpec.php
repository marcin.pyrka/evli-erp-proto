<?php

declare(strict_types=1);

namespace spec\AppBundle\Entity;

use AppBundle\Entity\ProductAttributeValue;
use Elvi\SyliusProductComponent\Model\ProductAttributeValueInterface;
use PhpSpec\ObjectBehavior;
use Sylius\Component\Resource\Model\ResourceInterface;

final class ProductAttributeValueSpec extends ObjectBehavior
{
    function it_is_initializable(): void
    {
        $this->shouldHaveType(ProductAttributeValue::class);
    }

    function it_is_a_resource(): void
    {
        $this->shouldHaveType(ResourceInterface::class);
    }

    function it_implements_product_aatribute_value_interface(): void
    {
        $this->shouldImplement(ProductAttributeValueInterface::class);
    }

    function it_has_no_id_by_default(): void
    {
        $this->getId()->shouldReturn(null);
    }

    function its_code_is_mutable(): void
    {
        $this->setCode('324856');
        $this->getCode()->shouldReturn('324856');
    }
}
