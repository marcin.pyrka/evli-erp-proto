<?php

declare(strict_types=1);

namespace spec\AppBundle\Importer;

use AppBundle\Importer\ProductAttributeImporter;
use AppBundle\Importer\ProductAttributeImporterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Elvi\ImportExportBundle\Resolver\ResourceResolverInterface;
use PhpSpec\ObjectBehavior;
use Sylius\Component\Product\Model\ProductAttributeInterface;

final class ProductAttributeImporterSpec extends ObjectBehavior
{
    function let(
        ResourceResolverInterface $productAttributeResourceResolver, EntityManagerInterface $entityManager
    ): void {
        $this->beConstructedWith(
            $productAttributeResourceResolver, $entityManager);
    }

    function it_is_initializable(): void
    {
        $this->shouldHaveType(ProductAttributeImporter::class);
    }

    function it_should_implements_product_attribute_value_importer_interface(): void
    {
        $this->shouldImplement(ProductAttributeImporterInterface::class);
    }

    function it_imports_product_attribute_value(
        ResourceResolverInterface $productAttributeResourceResolver,
        ProductAttributeInterface $productAttribute,
        EntityManagerInterface $entityManager
    ): void {
        $row = ['ID' => '2486', 'SORT' => '1', 'TITLE__LOCALE__' => 'Tshirt'];

        $productAttributeResourceResolver->resolveResource('2486')->willReturn($productAttribute);

        $productAttribute->setCode('2486')->shouldBeCalled();
        $productAttribute->setPosition(1)->shouldBeCalled();
        $productAttribute->setStorageType('text')->shouldBeCalled();

        $productAttribute->setCurrentLocale('_locale__')->shouldBeCalled();
        $productAttribute->setFallbackLocale('_locale__')->shouldBeCalled();
        $productAttribute->setName('Tshirt')->shouldBeCalled();

        $productAttribute->getId()->willReturn(null);
        $entityManager->persist($productAttribute)->shouldBeCalled();

        $this->import($row);
    }

    function it_returns_resource_code(): void
    {
        $this->getResourceCode()->shouldBeEqualTo('product_attribute');
    }
}
