<?php

declare(strict_types=1);

namespace spec\AppBundle\Importer;

use AppBundle\Importer\ProductOptionImporter;
use AppBundle\Importer\ProductOptionImporterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Elvi\ImportExportBundle\Resolver\ResourceResolverInterface;
use PhpSpec\ObjectBehavior;
use Sylius\Component\Product\Model\ProductOptionInterface;

final class ProductOptionImporterSpec extends ObjectBehavior
{
    function let(
        ResourceResolverInterface $productOptionResourceResolver,
        EntityManagerInterface $entityManager
    ): void {
        $this->beConstructedWith($productOptionResourceResolver, $entityManager);
    }

    function it_is_initializable(): void
    {
        $this->shouldHaveType(ProductOptionImporter::class);
    }

    function it_should_implements_product_option_importer_interface(): void
    {
        $this->shouldImplement(ProductOptionImporterInterface::class);
    }

    function it_imports_product_option(
        ProductOptionInterface $productOption,
        ResourceResolverInterface $productOptionResourceResolver,
        EntityManagerInterface $entityManager
    ): void {
        $row = ['ID' => '2486', 'SORT' => '1', 'TITLE__LOCALE__' => 'TEST'];

        $productOptionResourceResolver->resolveResource('2486')->willReturn($productOption);

        $productOption->setCode('2486')->shouldBeCalled();
        $productOption->setPosition(1)->shouldBeCalled();

        $productOption->setCurrentLocale('_locale__')->shouldBeCalled();
        $productOption->setName('TEST')->shouldBeCalled();

        $productOption->getId()->willReturn(null);
        $entityManager->persist($productOption)->shouldBeCalled();

        $this->import($row);
    }

    function it_returns_resource_code(): void
    {
        $this->getResourceCode()->shouldBeEqualTo('product_option');
    }
}
