<?php

declare(strict_types=1);

namespace spec\AppBundle\Importer;

use AppBundle\Entity\ProductAttributeValueInterface;
use AppBundle\Entity\ProductInterface;
use AppBundle\Importer\ProductAttributeValueImporter;
use AppBundle\Importer\ProductAttributeValueImporterInterface;
use AppBundle\Doctrine\ORM\Repository\ProductRepositoryInterface;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Elvi\ImportExportBundle\Resolver\ResourceResolverInterface;
use PhpSpec\ObjectBehavior;
use Sylius\Component\Locale\Context\LocaleContextInterface;
use Sylius\Component\Product\Model\ProductAttributeInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

final class ProductAttributeValueImporterSpec extends ObjectBehavior
{
    function let(
        ResourceResolverInterface $productAttributeValueResourceResolver,
        ProductRepositoryInterface $productRepository,
        RepositoryInterface $productAttributeRepository,
        LocaleContextInterface $localeContext,
        EntityManagerInterface $entityManager
    ): void {
        $this->beConstructedWith(
            $productAttributeValueResourceResolver,
            $productRepository,
            $productAttributeRepository,
            $localeContext,
            $entityManager);
    }

    function it_is_initializable(): void
    {
        $this->shouldHaveType(ProductAttributeValueImporter::class);
    }

    function it_should_implements_product_attribute_value_importer_interface(): void
    {
        $this->shouldImplement(ProductAttributeValueImporterInterface::class);
    }

    function it_cannot_import_with_null_product(
        ResourceResolverInterface $productAttributeValueResourceResolver,
        ProductAttributeValueInterface $productAttributeValue,
        ProductAttributeInterface $productAttribute,
        ProductRepositoryInterface $productRepository,
        ObjectRepository $productAttributeRepository
    ): void {
        $row = ['ATTRIBUTE_ID' => '2486', 'PRODUCT_PARENT_ID' => '1'];

        $productAttributeValueResourceResolver->resolveResource('2486')->willReturn($productAttributeValue);

        $productRepository->findOneBy(['code' => '1'])->willReturn(null);

        $productAttributeRepository->findOneBy([])->willReturn($productAttribute);

        $this->import($row);
    }

    function it_cannot_import_with_null_product_attribute(
        ResourceResolverInterface $productAttributeValueResourceResolver,
        ProductAttributeValueInterface $productAttributeValue,
        ProductInterface $product,
        ProductRepositoryInterface $productRepository,
        ObjectRepository $productAttributeRepository
    ): void {
        $row = ['ATTRIBUTE_ID' => '2486', 'PRODUCT_PARENT_ID' => '1'];

        $productAttributeValueResourceResolver->resolveResource('2486')->willReturn($productAttributeValue);

        $productRepository->findOneBy(['code' => '1'])->willReturn($product);

        $productAttributeRepository->findOneBy([])->willReturn(null);

        $this->import($row);
    }

    function it_imports_product_attribute_value(
         ResourceResolverInterface $productAttributeValueResourceResolver,
         ProductAttributeValueInterface $productAttributeValue,
         EntityManagerInterface $entityManager,
         ProductRepositoryInterface $productRepository,
         ProductAttributeInterface $productAttribute,
         ProductInterface $product,
         ObjectRepository $productAttributeRepository,
         LocaleContextInterface $localeContext
     ): void {
        $row = ['ATTRIBUTE_ID' => '2486', 'PRODUCT_PARENT_ID' => '1'];

        $productAttributeValueResourceResolver->resolveResource('2486')->willReturn($productAttributeValue);

        $productRepository->findOneBy(['code' => '1'])->willReturn($product);

        $productAttributeRepository->findOneBy([])->willReturn($productAttribute);

        $productAttributeValue->setCode('2486')->shouldBeCalled();
        $productAttributeValue->setProduct($product)->shouldBeCalled();

        $localeContext->getLocaleCode()->willReturn('TEST');
        $productAttributeValue->setLocaleCode('TEST')->shouldBeCalled();
        $productAttributeValue->setAttribute($productAttribute)->shouldBeCalled();

        $productAttributeValue->getId()->willReturn(null);
        $entityManager->persist($productAttributeValue)->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();

        $this->import($row);
    }

    function it_returns_resource_code(): void
    {
        $this->getResourceCode()->shouldBeEqualTo('product_attribute_value');
    }
}
