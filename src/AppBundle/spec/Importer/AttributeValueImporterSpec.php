<?php

declare(strict_types=1);

namespace spec\AppBundle\Importer;

use AppBundle\Entity\ProductAttributeValueInterface;
use AppBundle\Importer\AttributeValueImporter;
use AppBundle\Importer\AttributeValueImporterInterface;
use Doctrine\ORM\EntityManagerInterface;
use PhpSpec\ObjectBehavior;
use Sylius\Component\Product\Model\ProductAttributeInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

final class AttributeValueImporterSpec extends ObjectBehavior
{
    function let(
        RepositoryInterface $productAttributeRepository,
        EntityManagerInterface $entityManager
    ): void {
        $this->beConstructedWith($productAttributeRepository, $entityManager);
    }

    function it_is_initializable(
    ): void {
        $this->shouldHaveType(AttributeValueImporter::class);
    }

    function it_should_implements_attribute_value_importer_interface(): void
    {
        $this->shouldImplement(AttributeValueImporterInterface::class);
    }

    function it_cannot_import_without_product_atribute(
        RepositoryInterface $productAttributeRepository,
        ProductAttributeValueInterface $productAttributeValue
    ): void {
        $row = ['ID' => '2486', 'KEY_ID' => '1', 'TEXT__LOCALE__' => 'Tshirt'];

        $productAttributeRepository->findOneBy(['code' => '2486'])->willReturn($productAttributeValue);
        $productAttributeRepository->findOneBy(['code' => '1'])->willReturn(null);

        $this->import($row)->shouldReturn(null);
    }

    function it_cannot_import_without_product_atribute_value(
        RepositoryInterface $productAttributeRepository,
        ProductAttributeInterface $productAttribute
    ): void {
        $row = ['ID' => '2486', 'KEY_ID' => '1', 'TEXT__LOCALE__' => 'Tshirt'];

        $productAttributeRepository->findOneBy(['code' => '2486'])->willReturn(null);
        $productAttributeRepository->findOneBy(['code' => '1'])->willReturn($productAttribute);

        $this->import($row)->shouldReturn(null);
    }

    function it_imports_attribute_value(
        RepositoryInterface $productAttributeRepository,
        EntityManagerInterface $entityManager,
        ProductAttributeValueInterface $productAttributeValue,
        ProductAttributeInterface $productAttribute
    ): void {
        $row = ['ID' => '2486', 'KEY_ID' => '1', 'TEXT__LOCALE__' => 'Tshirt'];

        $productAttributeRepository->findOneBy(['code' => '2486'])->willReturn($productAttributeValue);
        $productAttributeRepository->findOneBy(['code' => '1'])->willReturn($productAttribute);

        $productAttributeValue->setCode('2486')->shouldBeCalled();
        $productAttributeValue->setAttribute($productAttribute)->shouldBeCalled();

        $productAttributeValue->setValue('Tshirt');
        $productAttributeValue->setLocaleCode('_locale__');

        $productAttributeValue->getId()->willReturn(null);

        $entityManager->persist($productAttributeValue)->shouldBeCalled();

        $this->import($row);
    }

    function it_returns_resource_code(): void
    {
        $this->getResourceCode()->shouldBeEqualTo('attribute_value');
    }
}
