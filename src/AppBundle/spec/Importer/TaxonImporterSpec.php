<?php

declare(strict_types=1);

namespace spec\AppBundle\Importer;

use AppBundle\Generator\SlugGeneratorInterface;
use AppBundle\Importer\TaxonImporter;
use AppBundle\Importer\TaxonImporterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Elvi\ImportExportBundle\Resolver\ResourceResolverInterface;
use PhpSpec\ObjectBehavior;
use Sylius\Component\Taxonomy\Model\TaxonInterface;

final class TaxonImporterSpec extends ObjectBehavior
{
    function let(
        ResourceResolverInterface $taxonResourceResolver,
        SlugGeneratorInterface $slugGenerator,
        EntityManagerInterface $entityManager
    ): void {
        $this->beConstructedWith($taxonResourceResolver, $slugGenerator, $entityManager);
    }

    function it_is_initializable(): void
    {
        $this->shouldHaveType(TaxonImporter::class);
    }

    function it_should_implements_taxon_importer_interface(): void
    {
        $this->shouldImplement(TaxonImporterInterface::class);
    }

    function it_imports_taxon(
        ResourceResolverInterface $taxonResourceResolver,
        TaxonInterface $taxon,
        SlugGeneratorInterface $slugGenerator,
        EntityManagerInterface $entityManager
    ): void {
        $row = ['ID' => '2486', 'TEXT__LOCALE__' => 'TEST'];

        $taxonResourceResolver->resolveResource('2486')->willReturn($taxon);

        $taxon->setCode('2486')->shouldBeCalled();
        $taxon->setPosition(1)->shouldBeCalled()->shouldBeCalled();
        $taxon->setLeft(1)->shouldBeCalled();
        $taxon->setRight(1)->shouldBeCalled();
        $taxon->setLevel(1)->shouldBeCalled();

        $taxon->setCurrentLocale('_locale__')->shouldBeCalled();
        $taxon->setFallbackLocale('_locale__')->shouldBeCalled();
        $taxon->setName('TEST')->shouldBeCalled();

        $slugGenerator->generate('TEST')->willReturn('Slug');
        $taxon->setSlug('Slug')->shouldBeCalled();

        $taxon->getId()->willReturn(null);
        $entityManager->persist($taxon)->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();

        $this->import($row);
    }

    function it_returns_resource_code(): void
    {
        $this->getResourceCode()->shouldBeEqualTo('taxon');
    }
}
