<?php

declare(strict_types=1);

namespace spec\AppBundle\Importer;

use AppBundle\Entity\BrandInterface;
use AppBundle\Importer\BrandImporter;
use AppBundle\Importer\BrandImporterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Elvi\ImportExportBundle\Resolver\ResourceResolverInterface;
use PhpSpec\ObjectBehavior;

final class BrandImporterSpec extends ObjectBehavior
{
    function let(
        ResourceResolverInterface $brandResourceResolver,
        EntityManagerInterface $entityManager
    ): void {
        $this->beConstructedWith($brandResourceResolver, $entityManager);
    }

    function it_is_initializable(): void
    {
        $this->shouldHaveType(BrandImporter::class);
    }

    function it_implements_brand_importer_interface(): void
    {
        $this->shouldHaveType(BrandImporterInterface::class);
    }

    function it_imports_brand(
        ResourceResolverInterface $brandResourceResolver,
        BrandInterface $brand,
        EntityManagerInterface $entityManager
    ): void {
        $row = ['ID' => '2486', 'TITLE' => 'Adidas'];

        $brandResourceResolver->resolveResource('2486')->willReturn($brand);
        $brand->getId()->willReturn(null);

        $brand->setCode('2486')->shouldBeCalled();
        $brand->setName('Adidas')->shouldBeCalled();

        $entityManager->persist($brand)->shouldBeCalled();

        $this->import($row);
    }

    function it_returns_resource_code(): void
    {
        $this->getResourceCode()->shouldBeEqualTo('brand');
    }
}
