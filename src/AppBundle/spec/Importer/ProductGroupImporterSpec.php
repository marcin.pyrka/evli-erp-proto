<?php

declare(strict_types=1);

namespace spec\AppBundle\Importer;

use AppBundle\Entity\ProductFamilyInterface;
use AppBundle\Entity\ProductGroupInterface;
use AppBundle\Importer\ProductGroupImporter;
use AppBundle\Importer\ProductGroupImporterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Elvi\ImportExportBundle\Resolver\ResourceResolverInterface;
use PhpSpec\ObjectBehavior;

final class ProductGroupImporterSpec extends ObjectBehavior
{
    function let(
        ResourceResolverInterface $productGroupResourceResolver,
        ResourceResolverInterface $productFamilyResourceResolver,
        EntityManagerInterface $entityManager
    ): void {
        $this->beConstructedWith($productGroupResourceResolver, $productFamilyResourceResolver, $entityManager);
    }

    function it_is_initializable(): void
    {
        $this->shouldHaveType(ProductGroupImporter::class);
    }

    function it_should_implements_product_group_importer_interface(): void
    {
        $this->shouldImplement(ProductGroupImporterInterface::class);
    }

    function it_imports_product_group(
        ResourceResolverInterface $productGroupResourceResolver,
        ResourceResolverInterface $productFamilyResourceResolver,
        ProductGroupInterface $productGroup,
        ProductFamilyInterface $productFamily,
        EntityManagerInterface $entityManager
    ): void {
        $row = ['ID' => '2486', 'TITLE' => 'Clothes', 'PRODUCT_FAMILY_ID' => '123'];

        $productGroupResourceResolver->resolveResource('2486')->willReturn($productGroup);

        $productFamilyResourceResolver->resolveResource('123')->willReturn($productFamily);

        $productGroup->setCode('2486')->shouldBeCalled();
        $productGroup->setFamily($productFamily)->shouldBeCalled();
        $productGroup->setName('Clothes')->shouldBeCalled();

        $productGroup->getId()->willReturn(null);
        $entityManager->persist($productGroup)->shouldBeCalled();

        $this->import($row);
    }

    function it_returns_resource_code(): void
    {
        $this->getResourceCode()->shouldBeEqualTo('product_group');
    }
}
