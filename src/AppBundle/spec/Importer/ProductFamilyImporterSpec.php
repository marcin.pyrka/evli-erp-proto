<?php

declare(strict_types=1);

namespace spec\AppBundle\Importer;

use AppBundle\Entity\BrandInterface;
use AppBundle\Entity\ProductFamilyInterface;
use AppBundle\Importer\ProductFamilyImporter;
use AppBundle\Importer\ProductFamilyImporterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Elvi\ImportExportBundle\Resolver\ResourceResolverInterface;
use PhpSpec\ObjectBehavior;

final class ProductFamilyImporterSpec extends ObjectBehavior
{
    function let(
        ResourceResolverInterface $productFamilyResourceResolver,
        ResourceResolverInterface $brandResourceResolver,
        EntityManagerInterface $entityManager
    ): void {
        $this->beConstructedWith($productFamilyResourceResolver, $brandResourceResolver, $entityManager);
    }

    function it_is_initializable(): void
    {
        $this->shouldHaveType(ProductFamilyImporter::class);
    }

    function it_should_implements_product_family_importer_interface(): void
    {
        $this->shouldImplement(ProductFamilyImporterInterface::class);
    }

    function it_imports_product_family(
        ResourceResolverInterface $productFamilyResourceResolver,
        ProductFamilyInterface $productFamily,
        ResourceResolverInterface $brandResourceResolver,
        BrandInterface $brand,
        EntityManagerInterface $entityManager
    ): void {
        $row = ['ID' => '2486', 'TITLE' => 'Jackets', 'BRAND_ID' => '123'];

        $productFamilyResourceResolver->resolveResource('2486')->willReturn($productFamily);

        $brandResourceResolver->resolveResource('123')->willReturn($brand);

        $productFamily->setCode('2486')->shouldBeCalled();
        $productFamily->setBrand($brand)->shouldBeCalled();
        $productFamily->setName('Jackets')->shouldBeCalled();

        $productFamily->getId()->willReturn(null);
        $entityManager->persist($productFamily)->shouldBeCalled();

        $this->import($row);
    }

    function it_returns_resource_code(): void
    {
        $this->getResourceCode()->shouldBeEqualTo('product_family');
    }
}
