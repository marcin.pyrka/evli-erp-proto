<?php

declare(strict_types=1);

namespace spec\AppBundle\Generator;

use AppBundle\Generator\CodeGeneratorInterface;
use PhpSpec\ObjectBehavior;

final class CodeGeneratorSpec extends ObjectBehavior
{
    public function it_implements_code_generator_interface(): void
    {
        $this->shouldImplement(CodeGeneratorInterface::class);
    }

    public function it_generates_code_based_on_given_value(): void
    {
        $this->generate('Naksos')->shouldReturn('naksos');
        $this->generate('High-Five')->shouldReturn('high_five');
    }

    public function it_generates_code_without_punctuation_marks(): void
    {
        $this->generate('"Lorem ipsum: dolor sit"')->shouldReturn('lorem_ipsum_dolor_sit');
        $this->generate('Sed vel- sodales() libero*')->shouldReturn('sed_vel_sodales_libero');
        $this->generate('Nullam sed: dui mollis...')->shouldReturn('nullam_sed_dui_mollis');
    }

    public function it_generates_code_without_special_signs(): void
    {
        $this->generate('Wejść do kręgielni: Europa')->shouldReturn('wejsc_do_kregielni_europa');
    }
}
