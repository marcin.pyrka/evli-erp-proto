<?php

declare(strict_types=1);

namespace spec\AppBundle\Generator;

use AppBundle\Generator\SlugGenerator;
use AppBundle\Generator\SlugGeneratorInterface;
use AppBundle\Doctrine\ORM\Repository\ProductRepositoryInterface;
use PhpSpec\ObjectBehavior;
use Sylius\Component\Resource\Model\ResourceInterface;

final class SlugGeneratorSpec extends ObjectBehavior
{
    function let(ProductRepositoryInterface $repository): void
    {
        $this->beConstructedWith($repository);
    }

    function it_implements_slug_generator_interface(): void
    {
        $this->shouldImplement(SlugGeneratorInterface::class);
    }

    function it_generates_slug_based_on_given_name(): void
    {
        $this->generate('ADIDAS')->shouldReturn('adidas');
        $this->generate('ADI\DAS')->shouldReturn('adi-das');
    }

    function it_generates_slug_without_punctuation_marks(ResourceInterface $firstProduct, ProductRepositoryInterface $repository): void
    {
        $repository->findOneBySlugOnly('adi-das');
        $firstProduct->getId()->willReturn('0');

        $this->generate('@ADI\DAS&&')->shouldReturn('adi-das');
    }

    public function it_generates_code_without_special_signs(ResourceInterface $firstProduct, ProductRepositoryInterface $repository): void
    {
        $repository->findOneBySlugOnly('wejsc-do-kregielni-europa');
        $firstProduct->getId()->willReturn('0');

        $this->generate('Wejść do kręgielni: Europa')->shouldReturn('wejsc-do-kregielni-europa');
    }
}
