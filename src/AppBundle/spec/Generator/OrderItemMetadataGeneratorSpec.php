<?php

declare(strict_types=1);

namespace spec\AppBundle\Generator;

use AppBundle\Generator\OrderItemMetadataGeneratorInterface;
use PhpSpec\ObjectBehavior;
use Sylius\Component\Product\Model\ProductOptionValueInterface;

final class OrderItemMetadataGeneratorSpec extends ObjectBehavior
{
    function it_implements_order_item_metadata_generator_interface(): void
    {
        $this->shouldImplement(OrderItemMetadataGeneratorInterface::class);
    }

    function it_cannot_generate_order_item_metadata_without_option_value(): void
    {
        $optionValues = [1, 2, 3];

        $this->shouldThrow(\InvalidArgumentException::class)->during('generateFromOptionValues', [$optionValues]);
    }

    function it_generates_metadata(
        ProductOptionValueInterface $firstOptionValue,
        ProductOptionValueInterface $secondOptionValue
    ): void {
        $optionValues = [$firstOptionValue, $secondOptionValue];
        $this->shouldNotThrow(\InvalidArgumentException::class)->during('generateFromOptionValues', [$optionValues]);

        $firstOptionValue->getOptionCode()->willReturn('size');
        $firstOptionValue->getValue()->willReturn('S');

        $secondOptionValue->getOptionCode()->willReturn('color');
        $secondOptionValue->getValue()->willReturn('yellow');

        $this->generateFromOptionValues($optionValues)->shouldBeEqualTo('size:S&color:yellow');
    }
}
