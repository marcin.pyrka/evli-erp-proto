<?php

declare(strict_types=1);

namespace spec\AppBundle\Generator;

use AppBundle\Entity\ProductInterface;
use AppBundle\Generator\ProductNameGeneratorInterface;
use Elvi\SyliusProductComponent\Model\ProductFamilyInterface;
use Elvi\SyliusProductComponent\Model\ProductGroupInterface;
use PhpSpec\ObjectBehavior;

final class ProductNameGeneratorSpec extends ObjectBehavior
{
    public function it_implements_product_name_generator_interface()
    {
        $this->shouldImplement(ProductNameGeneratorInterface::class);
    }

    public function it_cannot_generate_product_name_with_null_group(ProductInterface $product): void
    {
        $product->getGroup()->willReturn(null);

        $this->generate($product)->shouldReturn('');
    }

    public function it_cannot_generate_product_name_with_null_family(ProductInterface $product, ProductGroupInterface $family): void
    {
        $family->getFamily()->willReturn(null);

        $this->generate($product)->shouldReturn('');
    }

    public function it_generates_product_name(ProductInterface $product, ProductGroupInterface $family, ProductFamilyInterface $group): void
    {
        $group->getName()->willReturn('Ship');
        $family->getName()->willReturn('Tanker');

        $family->getFamily()->willReturn($group);
        $product->getGroup()->willReturn($family);

        $product->getQuantity()->willReturn(15);
        $product->getUnit()->willReturn('Km/h');

        $this->generate($product)->shouldReturn('Ship Tanker 15 Km/h');
    }
}
