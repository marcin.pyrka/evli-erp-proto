<?php

declare(strict_types=1);

namespace AppBundle\Entity;

class ProductVariantOptionValue implements ProductVariantOptionValueInterface
{
    /** @var int */
    private $id;

    /** @var ProductInterface */
    private $product;

    /** @var ProductVariantInterface */
    private $variant;

    /** @var float */
    private $power;

    /** @var float */
    private $axis;

    /** @var float */
    private $radius;

    /** @var float */
    private $cylinder;

    /** @var float */
    private $diameter;

    /** @var float */
    private $add;

    /** @var float */
    private $colour;

    public function getId(): int
    {
        return $this->id;
    }

    public function getProduct(): ProductInterface
    {
        return $this->product;
    }

    public function setProduct(ProductInterface $product): void
    {
        $this->product = $product;
    }

    public function getVariant(): ProductVariantInterface
    {
        return $this->variant;
    }

    public function setVariant(ProductVariantInterface $variant): void
    {
        $this->variant = $variant;
    }

    public function getPower(): ?float
    {
        return $this->power;
    }

    public function setPower(?float $power): void
    {
        $this->power = $power;
    }

    public function getAxis(): ?float
    {
        return $this->axis;
    }

    public function setAxis(?float $axis): void
    {
        $this->axis = $axis;
    }

    public function getRadius(): ?float
    {
        return $this->radius;
    }

    public function setRadius(?float $radius): void
    {
        $this->radius = $radius;
    }

    public function getCylinder(): ?float
    {
        return $this->cylinder;
    }

    public function setCylinder(?float $cylinder): void
    {
        $this->cylinder = $cylinder;
    }

    public function getDiameter(): ?float
    {
        return $this->diameter;
    }

    public function setDiameter(?float $diameter): void
    {
        $this->diameter = $diameter;
    }

    public function getAdd(): ?float
    {
        return $this->add;
    }

    public function setAdd(?float $add): void
    {
        $this->add = $add;
    }

    public function getColour(): ?float
    {
        return $this->colour;
    }

    public function setColour(?float $colour): void
    {
        $this->colour = $colour;
    }
}
