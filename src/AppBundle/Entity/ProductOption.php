<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Elvi\SyliusProductComponent\Model\ProductOptionTrait;
use Sylius\Component\Product\Model\ProductOption as BaseProductOption;

class ProductOption extends BaseProductOption implements ProductOptionInterface
{
    use ProductOptionTrait;

    /**
     * @return string
     */
    public function getIdent(): ?string
    {
        return $this->ident;
    }

    /**
     * @param string $ident
     */
    public function setIdent(string $ident): void
    {
        $this->ident = $ident;
    }
}
