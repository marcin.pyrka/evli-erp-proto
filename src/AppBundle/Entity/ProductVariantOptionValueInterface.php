<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;

interface ProductVariantOptionValueInterface extends ResourceInterface
{
    public function getId(): int;

    public function getProduct(): ProductInterface;

    public function setProduct(ProductInterface $product): void;

    public function getVariant(): ProductVariantInterface;

    public function setVariant(ProductVariantInterface $variant): void;

    public function getPower(): ?float;

    public function setPower(?float $power): void;

    public function getAxis(): ?float;

    public function setAxis(?float $axis): void;

    public function getRadius(): ?float;

    public function setRadius(?float $radius): void;

    public function getCylinder(): ?float;

    public function setCylinder(?float $cylinder): void;

    public function getDiameter(): ?float;

    public function setDiameter(?float $diameter): void;

    public function getAdd(): ?float;

    public function setAdd(?float $add): void;

    public function getColour(): ?float;

    public function setColour(?float $colour): void;
}
