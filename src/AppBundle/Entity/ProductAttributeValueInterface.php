<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Elvi\SyliusProductComponent\Model\ProductAttributeValueInterface as BaseProductAttributeValueInterface;

interface ProductAttributeValueInterface extends BaseProductAttributeValueInterface
{
}
