<?php

declare(strict_types=1);

namespace AppBundle\Entity\Fulfiller;

use Sylius\Component\Resource\Model\ResourceInterface;

class FulfillerOptionValue implements ResourceInterface
{
    /**
     * ID
     *
     * @var string
     */
    private $id;

    /**
     * Ignore
     *
     * @var bool
     */
    private $ignore;

    /**
     * Option value title
     *
     * @var string
     */
    private $title;

    /**
     * Option raw value
     *
     * @var string
     */
    private $value;

    /**
     * @var FulfillerOption
     */
    private $option;

    /**
     * @var Collection
     */
    private $variants;

    /**
     * @var Collection
     */
    private $optionVariants;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return FulfillerOption
     */
    public function getOption(): FulfillerOption
    {
        return $this->option;
    }

    /**
     * @param FulfillerOption $option
     */
    public function setOption(FulfillerOption $option): void
    {
        $this->option = $option;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return Collection
     */
    public function getVariants()
    {
        return $this->variants;
    }

    /**
     * @param Collection $variants
     */
    public function setVariants($variants)
    {
        $this->variants = $variants;
    }

    /**
     * @return Collection
     */
    public function getOptionVariants()
    {
        return $this->optionVariants;
    }

    /**
     * @param Collection $optionVariants
     */
    public function setOptionVariants($optionVariants)
    {
        $this->optionVariants = $optionVariants;
    }

    /**
     * @return bool
     */
    public function isIgnore()
    {
        return $this->ignore;
    }

    /**
     * @param bool $ignore
     */
    public function setIgnore($ignore)
    {
        $this->ignore = $ignore;
    }
}
