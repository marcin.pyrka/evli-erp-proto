<?php

declare(strict_types=1);

namespace AppBundle\Entity\Fulfiller;

use Sylius\Component\Resource\Model\ResourceInterface;

class TransformationRule implements ResourceInterface
{
    /**
     * ID
     *
     * @var string
     */
    private $id;

    /**
     * fulfiller id ?
     *
     * @var string
     */
    private $fulfiller;

    /**
     * Transformation type (option, option_value, product_title)
     *
     * @var
     */
    private $type;

    /**
     * @var FulfillerProduct
     */
    private $fulfillerProduct;

    /**
     * Title
     *
     * @var string
     */
    private $sourceString;

    /**
     * Target
     *
     * @var string
     */
    private $targetString;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getSourceString()
    {
        return $this->sourceString;
    }

    /**
     * @param string $sourceString
     */
    public function setSourceString($sourceString)
    {
        $this->sourceString = $sourceString;
    }

    /**
     * @return string
     */
    public function getTargetString()
    {
        return $this->targetString;
    }

    /**
     * @param string $targetString
     */
    public function setTargetString($targetString)
    {
        $this->targetString = $targetString;
    }

    /**
     * @return FulfillerProduct
     */
    public function getFulfillerProduct()
    {
        return $this->fulfillerProduct;
    }

    /**
     * @param FulfillerProduct $fulfillerProduct
     */
    public function setFulfillerProduct($fulfillerProduct)
    {
        $this->fulfillerProduct = $fulfillerProduct;
    }

    /**
     * @return string
     */
    public function getFulfiller()
    {
        return $this->fulfiller;
    }

    /**
     * @param string $fulfiller
     */
    public function setFulfiller($fulfiller)
    {
        $this->fulfiller = $fulfiller;
    }
}
