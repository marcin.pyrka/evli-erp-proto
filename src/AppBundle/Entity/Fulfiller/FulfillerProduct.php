<?php

declare(strict_types=1);

namespace AppBundle\Entity\Fulfiller;

use Doctrine\Common\Collections\ArrayCollection;
use Sylius\Component\Resource\Model\ResourceInterface;

class FulfillerProduct implements ResourceInterface
{
    /**
     * ID
     *
     * @var string
     */
    private $id;

    /**
     * fulfiller id ?
     *
     * @var string
     */
    private $fulfiller;

    /**
     * External ID
     *
     * @var string
     */
    private $externalId;

    /**
     * Active
     *
     * @var bool
     */
    private $active;

    /**
     * Ignore
     *
     * @var bool
     */
    private $ignore;

    /**
     * Title
     *
     * @var string
     */
    private $title;

    /**
     * Article number
     *
     * @var string
     */
    private $artNum;

    /**
     * Article number
     *
     * @var string
     */
    private $mpn;

    /**
     * Price
     *
     * @var float
     */
    private $price;

    /**
     * Sale Price
     *
     * @var float
     */
    private $priceSale;

    /**
     * Unit name
     *
     * @var string
     */
    private $unitName;

    /**
     * Quantity
     *
     * @var int
     */
    private $quantity = 0;

    /**
     * Product type
     *
     * @var int
     */
    private $productType = 0;

    /**
     * Total option values count
     *
     * @var
     */
    private $totalOptionValuesCount;

    /**
     * Matched option values count
     *
     * @var
     */
    private $matchedOptionValuesCount;

    /**
     * @var Collection
     */
    private $variantList;

    /**
     * @var Collection
     */
    private $valueMapList;

    /**
     * @var FulfillerBrand
     */
    private $brand;

    /**
     * @var Collection
     */
    private $optionValueList;

    /**
     * @var Collection
     */
    private $transformationList;

    /**
     * Imported date
     *
     * @var \DateTime
     */
    private $importDate;

    /**
     * Updated timedate
     *
     * @var \DateTime
     */
    private $updateDate;

    /**
     * Product constructor.
     */
    public function __construct()
    {
        $this->variantList = new ArrayCollection();
        $this->valueMapList = new ArrayCollection();
        $this->optionValueList = new ArrayCollection();
        $this->transformationList = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getExternalId(): string
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId($externalId): void
    {
        $this->externalId = $externalId;
    }

    /**
     * @return bool
     */
    public function getActive(): bool
    {
        return $this->active;
    }

    /**
     * @param string $active
     */
    public function setActive($active): void
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getArtNum(): string
    {
        return $this->artNum;
    }

    /**
     * @param string $artNum
     */
    public function setArtNum($artNum): void
    {
        $this->artNum = $artNum;
    }

    /**
     * @return string
     */
    public function getMpn(): string
    {
        return $this->mpn;
    }

    /**
     * @param string $mpn
     */
    public function setMpn($mpn): void
    {
        $this->mpn = $mpn;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function getPriceSale(): float
    {
        return $this->priceSale;
    }

    /**
     * @param mixed $priceSale
     */
    public function setPriceSale($priceSale): void
    {
        $this->priceSale = $priceSale;
    }

    /**
     * @return mixed
     */
    public function getUnitName(): string
    {
        return $this->unitName;
    }

    /**
     * @param mixed $unitName
     */
    public function setUnitName($unitName): void
    {
        $this->unitName = $unitName;
    }

    /**
     * @return mixed
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return int
     */
    public function getProductType(): int
    {
        return $this->productType;
    }

    /**
     * @param mixed $productCategory
     */
    public function setProductCategory($productCategory): void
    {
        $this->productType = $productCategory;
    }

    /**
     * Raw variant list
     *
     * @return Collection
     */
    public function getVariantList()
    {
        return $this->variantList;
    }

    /**
     * Raw variant list setter
     *
     * @param Collection $variantList
     */
    public function setVariantList($variantList)
    {
        $this->variantList = $variantList;
    }

    /**
     * Import date
     *
     * @return \DateTime
     */
    public function getImportDate()
    {
        return $this->importDate;
    }

    /**
     * Import date
     *
     * @param \DateTime $importDate
     */
    public function setImportDate($importDate)
    {
        $this->importDate = $importDate;
    }

    /**
     * Update date
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Update date
     *
     * @param \DateTime $updateDate
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;
    }

    /**
     * @return FulfillerBrand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param FulfillerBrand $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return Collection
     */
    public function getValueMapList(): array
    {
        return $this->valueMapList;
    }

    /**
     * @param Collection $valueMapList
     */
    public function setValueMapList($valueMapList)
    {
        $this->valueMapList = $valueMapList;
    }

    /**
     * @return string
     */
    public function getFulfiller()
    {
        return $this->fulfiller;
    }

    /**
     * @param string $fulfiller
     */
    public function setFulfiller($fulfiller)
    {
        $this->fulfiller = $fulfiller;
    }

    /**
     * @return Collection
     */
    public function getOptionValueList()
    {
        return $this->optionValueList;
    }

    /**
     * @param Collection $optionValueList
     */
    public function setOptionValueList($optionValueList)
    {
        $this->optionValueList = $optionValueList;
    }

    /**
     * @return Collection
     */
    public function getTransformationList()
    {
        return $this->transformationList;
    }

    /**
     * @param Collection $transformationList
     */
    public function setTransformationList($transformationList)
    {
        $this->transformationList = $transformationList;
    }

    /**
     * @return mixed
     */
    public function getTotalOptionValuesCount()
    {
        return $this->totalOptionValuesCount;
    }

    /**
     * @param mixed $totalOptionValuesCount
     */
    public function setTotalOptionValuesCount($totalOptionValuesCount)
    {
        $this->totalOptionValuesCount = $totalOptionValuesCount;
    }

    /**
     * @return mixed
     */
    public function getMatchedOptionValuesCount()
    {
        return $this->matchedOptionValuesCount;
    }

    /**
     * @param mixed $matchedOptionValuesCount
     */
    public function setMatchedOptionValuesCount($matchedOptionValuesCount)
    {
        $this->matchedOptionValuesCount = $matchedOptionValuesCount;
    }

    /**
     * @return bool
     */
    public function isIgnore()
    {
        return $this->ignore;
    }

    /**
     * @param bool $ignore
     */
    public function setIgnore($ignore)
    {
        $this->ignore = $ignore;
    }
}
