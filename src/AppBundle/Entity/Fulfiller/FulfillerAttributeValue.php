<?php

declare(strict_types=1);

namespace AppBundle\Entity\Fulfiller;

use Sylius\Component\Resource\Model\ResourceInterface;

class FulfillerAttributeValue implements ResourceInterface
{
    /**
     * ID
     *
     * @var string
     */
    private $id;

    /**
     * Attribute value
     *
     * @var string
     */
    private $value;

    /**
     * @var FulfillerAttribute
     */
    private $attribute;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return parent
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * @param parent $attribute
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;
    }
}
