<?php

declare(strict_types=1);

namespace AppBundle\Entity\Fulfiller;

use Sylius\Component\Resource\Model\ResourceInterface;

class FulfillerProductVariant implements ResourceInterface
{
    /**
     * ID
     *
     * @var string
     */
    private $id;

    /**
     * External ID
     *
     * @var string
     */
    private $externalId;

    /**
     * @var parent
     */
    private $parent;

    /**
     * @var Collection
     */
    private $optionValueList;

    /**
     * EAN
     *
     * @var string
     */
    private $ean;

    /**
     * Imported date
     *
     * @var \DateTime
     */
    private $importDate;

    /**
     * Updated timedate
     *
     * @var \DateTime
     */
    private $updateDate;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * @return parent
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param parent $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return string
     */
    public function getEan()
    {
        return $this->ean;
    }

    /**
     * @param string $ean
     */
    public function setEan($ean)
    {
        $this->ean = $ean;
    }

    /**
     * @return \DateTime
     */
    public function getImportDate()
    {
        return $this->importDate;
    }

    /**
     * @param \DateTime $importDate
     */
    public function setImportDate($importDate)
    {
        $this->importDate = $importDate;
    }

    /**
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * @param \DateTime $updateDate
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;
    }

    /**
     * @return Collection
     */
    public function getOptionValueList()
    {
        return $this->optionValueList;
    }

    /**
     * @param Collection $optionValueList
     */
    public function setOptionValueList($optionValueList)
    {
        $this->optionValueList = $optionValueList;
    }
}
