<?php

declare(strict_types=1);

namespace AppBundle\Entity\Fulfiller;

use Sylius\Component\Resource\Model\ResourceInterface;

class FulfillerVariantOptionValue implements ResourceInterface
{
    /**
     * ID
     *
     * @var string
     */
    private $id;

    /**
     * @var FulfillerProductVariant
     */
    private $variant;

    /**
     * @var FulfillerOptionValue
     */
    private $optionValue;

    /**
     * Imported date
     *
     * @var \DateTime
     */
    private $importDate;

    /**
     * Updated timedate
     *
     * @var \DateTime
     */
    private $updateDate;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return FulfillerProductVariant
     */
    public function getVariant()
    {
        return $this->variant;
    }

    /**
     * @param FulfillerProductVariant $variant
     */
    public function setVariant($variant)
    {
        $this->variant = $variant;
    }

    /**
     * @return FulfillerOptionValue
     */
    public function getOptionValue()
    {
        return $this->optionValue;
    }

    /**
     * @param FulfillerOptionValue $optionValue
     */
    public function setOptionValue($optionValue)
    {
        $this->optionValue = $optionValue;
    }

    /**
     * @return \DateTime
     */
    public function getImportDate()
    {
        return $this->importDate;
    }

    /**
     * @param \DateTime $importDate
     */
    public function setImportDate($importDate)
    {
        $this->importDate = $importDate;
    }

    /**
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * @param \DateTime $updateDate
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;
    }
}
