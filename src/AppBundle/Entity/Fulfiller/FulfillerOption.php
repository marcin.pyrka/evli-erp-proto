<?php

declare(strict_types=1);

namespace AppBundle\Entity\Fulfiller;

use Doctrine\Common\Collections\ArrayCollection;
use Sylius\Component\Resource\Model\ResourceInterface;

/**
 * @ORM\Table(name="fulfiller_raw_option")
 * @ORM\Entity()
 */
class FulfillerOption implements ResourceInterface
{
    /**
     * ID
     *
     * @var string
     */
    private $id;

    /**
     * Ignore
     *
     * @var bool
     */
    private $ignore;

    /**
     * Option title
     *
     * @var string
     */
    private $title;

    /**
     * Option position
     *
     * @var string
     */
    private $position;

    /**
     * @var Collection
     */
    private $valueList;

    /**
     * @var Collection
     */
    private $valueMapList;

    /**
     * Option constructor.
     */
    public function __construct()
    {
        $this->valueList = new ArrayCollection();
        $this->valueMapList = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return Collection
     */
    public function getValueList()
    {
        return $this->valueList;
    }

    /**
     * @param Collection $valueList
     */
    public function setValueList($valueList)
    {
        $this->valueList = $valueList;
    }

    /**
     * @return Collection
     */
    public function getValueMapList()
    {
        return $this->valueMapList;
    }

    /**
     * @param Collection $valueMapList
     */
    public function setValueMapList($valueMapList)
    {
        $this->valueMapList = $valueMapList;
    }

    /**
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param string $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return bool
     */
    public function isIgnore()
    {
        return $this->ignore;
    }

    /**
     * @param bool $ignore
     */
    public function setIgnore($ignore)
    {
        $this->ignore = $ignore;
    }
}
