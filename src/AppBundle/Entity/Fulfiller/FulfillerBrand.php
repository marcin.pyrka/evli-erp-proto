<?php

declare(strict_types=1);

namespace AppBundle\Entity\Fulfiller;

use Doctrine\Common\Collections\ArrayCollection;
use Sylius\Component\Resource\Model\ResourceInterface;

class FulfillerBrand implements ResourceInterface
{
    /**
     * ID
     *
     * @var string
     */
    private $id;

    /**
     * Option title
     *
     * @var string
     */
    private $title;

    /**
     * @var Collection
     */
    private $productList;

    /**
     * Customer constructor.
     */
    public function __construct()
    {
        $this->productList = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return Collection
     */
    public function getProductList()
    {
        return $this->productList;
    }

    /**
     * @param Collection $productList
     */
    public function setProductList($productList)
    {
        $this->productList = $productList;
    }
}
