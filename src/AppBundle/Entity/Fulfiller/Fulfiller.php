<?php

declare(strict_types=1);

namespace AppBundle\Entity\Fulfiller;

use Doctrine\Common\Collections\ArrayCollection;
use Sylius\Component\Resource\Model\ResourceInterface;

class Fulfiller implements ResourceInterface
{
    /**
     * ID
     *
     * @var string
     */
    private $id;

    /**
     * Option title
     *
     * @var string
     */
    private $title;

    /**
     * Total amount of products
     *
     * @var int
     */
    private $totalProductsCount;

    /**
     * Matched products count
     *
     * @var int
     */
    private $matchedProductsCount;

    /**
     * @var Collection
     */
    private $productList;

    /**
     * @var Collection
     */
    private $mappingList;

    /**
     * @var Collection
     */
    private $transformationList;

    /**
     * Customer constructor.
     */
    public function __construct()
    {
        $this->productList = new ArrayCollection();
        $this->mappingList = new ArrayCollection();
        $this->transformationList = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return Collection
     */
    public function getProductList()
    {
        return $this->productList;
    }

    /**
     * @param Collection $productList
     */
    public function setProductList($productList)
    {
        $this->productList = $productList;
    }

    /**
     * @return Collection
     */
    public function getMappingList()
    {
        return $this->mappingList;
    }

    /**
     * @param Collection $mappingList
     */
    public function setMappingList($mappingList)
    {
        $this->mappingList = $mappingList;
    }

    /**
     * @return Collection
     */
    public function getTransformationList()
    {
        return $this->transformationList;
    }

    /**
     * @param Collection $transformationList
     */
    public function setTransformationList($transformationList)
    {
        $this->transformationList = $transformationList;
    }

    /**
     * @return int
     */
    public function getTotalProductsCount()
    {
        return $this->totalProductsCount;
    }

    /**
     * @param int $totalProductsCount
     */
    public function setTotalProductsCount($totalProductsCount)
    {
        $this->totalProductsCount = $totalProductsCount;
    }

    /**
     * @return int
     */
    public function getMatchedProductsCount()
    {
        return $this->matchedProductsCount;
    }

    /**
     * @param int $matchedProductsCount
     */
    public function setMatchedProductsCount($matchedProductsCount)
    {
        $this->matchedProductsCount = $matchedProductsCount;
    }
}
