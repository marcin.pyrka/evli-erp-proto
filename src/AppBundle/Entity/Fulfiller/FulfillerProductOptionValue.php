<?php

declare(strict_types=1);

namespace AppBundle\Entity\Fulfiller;

use Sylius\Component\Resource\Model\ResourceInterface;

class FulfillerProductOptionValue implements ResourceInterface
{
    /**
     * ID
     *
     * @var string
     */
    private $id;

    /**
     * @var FulfillerProduct
     */
    private $product;

    /**
     * @var FulfillerOptionValue
     */
    private $optionValue;

    /**
     * Imported date
     *
     * @var \DateTime
     */
    private $importDate;

    /**
     * Updated timedate
     *
     * @var \DateTime
     */
    private $updateDate;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return FulfillerProduct
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param FulfillerProduct $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return FulfillerOptionValue
     */
    public function getOptionValue()
    {
        return $this->optionValue;
    }

    /**
     * @param FulfillerOptionValue $optionValue
     */
    public function setOptionValue($optionValue)
    {
        $this->optionValue = $optionValue;
    }

    /**
     * @return \DateTime
     */
    public function getImportDate()
    {
        return $this->importDate;
    }

    /**
     * @param \DateTime $importDate
     */
    public function setImportDate($importDate)
    {
        $this->importDate = $importDate;
    }

    /**
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * @param \DateTime $updateDate
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;
    }
}
