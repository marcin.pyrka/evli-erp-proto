<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Elvi\SyliusProductComponent\Model\ProductImageInterface as ElviProductImageInterface;
use Sylius\Component\Resource\Model\ResourceInterface;

interface ProductImageInterface extends ElviProductImageInterface, ResourceInterface
{
    public function getFile(): ?\SplFileInfo;

    public function setFile(\SplFileInfo $file): void;

    public function hasFile(): bool;

    public function getPath(): string;

    public function setPath(string $path): void;

    public function hasPath(): bool;

    public function getProduct(): ProductInterface;

    public function setProduct(?ProductInterface $product): void;
}
