<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Elvi\SyliusProductComponent\Model\BrandInterface as BaseBrandInterface;

interface BrandInterface extends BaseBrandInterface
{
}
