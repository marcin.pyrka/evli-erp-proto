<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use AppBundle\Entity\Fulfiller\Fulfiller;
use AppBundle\Entity\Fulfiller\FulfillerOption;
use AppBundle\Entity\Fulfiller\FulfillerOptionValue;
use AppBundle\Entity\Fulfiller\FulfillerProduct;
use Sylius\Component\Resource\Model\ResourceInterface;

class ElviOptionValueMap implements ResourceInterface
{
    /**
     * ID
     *
     * @var string
     */
    private $id;

    /**
     * Fulfiller
     *
     * @var Fulfiller
     */
    private $fulfiller;

    /**
     * Is mapped ??
     *
     * @var string
     */
    private $isMapped;

    /**
     * Mapping type
     *
     * @var string
     */
    private $mappingType;

    /**
     * Fulfiller product
     *
     * @var FulfillerProduct
     */
    private $fulfillerProduct;

    /**
     * ELVI product
     *
     * @var Product
     */
    private $elviProduct;

    /**
     * Fulfiller option
     *
     * @var FulfillerOption
     */
    private $fulfillerOption;

    /**
     * Fulfiller option value
     *
     * @var FulfillerOptionValue
     */
    private $fulfillerOptionValue;

    /**
     * Elvi option
     *
     * @var FulfillerOption
     */
    private $elviOption;

    /**
     * Elvi option value
     *
     * @var FulfillerOption
     */
    private $elviOptionValue;

    /**
     * Mapped options
     *
     * @var FulfillerOption
     */
    private $valueMapList;

    /**
     * Datetime of creation
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * ElviOptionValueMap constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getisMapped()
    {
        return $this->isMapped;
    }

    /**
     * @param string $isMapped
     */
    public function setIsMapped($isMapped)
    {
        $this->isMapped = $isMapped;
    }

    /**
     * @return string
     */
    public function getMappingType()
    {
        return $this->mappingType;
    }

    /**
     * @param string $mappingType
     */
    public function setMappingType($mappingType)
    {
        $this->mappingType = $mappingType;
    }

    /**
     * @return FulfillerProduct
     */
    public function getFulfillerProduct(): FulfillerProduct
    {
        return $this->fulfillerProduct;
    }

    /**
     * @param FulfillerProduct $fulfillerProduct
     */
    public function setFulfillerProduct(FulfillerProduct $fulfillerProduct): void
    {
        $this->fulfillerProduct = $fulfillerProduct;
    }

    /**
     * @return Product
     */
    public function getElviProduct(): Product
    {
        return $this->elviProduct;
    }

    /**
     * @param FulfillerProduct $elviProduct
     */
    public function setElviProduct($elviProduct)
    {
        $this->elviProduct = $elviProduct;
    }

    /**
     * @return FulfillerOption
     */
    public function getFulfillerOption()
    {
        return $this->fulfillerOption;
    }

    /**
     * @param FulfillerOption $fulfillerOption
     */
    public function setFulfillerOption(FulfillerOption $fulfillerOption): void
    {
        $this->fulfillerOption = $fulfillerOption;
    }

    /**
     * @return FulfillerOptionValue
     */
    public function getFulfillerOptionValue()
    {
        return $this->fulfillerOptionValue;
    }

    public function setFulfillerOptionValue(FulfillerOptionValue $fulfillerOptionValue): void
    {
        $this->fulfillerOptionValue = $fulfillerOptionValue;
    }

    /**
     * @return FulfillerOption
     */
    public function getElviOption()
    {
        return $this->elviOption;
    }

    /**
     * @param FulfillerOption $elviOption
     */
    public function setElviOption($elviOption)
    {
        $this->elviOption = $elviOption;
    }

    /**
     * @return FulfillerOption
     */
    public function getElviOptionValue()
    {
        return $this->elviOptionValue;
    }

    /**
     * @param FulfillerOption $elviOptionValue
     */
    public function setElviOptionValue($elviOptionValue)
    {
        $this->elviOptionValue = $elviOptionValue;
    }

    /**
     * @return FulfillerOption
     */
    public function getValueMapList()
    {
        return $this->valueMapList;
    }

    /**
     * @param FulfillerOption $valueMapList
     */
    public function setValueMapList($valueMapList)
    {
        $this->valueMapList = $valueMapList;
    }

    /**
     * @return Fulfiller
     */
    public function getFulfiller()
    {
        return $this->fulfiller;
    }

    /**
     * @param Fulfiller $fulfiller
     */
    public function setFulfiller($fulfiller)
    {
        $this->fulfiller = $fulfiller;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }
}
