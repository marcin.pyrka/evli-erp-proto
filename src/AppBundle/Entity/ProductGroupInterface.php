<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Elvi\SyliusProductComponent\Model\ProductGroupInterface as BaseProductGroupInterface;

interface ProductGroupInterface extends BaseProductGroupInterface
{
}
