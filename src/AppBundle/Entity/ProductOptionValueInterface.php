<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Elvi\SyliusProductComponent\Model\ProductOptionValueInterface as BaseProductOptionValueInterface;

interface ProductOptionValueInterface extends BaseProductOptionValueInterface
{
}
