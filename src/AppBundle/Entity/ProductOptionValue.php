<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Elvi\SyliusProductComponent\Model\ProductOptionValueTrait;
use Sylius\Component\Product\Model\ProductOptionValue as BaseProductOptionValue;

class ProductOptionValue extends BaseProductOptionValue implements ProductOptionValueInterface
{
    use ProductOptionValueTrait;

    public function getRawValue(): ?string
    {
        return $this->rawValue;
    }

    public function setRawValue(string $rawValue): void
    {
        $this->rawValue = $rawValue;
    }
}
