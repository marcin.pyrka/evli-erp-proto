<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use AppBundle\Entity\Fulfiller\Fulfiller;
use AppBundle\Entity\ValueObject\FulFillerOrderId;
use AppBundle\Entity\ValueObject\OrderAppId;
use AppBundle\Entity\ValueObject\OrderUuid;

class Order
{
    /** @var int */
    private $id;

    /** @var OrderAppId */
    private $appId;

    /** @var FulFillerOrderId */
    private $fulFillerOrderId;

    /** @var Fulfiller */
    private $fulFiller;

    /** @var OrderUuid */
    private $orderUuid;

    /** @var \DateTimeImmutable */
    private $checkoutCompletedAt;

    /** @var \DateTimeImmutable|null */
    private $requestedAt;

    public function __construct(
        OrderUuid $uuid,
        OrderAppId $orderAppId,
        Fulfiller $fulFiller,
        \DateTimeImmutable $checkoutCompletedAt
    ) {
        $this->orderUuid = $uuid;
        $this->appId = $orderAppId;
        $this->fulFiller = $fulFiller;
        $this->checkoutCompletedAt = $checkoutCompletedAt;
    }

    public function relateWithFulFillerOrder(FulFillerOrderId $fulFillerId): void
    {
        $this->fulFillerOrderId = $fulFillerId;
    }

    public function changeRequestedAtDate(\DateTimeImmutable $dateTime): void
    {
        $this->requestedAt = $dateTime;
    }
}
