<?php

declare(strict_types=1);

namespace AppBundle\Entity\ValueObject;

use AppBundle\Exception\OrderTokenException;

final class OrderUuid
{
    private const UUID_V1_VALIDATION_REGEX = '~^[0-9a-f]{8}-[0-9a-f]{4}-1[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$~i';

    /**
     * @var string
     */
    private $value;

    public function __construct(string $token)
    {
        if (empty($token)) {
            throw OrderTokenException::onEmpty();
        }

        if (!$this->isValidUuid($token)) {
            throw OrderTokenException::onInvalidUuid();
        }

        $this->value = $token;
    }

    public function __toString(): string
    {
        return $this->value;
    }

    private function isValidUuid(string $uuid): bool
    {
        if (preg_match(self::UUID_V1_VALIDATION_REGEX, $uuid)) {
            return true;
        }

        return false;
    }
}
