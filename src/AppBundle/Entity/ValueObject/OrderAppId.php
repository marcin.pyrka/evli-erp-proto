<?php

declare(strict_types=1);

namespace AppBundle\Entity\ValueObject;

use AppBundle\Exception\OrderAppIdException;

final class OrderAppId
{
    /** @var string */
    private $value;

    public function __construct(string $value)
    {
        if (empty($value)) {
            throw OrderAppIdException::onEmpty();
        }
        $this->value = $value;
    }

    public function __toString(): string
    {
        return $this->value;
    }
}
