<?php

declare(strict_types=1);

namespace AppBundle\Entity\ValueObject;

final class OrderCheckoutCompletedAt
{
    private const DATE_FORMAT = 'Y-m-d H:i:s';

    /**
     * @var \DateTime
     */
    private $date;

    public function __construct(\DateTime $date)
    {
        $this->date = $date;
    }

    public static function fromDateAsString(string $date): self
    {
        return new self(\DateTime::createFromFormat(self::DATE_FORMAT, $date));
    }

    public function __toString(): string
    {
        return $this->date->format(self::DATE_FORMAT);
    }
}
