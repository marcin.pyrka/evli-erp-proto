<?php

declare(strict_types=1);

namespace AppBundle\Entity\ValueObject;

use AppBundle\Exception\FulFillerOrderIdException;

final class FulFillerOrderId
{
    /** @var string */
    private $value;

    public function __construct(string $externalId)
    {
        if (empty($externalId)) {
            throw FulFillerOrderIdException::onEmpty();
        }

        $this->value = $externalId;
    }

    public function __toString(): string
    {
        return $this->value;
    }
}
