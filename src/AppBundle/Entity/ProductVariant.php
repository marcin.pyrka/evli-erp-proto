<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Elvi\SyliusProductComponent\Model\ProductVariantTrait;
use Sylius\Component\Product\Model\ProductVariant as BaseProductVariant;

class ProductVariant extends BaseProductVariant implements ProductVariantInterface
{
    use ProductVariantTrait;
}
