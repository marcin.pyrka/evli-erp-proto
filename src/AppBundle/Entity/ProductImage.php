<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Elvi\SyliusProductComponent\Model\ProductImageTrait;

class ProductImage implements ProductImageInterface
{
    use ProductImageTrait;

    /** @var int */
    private $id;

    /** @var \SplFileInfo */
    private $file;

    /** @var string */
    private $path;

    /** @var ProductInterface */
    private $product;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFile(): ?\SplFileInfo
    {
        return $this->file;
    }

    public function setFile(\SplFileInfo $file): void
    {
        $this->file = $file;
    }

    public function hasFile(): bool
    {
        return null !== $this->file;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    public function hasPath(): bool
    {
        return null !== $this->path;
    }

    public function getProduct(): ProductInterface
    {
        return $this->product;
    }

    public function setProduct(?ProductInterface $product): void
    {
        $this->product = $product;
    }
}
