<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Elvi\SyliusProductComponent\Model\ProductInterface as BaseProductInterface;
use Sylius\Component\Taxonomy\Model\TaxonInterface;

interface ProductInterface extends BaseProductInterface
{
    public function addTaxon(TaxonInterface $taxon): void;

    public function removeTaxon(TaxonInterface $taxon): void;

    /**
     * @return Collection|TaxonInterface[]
     */
    public function getTaxons(): Collection;

    public function hasTaxon(TaxonInterface $taxon): bool;

    public function getImages(): Collection;

    public function hasImage(ProductImageInterface $image): bool;

    public function addImage(ProductImageInterface $image): void;

    public function removeImage(ProductImageInterface $image): void;
}
