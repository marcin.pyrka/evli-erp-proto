<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Elvi\SyliusProductComponent\Model\ProductFamilyInterface as BaseProductFamilyInterface;

interface ProductFamilyInterface extends BaseProductFamilyInterface
{
}
