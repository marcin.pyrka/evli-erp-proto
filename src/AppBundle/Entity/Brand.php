<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Elvi\SyliusProductComponent\Model\BrandTrait;

class Brand implements BrandInterface
{
    use BrandTrait;
}
