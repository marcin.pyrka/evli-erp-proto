<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Elvi\SyliusProductComponent\Model\ProductAttributeValueTrait;
use Sylius\Component\Product\Model\ProductAttributeValue as BaseProductAttributeValue;

class ProductAttributeValue extends BaseProductAttributeValue implements ProductAttributeValueInterface
{
    use ProductAttributeValueTrait;
}
