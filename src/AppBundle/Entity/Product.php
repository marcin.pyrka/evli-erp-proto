<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Elvi\SyliusProductComponent\Model\ProductTrait;
use Sylius\Component\Product\Model\Product as BaseProduct;
use Sylius\Component\Taxonomy\Model\TaxonInterface;

class Product extends BaseProduct implements ProductInterface
{
    use ProductTrait;

    /** @var Collection|TaxonInterface[] */
    private $taxons;

    /** @var Collection|ProductImageInterface[] */
    private $images;

    public function __construct()
    {
        parent::__construct();

        $this->taxons = new ArrayCollection();
        $this->images = new ArrayCollection();
    }

    public function getProductType(): ?string
    {
        return $this->productType;
    }

    public function setProductType(string $productType): void
    {
        $this->productType = $productType;
    }

    public function addTaxon(TaxonInterface $taxon): void
    {
        if (!$this->hasTaxon($taxon)) {
            $this->taxons->add($taxon);
        }
    }

    public function removeTaxon(TaxonInterface $taxon): void
    {
        $this->taxons->remove($taxon);
    }

    public function getTaxons(): Collection
    {
        return $this->taxons;
    }

    public function hasTaxon(TaxonInterface $taxon): bool
    {
        return $this->taxons->contains($taxon);
    }

    public function getImages(): Collection
    {
        return $this->images;
    }

    public function hasImage(ProductImageInterface $image): bool
    {
        return $this->images->contains($image);
    }

    public function addImage(ProductImageInterface $image): void
    {
        $image->setProduct($this);
        $this->images->add($image);
    }

    public function removeImage(ProductImageInterface $image): void
    {
        if ($this->hasImage($image)) {
            $image->setProduct(null);
            $this->images->removeElement($image);
        }
    }

    /**
     * @return string
     */
    public function getProductCategory(): ?string
    {
        return $this->productCategory;
    }

    /**
     * @param string $productCategory
     */
    public function setProductCategory(?string $productCategory): void
    {
        $this->productCategory = $productCategory;
    }

    /**
     * @return string
     */
    public function getLensType(): ?string
    {
        return $this->lensType;
    }

    /**
     * @param string $lensType
     */
    public function setLensType(?string $lensType): void
    {
        $this->lensType = $lensType;
    }

    /**
     * @return string
     */
    public function getLensUsage(): string
    {
        return $this->lensUsage;
    }

    /**
     * @param string $lensUsage
     */
    public function setLensUsage(?string $lensUsage): void
    {
        $this->lensUsage = $lensUsage;
    }

    /**
     * @return int
     */
    public function getReminderCycle(): int
    {
        return $this->reminderCycle;
    }

    /**
     * @param int $reminderCycle
     */
    public function setReminderCycle(?int $reminderCycle): void
    {
        $this->reminderCycle = $reminderCycle;
    }
}
