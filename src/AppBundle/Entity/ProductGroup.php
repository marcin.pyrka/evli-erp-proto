<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Elvi\SyliusProductComponent\Model\ProductGroupTrait;

class ProductGroup implements ProductGroupInterface
{
    use ProductGroupTrait;
}
