<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Elvi\SyliusProductComponent\Model\ProductFamilyTrait;

class ProductFamily implements ProductFamilyInterface
{
    use ProductFamilyTrait;
}
