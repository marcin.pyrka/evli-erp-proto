<?php

declare(strict_types=1);

namespace AppBundle\CommandHandler;

use AppBundle\Command\AddOrderCommand;
use AppBundle\Doctrine\ORM\Repository\FulFillerRepositoryInterface;
use AppBundle\Doctrine\ORM\Repository\OrderRepositoryInterface;
use AppBundle\Factory\OrderFactoryInterface;

final class AddOrderCommandHandler
{
    /** @var OrderRepositoryInterface */
    private $orderRepository;

    /** @var OrderFactoryInterface */
    private $orderFactory;

    /** @var FulFillerRepositoryInterface */
    private $fulFillerRepository;

    public function __construct(
        OrderRepositoryInterface $orderRepository,
        OrderFactoryInterface $orderFactory,
        FulFillerRepositoryInterface $fulFillerRepository
    ) {
        $this->orderRepository = $orderRepository;
        $this->orderFactory = $orderFactory;
        $this->fulFillerRepository = $fulFillerRepository;
    }

    public function __invoke(AddOrderCommand $command): void
    {
        $fulFiller = $this->fulFillerRepository->findOneById($command->getFulFillerId());
        if (null === $fulFiller) {
            throw new \InvalidArgumentException('Ful filler with given Id not found!');
        }

        $order = $this->orderFactory->create(
            $command->getOrderUuid(),
            $command->getAppId(),
            $fulFiller,
            $command->getCheckoutCompletedAt()
        );
        $this->orderRepository->save($order);
    }
}
