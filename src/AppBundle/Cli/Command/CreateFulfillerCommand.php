<?php

declare(strict_types=1);

namespace AppBundle\Cli\Command;

use AppBundle\Builder\Fulfiller\FulfillerBuilderInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

final class CreateFulfillerCommand extends Command
{
    /** @var FulfillerBuilderInterface */
    private $fulfillerBuilder;

    public function __construct(FulfillerBuilderInterface $fulfillerBuilder)
    {
        parent::__construct();

        $this->fulfillerBuilder = $fulfillerBuilder;
    }

    protected function configure(): void
    {
        $this
            ->setName('app:create:fulfiller')
            ->setDescription('Creates fulfiller')
            ->addOption('id', null, InputOption::VALUE_REQUIRED, 'Fulfiller ID')
            ->addOption('title', null, InputOption::VALUE_REQUIRED, 'Fulfiller Title');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeLn(date('Y-m-d H:i:s') . ' - <comment>Creating fulfiller....<comment>');

        $id = $input->getOption('id');
        $title = $input->getOption('title');

        $this->fulfillerBuilder->build($id, $title);

        $output->writeLn(date('Y-m-d H:i:s') . ' - <info>Done!<info>');
    }
}
