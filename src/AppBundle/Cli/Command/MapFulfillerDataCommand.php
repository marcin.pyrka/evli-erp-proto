<?php

declare(strict_types=1);

namespace AppBundle\Cli\Command;

use AppBundle\Builder\Fulfiller\FulfillerDataMapBuilderInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

final class MapFulfillerDataCommand extends Command
{
    /** @var FulfillerDataMapBuilderInterface */
    private $fulfillerDataMapBuilder;

    public function __construct(FulfillerDataMapBuilderInterface $fulfillerDataMapBuilder)
    {
        parent::__construct();

        $this->fulfillerDataMapBuilder = $fulfillerDataMapBuilder;
    }

    protected function configure(): void
    {
        $this
            ->setName('app:map:fulfillerdata')
            ->setDescription('Maps fulfiller data')
            ->addOption('productid', null, InputOption::VALUE_OPTIONAL, 'Fulfiller product id')
            ->addOption('debug', null, InputOption::VALUE_OPTIONAL, 'Output debug data?');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeLn(date('Y-m-d H:i:s') . ' - <comment>Mapping fulfiller options to elvi options...<comment>');
        //todo: pass it via parameters
        $this->fulfillerDataMapBuilder->setFulfillerId('lensvision');
        $this->fulfillerDataMapBuilder->setOutput($output);
        if ($productId = $input->getOption('productid')) {
            $this->fulfillerDataMapBuilder->setProcessProductId($productId);
        }

        if ($debug = $input->getOption('debug')) {
            $this->fulfillerDataMapBuilder->setDebug($debug);
        }
        $this->fulfillerDataMapBuilder->build();
        $output->writeLn(date('Y-m-d H:i:s') . ' - <info>Done!<info>');
    }
}
