<?php

declare(strict_types=1);

namespace AppBundle\Cli\Command;

use AppBundle\Builder\Fulfiller\MappingRuleBuilderInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

final class ImportMappingRulesCommand extends Command
{
    /** @var MappingRuleBuilderInterface */
    private $mappingRuleBuilder;

    public function __construct(MappingRuleBuilderInterface $mappingRuleBuilder)
    {
        parent::__construct();

        $this->mappingRuleBuilder = $mappingRuleBuilder;
    }

    protected function configure(): void
    {
        $this
            ->setName('app:import:mappingrules')
            ->setDescription('Imports mapping rules from csv')
            ->addOption('csvUrl', null, InputOption::VALUE_OPTIONAL, 'Url to csv file');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeLn(date('Y-m-d H:i:s') . ' - <comment>Importing mapping rules from csv....<comment>');

        $csvFile = $input->getOption('csvUrl');

        if (!$csvFile) {
            $output->writeln('Please, provide url to csv!');
            exit(1);
        }

        $file_headers = @get_headers($csvFile);

        if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $output->writeln('Csv was not found on given url!');
            exit(1);
        }

        $oInput = fopen($csvFile, 'r');

        $aKeys = fgetcsv($oInput);

        if (!$aKeys[0] == 'id') {
            $output->writeln('Invalid csv file');
            exit();
        }

        while (($row = fgetcsv($oInput)) !== false) {
            // skip empty lines
            if ($row[4] == '') {
                continue;
            }

            $this->mappingRuleBuilder->build($row);
        }

        fclose($oInput);
        $output->writeLn(date('Y-m-d H:i:s') . ' - <info>Done!<info>');
    }
}
