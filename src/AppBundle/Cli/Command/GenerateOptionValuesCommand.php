<?php

declare(strict_types=1);

namespace AppBundle\Cli\Command;

use AppBundle\Builder\Fulfiller\OptionValueBuilderInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class GenerateOptionValuesCommand extends Command
{
    /** @var OptionValueBuilderInterface */
    private $optionValueBuilder;

    public function __construct(OptionValueBuilderInterface $optionValueBuilder)
    {
        parent::__construct();

        $this->optionValueBuilder = $optionValueBuilder;
    }

    protected function configure(): void
    {
        $this
            ->setName('app:generate:optionvalues')
            ->setDescription('Generates option values');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeLn(date('Y-m-d H:i:s') . ' - <comment>Generating option values...<comment>');
        $this->optionValueBuilder->build();
        $output->writeLn(date('Y-m-d H:i:s') . ' - <info>Done!<info>');
    }
}
