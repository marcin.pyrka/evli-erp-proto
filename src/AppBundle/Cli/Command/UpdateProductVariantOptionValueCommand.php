<?php

declare(strict_types=1);

namespace AppBundle\Cli\Command;

use AppBundle\Builder\ProductVariantOptionValueIndexBuilderInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class UpdateProductVariantOptionValueCommand extends Command
{
    /** @var ProductVariantOptionValueIndexBuilderInterface */
    private $productVariantOptionValueIndexBuilder;

    public function __construct(ProductVariantOptionValueIndexBuilderInterface $productVariantOptionValueIndexBuilder)
    {
        parent::__construct();

        $this->productVariantOptionValueIndexBuilder = $productVariantOptionValueIndexBuilder;
    }

    protected function configure(): void
    {
        $this
            ->setName('app:update:index')
            ->setDescription('Update Product Variant Option Value Index Table')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $this->productVariantOptionValueIndexBuilder->build();
    }
}
