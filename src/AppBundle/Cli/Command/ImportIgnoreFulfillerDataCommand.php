<?php

declare(strict_types=1);

namespace AppBundle\Cli\Command;

use AppBundle\Builder\Fulfiller\IgnoreProductBuilderInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

final class ImportIgnoreFulfillerDataCommand extends Command
{
    /** @var IgnoreProductBuilderInterface */
    private $ignoreProductBuilder;

    /** @var OutputInterface */
    private $debugOutput;

    public function __construct(IgnoreProductBuilderInterface $ignoreProductBuilder)
    {
        parent::__construct();

        $this->ignoreProductBuilder = $ignoreProductBuilder;
    }

    protected function configure(): void
    {
        $this
            ->setName('app:import:fulfillerignores')
            ->setDescription('Imports flags for data which sould be ignored')
            ->addOption('type', null, InputOption::VALUE_REQUIRED, 'Type (product)')
            ->addOption('csvUrl', null, InputOption::VALUE_OPTIONAL, 'Url to csv file');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeLn(date('Y-m-d H:i:s') . ' - <comment>Importing ignore data from csv....<comment>');
        $this->_debugOutput = $output;
        if (!$type = $input->getOption('type')) {
            $output->writeln('Please, provide data type!');
            exit(1);
        }

        $csvFile = $input->getOption('csvUrl');

        if (!$csvFile) {
            $output->writeln('Please, provide url to csv!');
            exit(1);
        }

        $file_headers = @get_headers($csvFile);

        if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $output->writeln('Csv was not found on given url!');
            exit(1);
        }

        switch ($type) {
            case 'product':
                $this->_importProductIgnores($csvFile);

                break;
            default:
        }
    }

    /**
     * Import product ignores
     *
     * @param $csvFile
     */
    protected function _importProductIgnores($csvFile)
    {
        $oInput = fopen($csvFile, 'r');

        $aKeys = fgetcsv($oInput);

        if ($aKeys[0] != 'fulfiller_product_id') {
            $this->debugOutput->writeln('Invalid csv file');
            exit();
        }

        $cnt = 0;
        while (($row = fgetcsv($oInput)) !== false) {
            // skip empty lines
            if ($row[0] == '') {
                continue;
            }
            ++$cnt;
            $this->ignoreProductBuilder->build($row);
        }

        fclose($oInput);
        $this->_debugOutput->writeLn(date('Y-m-d H:i:s') . ' - Rows processed: ' . $cnt);
        $this->_debugOutput->writeLn(date('Y-m-d H:i:s') . ' - <info>Done!<info>');
    }
}
