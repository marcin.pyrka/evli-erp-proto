<?php

declare(strict_types=1);

namespace AppBundle\Cli\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

final class GenerateParentTemplateConfigCommand extends Command
{
    protected function configure(): void
    {
        $this
            ->setName('app:generate:parentymltemplate')
            ->setDescription('Generates parent yml templates from given csv')
            ->addOption('csvFile', null, InputOption::VALUE_OPTIONAL, 'Path to csv file')
            ->addOption('ymlFile', null, InputOption::VALUE_OPTIONAL, 'Path to yml file')
            ->addOption('csvUrl', null, InputOption::VALUE_OPTIONAL, 'Url to csv file');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeLn(date('Y-m-d H:i:s') . ' - <comment>Updating product yml template from csv....<comment>');
        if (!$input->getOption('csvFile') && !$input->getOption('csvUrl')) {
            $output->writeln('Csv was not found on given url!');
            exit(1);
        }

        $csvFile = $input->getOption('csvFile');
        $ymlFile = $input->getOption('ymlFile');

        if (!$csvFile) {
            $csvFile = $input->getOption('csvUrl');

            $file_headers = @get_headers($csvFile);

            if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                $output->writeln('Csv was not found on given url!');
                exit(1);
            }
        } else {
            if (!file_exists($csvFile)) {
                $output->writeln('Input file does not exist!');
                exit(1);
            }
        }

        if (!$ymlFile) {
            $ymlFile = 'src/AppBundle/Resources/config/product_templates.yml';
        }

        if (!isset($ymlFile)) {
            $output->writeln('Please enter output file name as a second argument for this script.!');
            exit(2);
        }

        $oInput = fopen($csvFile, 'r');

        $aKeys = fgetcsv($oInput);

        if (!($aKeys[0] == 'oxid')) {
            $output->writeln('Invalid csv file');
            exit();
        }
        $ymlFileTmp = $ymlFile.'.tmp';

        $oOutput = fopen($ymlFileTmp, 'wr');

        fwrite($oOutput, 'parameters:' . PHP_EOL);
        fwrite($oOutput, '    product_templates:' . PHP_EOL);
        $aDublicateKeys = [];
        while (($aLine = fgetcsv($oInput)) !== false) {

            // skip empty lines
            if ($aLine[0] == '') {
                continue;
            }
            $aLine = $this->prepareRow($aKeys, $aLine);

            $sKey = $aLine['key'];

            if (isset($aDublicateKeys[$sKey])) {
                $output->writeln('Duplicate line found in CSV: ' . $sKey . ' (oxid = ' . $aLine['oxid'] . ' vs ' . $aDublicateKeys[$sKey] . ')');

                continue;
            }
            $aDublicateKeys[$sKey] = $aLine['key'];

            fwrite($oOutput, "        $sKey:" . PHP_EOL);
            fwrite($oOutput, '            title: "' . $aLine['title'] . '"' . PHP_EOL);
            fwrite($oOutput, '            unit: "' . $aLine['unit'] . '"' . PHP_EOL);
            fwrite($oOutput, '            quantity: ' . $aLine['quantity'] . PHP_EOL);
            fwrite($oOutput, '            reminder_cycle: ' . $aLine['reminder_cycle'] . PHP_EOL);
            fwrite($oOutput, '            group: ' . $aLine['group'] . PHP_EOL);
            fwrite($oOutput, '            family: ' . $aLine['family'] . PHP_EOL);
            fwrite($oOutput, '            lens_usage: ' . $aLine['lens_usage'] . PHP_EOL);
            fwrite($oOutput, '            trial: ' . ($aLine['trial'] == 1 ? 'true' : 'false') . PHP_EOL);
            fwrite($oOutput, '            canonical: ' . ($aLine['canonical'] == 1 ? 'true' : 'false') . PHP_EOL);
            fwrite($oOutput, '            manufacturer: "' . $aLine['manufacturer'] . '"' . PHP_EOL);
            fwrite($oOutput, '            lens_type: "' . $aLine['lens_type'] . '"' . PHP_EOL);
            fwrite($oOutput, '            product_category: "' . $aLine['product_category'] . '"' . PHP_EOL);
        }

        fclose($oInput);
        fclose($oOutput);
        rename($ymlFileTmp, $ymlFile);
        $output->writeLn(date('Y-m-d H:i:s') . ' - <info>Done!<info>');
    }

    public function prepareRow($keys, $data)
    {
        $i = 0;
        $return = [];
        foreach($keys as $key) {
            $return[$key] = (isset($data[$i]) ? $data[$i] : '');
            $i++;
        }
        return $return;
    }
}
