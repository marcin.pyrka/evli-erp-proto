<?php

declare(strict_types=1);

namespace AppBundle\Cli\Command;

use AppBundle\Builder\Fulfiller\ProductParentBuilderInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class GenerateParentProductsCommand extends Command
{
    /** @var ProductParentBuilderInterface */
    private $productParentBuilder;

    public function __construct(ProductParentBuilderInterface $productParentBuilder)
    {
        parent::__construct();

        $this->productParentBuilder = $productParentBuilder;
    }

    protected function configure(): void
    {
        $this
            ->setName('app:generate:parents')
            ->setDescription('Generates parent products from templates');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeLn(date('Y-m-d H:i:s') . ' - <comment>Generating parent products from templates...<comment>');
        $this->productParentBuilder->build();
        $output->writeLn(date('Y-m-d H:i:s') . ' - <info>Done!<info>');
    }
}
