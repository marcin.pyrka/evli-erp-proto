<?php

declare(strict_types=1);

namespace AppBundle\Factory;

use AppBundle\Entity\Fulfiller\Fulfiller;
use AppBundle\Entity\Order;
use AppBundle\Entity\ValueObject\OrderAppId;
use AppBundle\Entity\ValueObject\OrderUuid;
use AppBundle\Exception\OrderAlreadyExistsException;
use AppBundle\Validator\OrderUuidSpecificationInterface;

final class OrderFactory implements OrderFactoryInterface
{
    /** @var OrderUuidSpecificationInterface */
    private $specification;

    public function __construct(OrderUuidSpecificationInterface $specification)
    {
        $this->specification = $specification;
    }

    public function create(
        OrderUuid $uuid,
        OrderAppId $appId,
        Fulfiller $fulFiller,
        \DateTimeImmutable $completedAt
    ): Order {
        if (!$this->specification->isSatisfiedBy($uuid)) {
            throw OrderAlreadyExistsException::withOrderUuid((string) $uuid);
        }

        return new Order($uuid, $appId, $fulFiller, $completedAt);
    }
}
