<?php

declare(strict_types=1);

namespace AppBundle\Factory;

use AppBundle\Entity\Fulfiller\Fulfiller;
use AppBundle\Entity\Order;
use AppBundle\Entity\ValueObject\OrderAppId;
use AppBundle\Entity\ValueObject\OrderUuid;

interface OrderFactoryInterface
{
    public function create(
        OrderUuid $uuid,
        OrderAppId $appId,
        Fulfiller $fulFiller,
        \DateTimeImmutable $completedAt
    ): Order;
}
