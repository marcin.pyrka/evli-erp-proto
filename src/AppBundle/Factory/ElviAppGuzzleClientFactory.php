<?php

declare(strict_types=1);

namespace AppBundle\Factory;

use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\RequestStack;

final class ElviAppGuzzleClientFactory
{
    /**
     * @param RequestStack $requestStack
     * @param string $uri
     * @param string $version
     *
     * @return Client
     */
    public static function create(RequestStack $requestStack, string $uri, string $version): Client
    {
        $uri = str_replace('{host}', $requestStack->getMasterRequest()->getHttpHost(), $uri);

        return new Client(['base_uri' => sprintf('%s/%s/', $uri, $version)]);
    }
}
