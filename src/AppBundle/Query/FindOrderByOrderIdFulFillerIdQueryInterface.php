<?php

declare(strict_types=1);

namespace AppBundle\Query;

use AppBundle\Query\Model\Order;

interface FindOrderByOrderIdFulFillerIdQueryInterface
{
    public function __invoke(string $orderUuid, string $fulFillerId): Order;
}
