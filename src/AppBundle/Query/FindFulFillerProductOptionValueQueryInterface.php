<?php

declare(strict_types=1);

namespace AppBundle\Query;

interface FindFulFillerProductOptionValueQueryInterface
{
    public function __invoke(string $code, string $optionName, string $optionValue): array;
}
