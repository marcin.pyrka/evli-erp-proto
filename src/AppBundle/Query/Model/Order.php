<?php

declare(strict_types=1);

namespace AppBundle\Query\Model;

final class Order
{
    /** @var string */
    private $id;

    /** @var string */
    private $appId;

    /** @var string */
    private $fulFillerOrderId;

    public function __construct(string $id, string $appId, ?string $fulFillerOrderId)
    {
        $this->id = $id;
        $this->appId = $appId;
        $this->fulFillerOrderId = $fulFillerOrderId;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function appId(): string
    {
        return $this->appId;
    }

    public function fulFillerOrderId(): ?string
    {
        return $this->fulFillerOrderId;
    }
}
