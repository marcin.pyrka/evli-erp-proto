<?php

declare(strict_types=1);

namespace AppBundle\Query\Model;

final class FulFillerOrderProductVariantOption implements \JsonSerializable
{
    /** @var string */
    private $externalProductId;

    /** @var string */
    private $title;

    /** @var string */
    private $unit;

    /** @var int */
    private $content;

    /** @var FulFillerOrderVariantOptionValueCollection */
    private $option;

    public function __construct(
        string $externalProductId,
        string $title,
        string $unit,
        int $content,
        FulFillerOrderVariantOptionValueCollection $option
    ) {
        $this->externalProductId = $externalProductId;
        $this->title = $title;
        $this->unit = $unit;
        $this->content = $content;
        $this->option = $option;
    }

    public static function fromArray(array $product, array $options): self
    {
        $options = new FulFillerOrderVariantOptionValueCollection($options);

        return new self(
            $product['external_product_id'],
            $product['title'],
            $product['unit'],
            $product['content'],
            $options
        );
    }

    public function jsonSerialize(): array
    {
        return [
            'external_product_id' => $this->externalProductId,
            'title' => $this->title,
            'unit' => $this->unit,
            'content' => $this->content,
            'option' => $this->option,
        ];
    }
}
