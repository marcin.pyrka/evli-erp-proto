<?php

declare(strict_types=1);

namespace AppBundle\Query\Model;

use AppBundle\DTO\ExternalOrderVariantOptionValue;
use ElviERP\Common\Collection\AbstractCollection;

final class FulFillerOrderVariantOptionValueCollection extends AbstractCollection implements \JsonSerializable
{
    public function __construct(array $options = [])
    {
        foreach ($options as $option) {
            foreach ($option as $ident => $value) {
                $this->add(new ExternalOrderVariantOptionValue($ident, $value));
            }
        }
    }

    public function current(): ExternalOrderVariantOptionValue
    {
        /** @var ExternalOrderVariantOptionValue $current */
        $current = $this->returnCurrent();

        return $current;
    }

    public function add(ExternalOrderVariantOptionValue $optionValue): void
    {
        $this->doAdd($optionValue);
    }

    public function jsonSerialize(): array
    {
        return iterator_to_array($this);
    }
}
