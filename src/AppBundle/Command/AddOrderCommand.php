<?php

declare(strict_types=1);

namespace AppBundle\Command;

use AppBundle\Entity\ValueObject\OrderAppId;
use AppBundle\Entity\ValueObject\OrderUuid;

final class AddOrderCommand
{
    /** @var string */
    private $appId;

    /** @var string */
    private $orderUuid;

    /** @var string */
    private $fulFillerId;

    /** @var string */
    private $checkoutCompletedAt;

    public function __construct(string $appId, string $orderUuid, string $fulFillerId, string $checkoutCompletedAt)
    {
        $this->appId = $appId;
        $this->orderUuid = $orderUuid;
        $this->fulFillerId = $fulFillerId;
        $this->checkoutCompletedAt = $checkoutCompletedAt;
    }

    public static function create(
        string $appId,
        string $orderUuid,
        string $fulFillerId,
        string $checkoutCompletedAt
    ): self {
        return new self($appId, $orderUuid, $fulFillerId, $checkoutCompletedAt);
    }

    public function getAppId(): OrderAppId
    {
        return new OrderAppId($this->appId);
    }

    public function getOrderUuid(): OrderUuid
    {
        return new OrderUuid($this->orderUuid);
    }

    public function getCheckoutCompletedAt(): \DateTimeImmutable
    {
        return \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $this->checkoutCompletedAt);
    }

    public function getFulFillerId(): string
    {
        return $this->fulFillerId;
    }
}
