<?php

declare(strict_types=1);

namespace AppBundle\Builder;

interface ProductVariantOptionValueIndexBuilderInterface
{
    public function build(): void;
}
