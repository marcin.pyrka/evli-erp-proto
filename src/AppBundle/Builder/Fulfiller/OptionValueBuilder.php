<?php

declare(strict_types=1);

namespace AppBundle\Builder\Fulfiller;

use Doctrine\ORM\EntityManagerInterface;
use Sylius\Component\Product\Model\ProductOptionInterface;
use Sylius\Component\Product\Model\ProductOptionValue;
use Sylius\Component\Product\Repository\ProductOptionRepositoryInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

final class OptionValueBuilder implements OptionValueBuilderInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ProductOptionRepositoryInterface */
    private $productOptionRepository;

    /** @var RepositoryInterface */
    private $productOptionValueRepository;

    /** @var FactoryInterface */
    private $productOptionValueFactory;

    /** @var FactoryInterface */
    private $productOptionFactory;

    /** @var array */
    private $optionTemplates;

    /** @var array */
    private $availableLocales;

    /**
     * OptionValueBuilder constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param ProductOptionRepositoryInterface $productOptionRepository
     * @param RepositoryInterface $productOptionValueRepository
     * @param FactoryInterface $productOptionValueFactory
     * @param FactoryInterface $productOptionFactory
     * @param array $optionTemplates
     * @param array $availableLocales
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ProductOptionRepositoryInterface $productOptionRepository,
        RepositoryInterface $productOptionValueRepository,
        FactoryInterface $productOptionValueFactory,
        FactoryInterface $productOptionFactory,
        array $optionTemplates,
        array $availableLocales
    ) {
        $this->entityManager = $entityManager;
        $this->productOptionRepository = $productOptionRepository;
        $this->productOptionValueRepository = $productOptionValueRepository;
        $this->productOptionValueFactory = $productOptionValueFactory;
        $this->productOptionFactory = $productOptionFactory;
        $this->optionTemplates = $optionTemplates;
        $this->availableLocales = $availableLocales;
    }

    /**
     * Build
     */
    public function build(): void
    {
        $optionTemplates = $this->getOptionTemplates();

        foreach ($optionTemplates as $key => $optionConfig) {
            $code = md5($key);
            $productOption = $this->productOptionRepository->findOneBy(['code' => $code]);
            if (!$productOption) {
                /** @var ProductOptionInterface $option */
                $productOption = $this->productOptionFactory->createNew();
                $productOption->setCode($code);
                $productOption->setIdent($optionConfig['title']);
                $productOption->setPosition($optionConfig['position']);
                foreach ($this->availableLocales as $locale) {
                    $productOption->setCurrentLocale($locale);
                    $productOption->setFallbackLocale($locale);
                    $productOption->setName($optionConfig['title']);
                }

                $this->entityManager->persist($productOption);
                $this->entityManager->flush();
            }
            if (isset($optionConfig['ranges'])) {
                $ranges = $optionConfig['ranges'];

                foreach ($ranges as $rangeKey => $rangeConfig) {
                    $val = $rangeConfig['range_min'];
                    $step = (float)($rangeConfig['range_step']);
                    while ($val <= $rangeConfig['range_max']) {
                        $optionValueCode = md5($productOption->getName() . $val);
                        $optionValue = $this->productOptionValueRepository->findOneBy(['code' => $optionValueCode]);
                        if (!$optionValue) {
                            /** @var ProductOptionValue $optionValue */
                            $optionValue = $this->productOptionValueFactory->createNew();
                            $optionValue->setCode($optionValueCode);
                            $optionValue->setOption($productOption);
                            if ($rangeConfig['range_type'] == 'float') {
                                $formatedVal = number_format(
                                    (float)$val,
                                    strlen(strrchr((string)$rangeConfig['range_step'], '.')) - 1, '.', '');
                            } else {
                                $formatedVal = $val;
                            }
                            $optionValue->setRawValue((string)((float)$formatedVal));
                            // echo $rangeConfig['range_step'].' -> '.$val.' -> '.$formatedVal."\r\n";
                            foreach ($this->availableLocales as $locale) {
                                $optionValue->setCurrentLocale($locale);
                                $optionValue->setFallbackLocale($locale);
                                $optionValue->setValue((string)$formatedVal);
                            }

                            $this->entityManager->persist($optionValue);
                        }

                        $val = $val + $step;
                    }
                    $this->entityManager->flush();
                }
            }

            if (isset($optionConfig['option_list'])) {
                foreach($optionConfig['option_list'] as $val) {
                    $optionValueCode = md5($productOption->getName() . $val);
                    $optionValue = $this->productOptionValueRepository->findOneBy(['code' => $optionValueCode]);

                    if (!$optionValue) {
                        /** @var ProductOptionValue $optionValue */
                        $optionValue = $this->productOptionValueFactory->createNew();
                        $optionValue->setCode($optionValueCode);
                        $optionValue->setOption($productOption);
                        $optionValue->setRawValue((string)$val);

                        foreach ($this->availableLocales as $locale) {
                            $optionValue->setCurrentLocale($locale);
                            $optionValue->setFallbackLocale($locale);
                            $optionValue->setValue((string)$val);
                        }

                        $this->entityManager->persist($optionValue);
                    }
                }

            }
        }
    }

    /**
     * Returns option template
     *
     * @param $optionCode
     *
     * @return array
     */
    public function getOptionTemplate($optionCode): array
    {
        if (isset($this->optionTemplates[$optionCode])) {
            return $this->optionTemplates[$optionCode];
        }

        return false;
    }

    /**
     * returns option templates
     *
     * @return array
     */
    public function getOptionTemplates(): array
    {
        return $this->optionTemplates;
    }
}
