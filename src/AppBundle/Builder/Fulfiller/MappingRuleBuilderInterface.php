<?php

declare(strict_types=1);

namespace AppBundle\Builder\Fulfiller;

interface MappingRuleBuilderInterface
{
    public function build($data = []): void;
}
