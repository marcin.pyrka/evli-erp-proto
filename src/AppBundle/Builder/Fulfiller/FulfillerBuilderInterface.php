<?php

declare(strict_types=1);

namespace AppBundle\Builder\Fulfiller;

interface FulfillerBuilderInterface
{
    public function build(string $fulfillerId, string $fulfillerName): void;
}
