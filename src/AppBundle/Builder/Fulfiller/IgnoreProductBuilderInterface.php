<?php

declare(strict_types=1);

namespace AppBundle\Builder\Fulfiller;

interface IgnoreProductBuilderInterface
{
    public function build(): void;
}
