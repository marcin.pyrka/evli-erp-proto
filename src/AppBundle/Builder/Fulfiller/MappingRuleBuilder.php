<?php

declare(strict_types=1);

namespace AppBundle\Builder\Fulfiller;

use Doctrine\ORM\EntityManagerInterface;

final class MappingRuleBuilder implements MappingRuleBuilderInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * MappinRuleBuilder constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    /**
     * Build
     *
     * @param array $data
     *
     * @throws
     */
    public function build($data = []): void
    {
        $id = md5($data[1] . $data[3] . $data[4] . $data[5] . $data[6]);

        $mapping_data = [
            'id' => $id,
            'fulfiller_product_id' => ($data[1] != '' ? $data[1] : null),
            'fulfiller_id' => $data[3],
            'type' => $data[4],
            'source_string' => $data[5],
            'target_string' => $data[6],
        ];

        $insert_placeholders = implode(', ', array_fill(0, count($mapping_data), '?'));
        $query = 'REPLACE INTO 
                fulfiller_transformation_rule(`' . implode('`, `', array_keys($mapping_data)) . '`) VALUES(' . $insert_placeholders . ')';

        $this->entityManager->getConnection()->executeQuery(
            $query,
            array_values($mapping_data)
        );
    }
}
