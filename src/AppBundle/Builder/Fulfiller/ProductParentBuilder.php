<?php

declare(strict_types=1);

namespace AppBundle\Builder\Fulfiller;

use Doctrine\ORM\EntityManagerInterface;
use Elvi\ImportBundle\Resolver\ImportResourceResolverInterface;
use Elvi\ImportExportBundle\Resolver\ResourceResolverInterface;
use Sylius\Bundle\ProductBundle\Doctrine\ORM\ProductRepository;
use Sylius\Component\Product\Generator\SlugGenerator;

final class ProductParentBuilder implements ProductParentBuilderInterface
{
    /** @var ImportResourceResolverInterface */
    private $productResourceResolver;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var array */
    private $productTemplates;

    /** @var array */
    private $availableLocales;

    /** @var SlugGenerator */
    private $slugGenerator;

    /** @var ProductRepository */
    private $productRepository;

    /** @var ImportResourceResolverInterface */
    private $productGroupResourceResolver;

    /** @var ImportResourceResolverInterface */
    private $productFamilyResourceResolver;

    /** @var ImportResourceResolverInterface */
    private $brandResourceResolver;

    /**
     * OptionValueBuilder constructor.
     *
     * @param ResourceResolverInterface $productResourceResolver
     * @param EntityManagerInterface $entityManager
     * @param ProductRepository $productRepository
     * @param SlugGenerator $slugGenerator
     * @param ResourceResolverInterface $productFamilyResourceResolver
     * @param ResourceResolverInterface $productGroupResourceResolver
     * @param ResourceResolverInterface $brandResourceResolver
     * @param array $productTemplates
     * @param array $availableLocales
     */
    public function __construct(
        ResourceResolverInterface $productResourceResolver,
        EntityManagerInterface $entityManager,
        ProductRepository $productRepository,
        SlugGenerator $slugGenerator,
        ResourceResolverInterface $productFamilyResourceResolver,
        ResourceResolverInterface $productGroupResourceResolver,
        ResourceResolverInterface $brandResourceResolver,
        array $productTemplates,
        array $availableLocales
    ) {
        $this->productResourceResolver = $productResourceResolver;
        $this->entityManager = $entityManager;
        $this->productRepository = $productRepository;
        $this->productTemplates = $productTemplates;
        $this->productGroupResourceResolver = $productGroupResourceResolver;
        $this->productFamilyResourceResolver = $productFamilyResourceResolver;
        $this->brandResourceResolver = $brandResourceResolver;
        $this->slugGenerator = $slugGenerator;
        $this->availableLocales = $availableLocales;
    }

    /**
     * Build
     */
    public function build(): void
    {
        $productTemplates = $this->getProductsTemplate();
        foreach ($productTemplates as $productCode => $productTemplate) {
            $productBrand = null;
            $productFamily = null;
            $productGroup = null;
            $productCode = (string)$productCode;
            if ($productTemplate['manufacturer']) {
                $brandCode = md5($productTemplate['manufacturer']);
                /** @var BrandInterface $brand */
                $productBrand = $this->brandResourceResolver->resolveResource($brandCode);

                $productBrand->setCode($brandCode);
                $productBrand->setName($productTemplate['manufacturer']);

                $productBrand->getId() ?: $this->entityManager->persist($productBrand);
            }

            if ($productTemplate['family']) {
                $productFamilyCode = md5($productTemplate['family']);

                /** @var ProductFamilyInterface $productFamily */
                $productFamily = $this->productFamilyResourceResolver->resolveResource($productFamilyCode);

                $productFamily->setCode($productFamilyCode);
                $productFamily->setBrand($productBrand);
                $productFamily->setName($productTemplate['family']);

                $productFamily->getId() ?: $this->entityManager->persist($productFamily);
            }

            if ($productTemplate['group']) {
                $productGroupCode = md5($productTemplate['group']);
                /** @var ProductGroupInterface $productGroup */
                $productGroup = $this->productGroupResourceResolver->resolveResource($productGroupCode);

                $productGroup->setCode($productGroupCode);
                if ($productFamily) {
                    $productGroup->setFamily($productFamily);
                }
                $productGroup->setName($productTemplate['group']);

                $productGroup->getId() ?: $this->entityManager->persist($productGroup);
            }

            $product = $this->productResourceResolver->resolveResource($productCode);

            $product->setCode($productCode);
            $product->setEnabled(true);
            $product->setPrice(0);
            $product->setOriginalPrice(0);
            if ($productGroup) {
                $product->setGroup($productGroup);
            }
            $product->setQuantity($productTemplate['quantity']);
            $product->setUnit($productTemplate['unit']);
            $product->setTrial($productTemplate['trial']);
            $product->setCanonical($productTemplate['canonical']);
            $product->setLensType($productTemplate['lens_type']);
            $product->setProductCategory($productTemplate['product_category']);
            $product->setReminderCycle($productTemplate['reminder_cycle']);
            $product->setLensUsage($productTemplate['lens_usage']);
            $title = $productTemplate['title'];
            $slug = $this->slugGenerator->generate($title);

            foreach ($this->availableLocales as $locale) {
                $product->setCurrentLocale($locale);
                $product->setFallbackLocale($locale);
                $product->setName($title);
                $product->setSlug($slug);
            }
            $this->entityManager->persist($product);
            $this->entityManager->flush();
        }

        $this->entityManager->flush();
    }

    /**
     * returns option templates
     *
     * @return array
     */
    public function getProductsTemplate(): array
    {
        return $this->productTemplates;
    }
}
