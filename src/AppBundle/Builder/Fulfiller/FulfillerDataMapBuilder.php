<?php

declare(strict_types=1);

namespace AppBundle\Builder\Fulfiller;

use Doctrine\ORM\EntityManagerInterface;
use Sylius\Bundle\ProductBundle\Doctrine\ORM\ProductRepository;

final class FulfillerDataMapBuilder implements FulfillerDataMapBuilderInterface
{
    /**
     * Empty space placeholder for mapping rules
     * @var string
     */
    public const EMPTY_SPACE_PLACEHOLDER = '____EMPTY_SPACE____';

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ProductRepository */
    private $productRepository;

    /** @var string */
    private $fulfillerId;

    /** @var array */
    private $_transformationRules;

    /** @var string */
    private $processProductId;

    /** @var OutputInterface */
    private $output;

    /** @var debug */
    private $debug;

    /** @var array */
    private $notFoundProducts;

    /** @var bool */
    private $debugHeaderOutputed = false;

    /**
     * Fulfiller data mapping to elvi products and options
     *
     * @param EntityManagerInterface $entityManager
     * @param ProductRepository $productRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ProductRepository $productRepository
    ) {
        $this->entityManager = $entityManager;
        $this->productRepository = $productRepository;
    }

    /**
     * Build
     */
    public function build(): void
    {
        // 1. generate mappings
        $this->_mapFulfillerData();
        // 2. generate app_product_option_values records
        $this->_generateAppProductOptionValues();

        // 3. calculate stats
        $this->_claculateStats();
    }

    /**
     * Calculate stats
     */
    private function _claculateStats()
    {
        $query = '
                    SELECT
                           fulfiller_raw_product.id,
                           SUM(IF(fteovm.id IS NULL, 0, 1)) matched_option_value,
                           COUNT(DISTINCT frpo.id) as       total_option_value
                    FROM
                        fulfiller_raw_product
                    LEFT JOIN
                        fulfiller_raw_product_option frpo on fulfiller_raw_product.id = frpo.product_id
                    LEFT JOIN
                        fulfiller_raw_option_value frov on frpo.option_value_id = frov.id
                    LEFT JOIN
                        fulfiller_raw_option fro ON fro.id = frov.option_id
                    LEFT JOIN
                        fulfiller_to_elvi_option_value_map fteovm on (frpo.product_id = fteovm.fulfiller_product_id AND
                                                                              frov.id = fteovm.fulfiller_option_value_id AND
                                                                              frov.option_id = fteovm.fulfiller_option_id)
                    WHERE
                           fulfiller_raw_product.ignore != 1 AND
                           frov.ignore != 1 AND
                           fro.ignore != 1
                    GROUP BY 
                           fulfiller_raw_product.id
        ';

        $connection = $this->entityManager->getConnection();

        $stmt = $connection->prepare($query);
        $stmt->execute();

        while ($row = $stmt->fetch()) {
            $query = 'UPDATE fulfiller_raw_product SET total_option_values_cnt = ?, matched_option_values_cnt = ? WHERE id = ?';

            $connection->executeQuery(
               $query,
               [
                   $row['total_option_value'],
                   $row['matched_option_value'],
                   $row['id'],
               ]
            );
        }

        $query = 'UPDATE
                      fulfiller f,
                      (
                        SELECT
                               SUM(IF(p.total_option_values_cnt > 0 AND p.matched_option_values_cnt = p.total_option_values_cnt, 1, 0)) as matched_cnt,
                               COUNT(DISTINCT p.id) as total_cnt
                        FROM
                             fulfiller_raw_product p
                        WHERE
                            p.fulfiller_id = ? AND 
                            p.ignore != 1
                        GROUP BY
                            p.fulfiller_id
                      ) Stats
                    SET
                        f.matched_product_cnt = Stats.matched_cnt,
                        f.total_product_cnt = Stats.total_cnt
        ';

        $connection->executeQuery(
            $query,
            [
                $this->getFulfillerId(),
            ]
        );
    }

    /**
     * Maps fulfiller data
     */
    private function _mapFulfillerData()
    {
        $where = [];
        if ($processProductId = $this->getProcessProductId()) {
            $where[] = 'p.id = :product_id';
        }
        $query = 'SELECT 
                    p.id as product_id,
                    o.id as option_id,
                    ov.id as option_value_id,
                    p.title as product_title,
                    o.title as option_title,
                    ov.title as option_value_title
                  FROM 
                      fulfiller_raw_product p
                  INNER JOIN
                      fulfiller_raw_product_option po ON po.product_id = p.id
                  INNER JOIN
                      fulfiller_raw_option_value ov ON ov.id = po.option_value_id
                  INNER JOIN
                      fulfiller_raw_option o ON o.id = ov.option_id
                  LEFT JOIN
                      fulfiller_to_elvi_option_value_map fteovm on (p.id = fteovm.fulfiller_product_id AND o.id = fteovm.fulfiller_option_id AND fteovm.fulfiller_option_value_id = ov.id)
                  WHERE
                      fteovm.id IS NULL AND 
                      p.ignore != 1 AND 
                      o.ignore != 1 AND
                      ov.ignore != 1
                  ';
        if ($where) {
            $query .= ' AND ' . implode(' AND ', $where);
        }

        $connection = $this->entityManager->getConnection();

        $stmt = $connection->prepare($query);
        if ($processProductId) {
            $stmt->bindValue('product_id', $processProductId);
        }
        $stmt->execute();

        while ($row = $stmt->fetch()) {
            $mappingRow = $this->_buildMappingRow($row);

            if ($mappingRow) {
                $query = 'REPLACE INTO fulfiller_to_elvi_option_value_map(`' . implode('`,`', array_keys($mappingRow)) . "`) VALUES ('" . implode("','", $mappingRow) . "')";
                $this->entityManager->getConnection()->executeQuery($query);
            }
        }
    }

    /**
     * Checks the data and prepares row for inserting to mapping table
     *
     * @param $row
     *
     * @return array
     */
    private function _buildMappingRow($row)
    {
        $fulfillerProductId = $row['product_id'];
        $fulfillerOptionId = $row['option_id'];
        $fulfillerOptionValueId = $row['option_value_id'];

        $productTransformations = $this->getTransformations('product', $row['product_title'], null, $fulfillerProductId);

        $optionTransformations = $this->getTransformations('option', $row['option_title'], null, $fulfillerProductId);
        $optionValueTransformations = $this->getTransformations('option_value', $row['option_value_title'], null, $fulfillerProductId);

        $productId = $this->_getElviProductId($row['product_title'], $productTransformations);
        $optionId = $this->_getElviOptionId($row['option_title'], $optionTransformations);
        $optionValueId = $this->_getElviOptionValueId($row['option_value_title'], $optionId, $optionValueTransformations);
        // if one of those components were not found, then we can't add mapping
        if (!($productId && $optionId && $optionValueId)) {
            if ($productId && $this->getDebug()) {
                if (!$this->debugHeaderOutputed) {
                    $debugData = [
                        'timestamp',
                        'fulfiller_product_id',
                        'fulfiller_product_title',
                        'product_synonyms',
                        'elvi_product_id',
                        'fulfiller_option_title',
                        'option_synonyms',
                        'elvi_option_id',
                        'fulfiller_option_value_title',
                        'option_value_synonyms',
                        'fulfiller_option_value_id',
                    ];
                    $this->_outputDebugInCsv($debugData);
                    $this->debugHeaderOutputed = true;
                }
                $debugData = [
                    date('Y-m-d H:i:s'),
                    $fulfillerProductId,
                    $row['product_title'],
                    is_array($productTransformations) ? implode(' || ', $productTransformations) : ' - ',
                    $productId,
                    $row['option_title'],
                    is_array($optionTransformations) ? implode(' || ', $optionTransformations) : ' - ',
                    $optionId,
                    $row['option_value_title'],
                    is_array($optionValueTransformations) ? implode(' || ', $optionValueTransformations) : ' - ',
                    $optionValueId,
                ];
                $this->_outputDebugInCsv($debugData);
//                $this->getOutput()->writeLn("====================================================================================");
//                $this->getOutput()->writeLn("<info>fulfiller_product_id:</info> " . $fulfillerProductId);
//                $this->getOutput()->writeLn("<info>fulfiller_product_title:</info> " . $row['product_title']);
//                $this->getOutput()->writeLn("<info>product_synonyms:</info> " . (is_array($productTransformations) ? implode(' || ', $productTransformations) : ' - '));
//                $this->getOutput()->writeLn("<info>elvi_product_id:</info> " . $productId);
//                $this->getOutput()->writeLn("------------");
//                $this->getOutput()->writeLn("<info>fulfiller_option_title:</info> " . $row['option_title']);
//                $this->getOutput()->writeLn("<info>option_synonyms:</info> " . (is_array($optionTransformations) ? implode(' || ', $optionTransformations) : ' - '));
//                $this->getOutput()->writeLn("<info>elvi_option_id:</info> " . $optionId);
//                $this->getOutput()->writeLn("------------");
//                $this->getOutput()->writeLn("<info>fulfiller_option_value_title:</info> " . $row['option_value_title']);
//                $this->getOutput()->writeLn("<info>option_value_synonyms:</info> " . (is_array($optionValueTransformations) ? implode(' || ', $optionValueTransformations) : ' - '));
//                $this->getOutput()->writeLn("<info>fulfiller_option_value_id:</info> " . $optionValueId);
            } elseif ($this->getDebug() && !isset($this->notFoundProducts[$fulfillerProductId])) {
                $this->notFoundProducts[$fulfillerProductId] = true;
                $this->getOutput()->writeLn('====================================================================================');
                $this->getOutput()->writeLn('<error>Not found product (' . $fulfillerProductId . '):</error> ' . $row['product_title']);
            }

            return false;
        }

        $mappingRow = [
            'id' => md5($fulfillerProductId . $fulfillerOptionId . $fulfillerOptionValueId),
            'fulfiller_id' => $this->getFulfillerId(),
            'fulfiller_product_id' => $fulfillerProductId,
            'elvi_product_id' => $productId,
            'fulfiller_option_id' => $fulfillerOptionId,
            'fulfiller_option_value_id' => $fulfillerOptionValueId,
            'elvi_option_id' => $optionId,
            'elvi_option_value_id' => $optionValueId,
            'is_mapped' => '1',
            'mapping_type' => 'one_to_one',
            'created_at' => date('Y-m-d H:i:s'),
        ];

        return $mappingRow;
    }

    /**
     * Returns elvi parent product id by title
     *
     * @param $name
     * @param $synonyms
     *
     * @throws
     *
     * @return string
     */
    private function _getElviProductId($name, $synonyms = null)
    {
        $ident = md5($name);
        $type = 'product';

        if (!isset($this->_cachedData[$type][$ident])) {
            $query = 'SELECT translatable_id FROM sylius_product_translation WHERE name = :name LIMIT 1';
            $connection = $this->entityManager->getConnection();
            $stmt = $connection->prepare($query);
            $stmt->bindValue('name', $name);
            $stmt->execute();
            $id = $stmt->fetchColumn();

            // if we couldn't find by main name, then let's try via synonims/transformation rules
            if (!$id & $synonyms !== null) {
                $query = 'SELECT translatable_id FROM sylius_product_translation WHERE name IN(?) LIMIT 1';

                $connection = $this->entityManager->getConnection();
                $stmt = $connection->executeQuery(
                    $query,
                    [array_values($synonyms)],
                    [\Doctrine\DBAL\Connection::PARAM_INT_ARRAY]
                );
                $id = $stmt->fetchColumn();
            }

            if ($id) {
                $this->_cachedData[$type][$ident] = $id;
            }
        }

        if (isset($this->_cachedData[$type][$ident])) {
            return $this->_cachedData[$type][$ident];
        }

        return false;
    }

    /**
     * Returns elvi option id by title
     *
     * @param $name
     * @param $synonyms
     *
     * @throws
     *
     * @return string
     */
    private function _getElviOptionId($name, $synonyms = null)
    {
        $ident = md5($name);
        $type = 'option';

        if (!isset($this->_cachedData[$type][$ident])) {
            $query = 'SELECT translatable_id FROM sylius_product_option_translation WHERE name = :name LIMIT 1';
            $connection = $this->entityManager->getConnection();
            $stmt = $connection->prepare($query);
            $stmt->bindValue('name', $name);
            $stmt->execute();
            $id = $stmt->fetchColumn();

            // if we couldn't find by main name, then let's try via synonims/transformation rules
            if (!$id && $synonyms !== null) {
                $query = 'SELECT translatable_id FROM sylius_product_option_translation WHERE name IN (?) LIMIT 1';
                $connection = $this->entityManager->getConnection();
                $stmt = $connection->executeQuery(
                    $query,
                    [array_values($synonyms)],
                    [\Doctrine\DBAL\Connection::PARAM_INT_ARRAY]
                );
                $id = $stmt->fetchColumn();
            }

            if ($id) {
                $this->_cachedData[$type][$ident] = $id;
            }
        }

        if (isset($this->_cachedData[$type][$ident])) {
            return $this->_cachedData[$type][$ident];
        }

        return false;
    }

    /**
     * Returns elvi option id by value
     *
     * @param $value
     * @param $optionId
     * @param $synonyms
     *
     * @throws
     *
     * @return string
     */
    private function _getElviOptionValueId($value, $optionId = null, $synonyms = null)
    {
        $value = str_replace(['°', '+'], '', $value);
        $ident = md5($value.$optionId);
        $type = 'option_value';

        if (!isset($this->_cachedData[$type][$ident])) {
            $query = 'SELECT 
                           id 
                      FROM 
                           sylius_product_option_value v
                      WHERE 
                           ';

            if (is_numeric($value)) {
                $query .= '((raw_value = :value) OR  (CAST(raw_value AS DECIMAL(6,2)) = CAST(:value AS DECIMAL(6,2))))';
            } else {
                $query .= '(raw_value = :value)';
            }

            if ($optionId) {
                $query .= ' AND v.option_id = :optionId';
            }

            $query .= ' LIMIT 1';
            $connection = $this->entityManager->getConnection();
            $stmt = $connection->prepare($query);
            $stmt->bindValue('value', $value);
            if ($optionId) {
                $stmt->bindValue('optionId', $optionId);
            }
            $stmt->execute();
            $id = $stmt->fetchColumn();

            // if we couldn't find by main name, then let's try via synonims/transformation rules
            if (!$id && $synonyms !== null) {
                $query = 'SELECT 
                           id 
                      FROM 
                           sylius_product_option_value v
                      WHERE 
                           raw_value IN (?)';
                if ($optionId) {
                    $query .= ' AND v.option_id = ?';
                }

                $query .= ' LIMIT 1';
                $connection = $this->entityManager->getConnection();
                $stmt = $connection->prepare($query);
                $params = [];
                $types = [];
                $params[] = array_values($synonyms);
                $types[] = \Doctrine\DBAL\Connection::PARAM_STR_ARRAY;

                if ($optionId) {
                    $params[] = $optionId;
                    $types[] = \PDO::PARAM_STR;
                }
                $stmt = $connection->executeQuery($query,
                    $params,
                    $types
                    );

                $id = $stmt->fetchColumn();
            }

            if ($id) {
                $this->_cachedData[$type][$ident] = $id;
            }
        }

        if (isset($this->_cachedData[$type][$ident])) {
            return $this->_cachedData[$type][$ident];
        }

        return false;
    }

    /**
     * Returns synonims/transformation rules for given string and type
     *
     * @param $type
     * @param $string
     * @param $fulfillerId
     * @param $fulfillerProductId
     *
     * @throws
     *
     * @return mixed
     */
    public function getTransformations($type, $string, $fulfillerId = null, $fulfillerProductId = null)
    {
        if (!$fulfillerId) {
            $fulfillerId = $this->getFulfillerId();
        }

        $ident = $fulfillerId . $type . $fulfillerProductId;

        if (!isset($this->_transformationRules[$ident])) {
            $query = 'SELECT 
                          source_string, 
                          target_string,
                          fulfiller_product_id
                      FROM 
                          fulfiller_transformation_rule 
                      WHERE 
                          fulfiller_id = :fulfillerId AND 
                          type = :type
            ';
            if ($fulfillerProductId) {
                $query .= ' AND (fulfiller_product_id = :product_id OR fulfiller_product_id IS NULL)';
            }
            $query .= ' ORDER BY fulfiller_product_id DESC';
            $connection = $this->entityManager->getConnection();
            $stmt = $connection->prepare($query);
            $stmt->bindValue('fulfillerId', $fulfillerId);
            $stmt->bindValue('type', $type);
            if ($fulfillerProductId) {
                $stmt->bindValue('product_id', $fulfillerProductId);
            }
            $stmt->execute();
            $ignoreWithoutProductId = false;
            while ($row = $stmt->fetch()) {
                $targetString = $row['target_string'];
                if ($targetString == '') {
                    $targetString = self::EMPTY_SPACE_PLACEHOLDER;
                }
                if ($fulfillerProductId && $row['fulfiller_product_id'] == $fulfillerProductId) {
                    $this->_transformationRules[$ident][$row['source_string']][$targetString] = $row['target_string'];
                    $ignoreWithoutProductId = true;
                } elseif (!$ignoreWithoutProductId) {
                    // in case when row is not for specified fulfiller product id, we need to check if this string is not yet translated with product specific rule
                    if (!isset($this->_transformationRules[$ident][$row['source_string']][$targetString])) {
                        $this->_transformationRules[$ident][$row['source_string']][$targetString] = $row['target_string'];
                    }
                }
            }
        }

        $replacement = false;

        if (isset($this->_transformationRules[$ident][$string])) {
            $replacement =  $this->_transformationRules[$ident][$string];
        } elseif (is_array($this->_transformationRules[$ident])) {
            foreach ($this->_transformationRules[$ident] as $sourceString => $targetStrings) {
                if (stripos($string, $sourceString) !== false) {
                    $replacement = $targetStrings;
                }
            }
        }

        if ($replacement) {
            return $replacement;
        }

        return null;
    }

    /**
     * Returns current fulfiller id
     *
     * @return string
     */
    public function getFulfillerId(): string
    {
        return $this->fulfillerId;
    }

    /**
     * Sets current fulfiller id
     *
     * @param $fulfillerId
     */
    public function setFulfillerId($fulfillerId)
    {
        $this->fulfillerId = $fulfillerId;
    }

    /**
     * Get Single fulfiller product id which should be processed
     *
     * @return string
     */
    public function getProcessProductId()
    {
        return $this->processProductId;
    }

    /**
     * Set single fulfiller product id which should be processed
     *
     * @param string $processProductId
     */
    public function setProcessProductId($processProductId)
    {
        $this->processProductId = $processProductId;
    }

    /**
     * @return OutputInterface
     */
    public function getOutput()
    {
        return $this->output;
    }

    /**
     * @param OutputInterface $output
     */
    public function setOutput($output)
    {
        $this->output = $output;
    }

    /**
     * @return debug
     */
    public function getDebug()
    {
        return $this->debug;
    }

    /**
     * @param debug $debug
     */
    public function setDebug($debug)
    {
        $this->debug = $debug;
    }

    /**
     * Map parent products
     */
    private function _mapParentProducts()
    {
        $query = "
          REPLACE INTO fulfiller_to_elvi_option_value_map
          SELECT
                md5(CONCAT(p.id,t.translatable_id)),
                p.id,
                t.translatable_id,
                null,
                null,
                null,
                null,
                null,
                1,
                'one_to_one'
            FROM
                fulfiller_raw_product p
            LEFT JOIN
                sylius_product_translation t ON t.name = p.title
            WHERE
                t.id IS NOT NULL

        ";
        $connection = $this->entityManager->getConnection();
        $connection->executeQuery($query);
    }

    /**
     * Map parent products
     */
    private function _mapProductValues()
    {
        $query = "
          REPLACE INTO fulfiller_to_elvi_option_value_map
          SELECT
                md5(CONCAT(p.id, opt.id, optval.id)),
                p.id,
                t.translatable_id,
                opt.id,
                optval.id,
                sylius_product_option_translation.translatable_id,
                sylius_product_option_value.id,
                '',
                1,
                'one_to_one'
            FROM
                fulfiller_raw_product p
            LEFT JOIN
                fulfiller_raw_product_variant v ON v.parent_id = p.id
            LEFT JOIN
                fulfiller_raw_variant_option varopt ON varopt.variant_id = v.id
            LEFT JOIN
                fulfiller_raw_option_value optval ON optval.id = varopt.option_value_id
            LEFT JOIN
                fulfiller_raw_option opt ON opt.id = optval.option_id
            LEFT JOIN
                sylius_product_translation t ON t.name = p.title
            LEFT JOIN
                sylius_product_option_translation ON sylius_product_option_translation.name = opt.title
            LEFT JOIN
                sylius_product_option_value ON sylius_product_option_value.option_id =  sylius_product_option_translation.translatable_id
            LEFT JOIN
                sylius_product_option_value_translation ON sylius_product_option_value_translation.translatable_id = sylius_product_option_value.id
            WHERE
                t.id IS NOT NULL AND
                sylius_product_option_value_translation.value = optval.title
            GROUP BY
                v.id, opt.id, optval.id
        ";
        $connection = $this->entityManager->getConnection();
        $connection->executeQuery($query);
    }

    /**
     * Generates product option values by mapped products
     */
    private function _generateAppProductOptionValues()
    {
        $query = '
            REPLACE INTO app_product_option_values
            SELECT
              m.elvi_product_id,
              m.elvi_option_value_id                     
            FROM
              fulfiller_to_elvi_option_value_map m 
            WHERE
              m.fulfiller_option_value_id IS NOT NULL
            GROUP BY
              m.elvi_product_id,
              m.elvi_option_value_id
        ';

        $connection = $this->entityManager->getConnection();
        $connection->executeQuery($query);
    }

    /**
     * Outputs debug data in csv format
     *
     * @param array $debugData
     */
    private function _outputDebugInCsv($debugData = [])
    {
        $this->getOutput()->writeLn(implode(', ', $debugData));
    }
}
