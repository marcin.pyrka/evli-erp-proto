<?php

declare(strict_types=1);

namespace AppBundle\Builder\Fulfiller;

interface ProductParentBuilderInterface
{
    public function build(): void;
}
