<?php

declare(strict_types=1);

namespace AppBundle\Builder\Fulfiller;

use AppBundle\Entity\Fulfiller\Fulfiller;
use AppBundle\Doctrine\ORM\Repository\FulFillerRepositoryInterface;

final class FulfillerBuilder implements FulfillerBuilderInterface
{
    /** @var FulFillerRepositoryInterface */
    private $fulfillerRepository;

    public function __construct(FulFillerRepositoryInterface $fulfillerRepository)
    {
        $this->fulfillerRepository = $fulfillerRepository;
    }

    public function build(string $fulfillerId, string $fulfillerName): void
    {
        $fulfiller = $this->fulfillerRepository->findOneById($fulfillerId);

        if (!$fulfiller) {
            $fulfiller = new Fulfiller();
            $fulfiller->setId($fulfillerId);
        }

        $fulfiller->setTitle($fulfillerName);
        $this->fulfillerRepository->save($fulfiller);
    }
}
