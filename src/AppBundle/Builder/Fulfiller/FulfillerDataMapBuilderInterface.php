<?php

declare(strict_types=1);

namespace AppBundle\Builder\Fulfiller;

interface FulfillerDataMapBuilderInterface
{
    public function build(): void;
}
