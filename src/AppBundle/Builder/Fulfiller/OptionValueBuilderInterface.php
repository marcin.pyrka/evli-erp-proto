<?php

declare(strict_types=1);

namespace AppBundle\Builder\Fulfiller;

interface OptionValueBuilderInterface
{
    public function build(): void;
}
