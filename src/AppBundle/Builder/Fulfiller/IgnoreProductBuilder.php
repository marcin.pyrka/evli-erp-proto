<?php

declare(strict_types=1);

namespace AppBundle\Builder\Fulfiller;

use Doctrine\ORM\EntityManagerInterface;

final class IgnoreProductBuilder implements IgnoreProductBuilderInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * MappinRuleBuilder constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    /**
     * Build
     *
     * @param array $data
     *
     * @throws
     */
    public function build($data = []): void
    {
        $productId = $data[0];

        $query = 'UPDATE fulfiller_raw_product SET `ignore`=1 WHERE id = ?';

        $this->entityManager->getConnection()->executeQuery(
            $query,
            [$productId]
        );
    }
}
