<?php

declare(strict_types=1);

namespace AppBundle\Builder;

use AppBundle\Entity\ProductVariantInterface;
use AppBundle\Entity\ProductVariantOptionValueInterface;
use Doctrine\ORM\EntityManagerInterface;
use Sylius\Component\Product\Repository\ProductVariantRepositoryInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

final class ProductVariantOptionValueIndexBuilder implements ProductVariantOptionValueIndexBuilderInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ProductVariantRepositoryInterface */
    private $productVariantRepository;

    /** @var RepositoryInterface */
    private $productVariantOptionValueRepository;

    /** @var FactoryInterface */
    private $productVariantOptionValueFactory;

    public function __construct(
        EntityManagerInterface $entityManager,
        ProductVariantRepositoryInterface $productVariantRepository,
        RepositoryInterface $productVariantOptionValueRepository,
        FactoryInterface $productVariantOptionValueFactory
    ) {
        $this->entityManager = $entityManager;
        $this->productVariantRepository = $productVariantRepository;
        $this->productVariantOptionValueRepository = $productVariantOptionValueRepository;
        $this->productVariantOptionValueFactory = $productVariantOptionValueFactory;
    }

//    @TODO: do it cleaner if possible
    public function build(): void
    {
        $productVariants = $this->productVariantRepository->findAll();

        /** @var ProductVariantInterface $productVariant */
        foreach ($productVariants as $productVariant) {
            /** @var ProductVariantOptionValueInterface $productVariantOptionValue */
            $productVariantOptionValue = $this->productVariantOptionValueFactory->createNew();

            $productVariantOptionValue->setProduct($productVariant->getProduct());
            $productVariantOptionValue->setVariant($productVariant);

            $variantOptionValues = $productVariant->getOptionValues();

            foreach ($variantOptionValues as $variantOptionValue) {
                $option = $variantOptionValue->getOption();
                if (null !== $option) {
                    $setter = 'set' . ucwords(strtolower($option->getName()));
                    $productVariantOptionValue->$setter((float) $variantOptionValue->getValue());
                }
            }

            $this->entityManager->persist($productVariantOptionValue);
        }

        $this->entityManager->flush();
    }
}
