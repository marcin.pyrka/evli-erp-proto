<?php declare(strict_types=1);

namespace AppBundle\UI\Http\CustomerConnector\Measurement;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\UI\Http\CustomerConnector\HttpHelperTrait;
use Symfony\Component\HttpFoundation\Response;

class DeleteMeasurementHttp
{
    use HttpHelperTrait;

    /**
     * @param string $measurementId
     *
     * @return Response
     */
    public function __invoke(string $measurementId): Response
    {
        $uuid = $this->uuid();
        $this->request($uuid, HttpEndpoint::DELETE_MEASUREMENT(), ['measurementId' => $measurementId]);

        return $this->response($uuid);
    }
}
