<?php declare(strict_types=1);

namespace AppBundle\UI\Http\CustomerConnector\Measurement;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\Infrastructure\Helper\Json;
use AppBundle\UI\Http\CustomerConnector\HttpHelperTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UpdateMeasurementHttp
{
    use HttpHelperTrait;

    /**
     * @param Request $request
     * @param string $measurementId
     *
     * @return Response
     */
    public function __invoke(Request $request, string $measurementId): Response
    {
        $uuid = $this->uuid();
        $data = Json::decode($request->getContent());
        $this->request($uuid, HttpEndpoint::UPDATE_MEASUREMENT(), ['measurementId' => $measurementId], $data);

        return $this->response($uuid);
    }
}
