<?php declare(strict_types=1);

namespace AppBundle\UI\Http\CustomerConnector\Measurement;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\UI\Http\CustomerConnector\HttpHelperTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GetMeasurementHttp
{
    use HttpHelperTrait;

    /**
     * @param Request $request
     * @param string $measurementId
     *
     * @return Response
     */
    public function __invoke(Request $request, string $measurementId): Response
    {
        $uuid = $this->uuid();
        $this->request($uuid, HttpEndpoint::GET_MEASUREMENT(), array_merge($request->query->all(), [
            'measurementId' => $measurementId,
        ]));

        return $this->response($uuid);
    }
}
