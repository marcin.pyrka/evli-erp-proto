<?php declare(strict_types=1);

namespace AppBundle\UI\Http\CustomerConnector\Customer;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\Infrastructure\Helper\Json;
use AppBundle\UI\Http\CustomerConnector\HttpHelperTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UpdateCustomerHttp
{
    use HttpHelperTrait;

    /**
     * @param Request $request
     * @param string $customerId
     *
     * @return Response
     */
    public function __invoke(Request $request, string $customerId): Response
    {
        $uuid = $this->uuid();
        $data = Json::decode($request->getContent());
        $this->request($uuid, HttpEndpoint::UPDATE_CUSTOMER(), ['customerId' => $customerId], $data);

        return $this->response($uuid);
    }
}
