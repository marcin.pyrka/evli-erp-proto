<?php declare(strict_types=1);

namespace AppBundle\UI\Http\CustomerConnector\Customer;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\UI\Http\CustomerConnector\HttpHelperTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GetCustomerListHttp
{
    use HttpHelperTrait;

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        $uuid = $this->uuid();
        $this->request($uuid, HttpEndpoint::GET_CUSTOMER_LIST(), $request->query->all());

        return $this->response($uuid);
    }
}
