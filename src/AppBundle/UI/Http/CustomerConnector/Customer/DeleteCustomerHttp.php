<?php declare(strict_types=1);

namespace AppBundle\UI\Http\CustomerConnector\Customer;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\UI\Http\CustomerConnector\HttpHelperTrait;
use Symfony\Component\HttpFoundation\Response;

class DeleteCustomerHttp
{
    use HttpHelperTrait;

    /**
     * @param string $customerId
     *
     * @return Response
     */
    public function __invoke(string $customerId): Response
    {
        $uuid = $this->uuid();
        $this->request($uuid, HttpEndpoint::DELETE_CUSTOMER(),['customerId' => $customerId]);

        return $this->response($uuid);
    }
}
