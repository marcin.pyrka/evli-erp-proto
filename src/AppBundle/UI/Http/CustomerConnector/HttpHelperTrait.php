<?php declare(strict_types=1);

namespace AppBundle\UI\Http\CustomerConnector;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\Application\CustomerConnector\UseCase\FetchResponse\FetchResponse;
use AppBundle\Application\CustomerConnector\UseCase\InsertRequest\InsertRequest;
use Persist\BusBundle\CommandBus\CommandBusAwareTrait;
use Persist\BusBundle\CommandBus\CommandBusInterface;
use Persist\BusBundle\QueryBus\QueryBusAwareTrait;
use Persist\BusBundle\QueryBus\QueryBusInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

trait HttpHelperTrait
{
    use CommandBusAwareTrait;
    use QueryBusAwareTrait;

    /**
     * @param CommandBusInterface $commandBus
     * @param QueryBusInterface $queryBus
     */
    public function __construct(CommandBusInterface $commandBus, QueryBusInterface $queryBus)
    {
        $this->setCommandBus($commandBus);
        $this->setQueryBus($queryBus);
    }

    /**
     * @return UuidInterface
     */
    protected function uuid(): UuidInterface
    {
        return Uuid::uuid1();
    }

    /**
     * @param UuidInterface $uuid
     * @param HttpEndpoint $endpoint
     * @param array $params
     * @param array $data
     */
    protected function request(UuidInterface $uuid, HttpEndpoint $endpoint, array $params = [], array $data = []): void
    {
        $this->handleCommand(InsertRequest::create($uuid, $endpoint, $this->clean($params), $data));
    }

    /**
     * @param array $data
     *
     * @return array
     */
    protected function clean(array $data): array
    {
        unset($data['key']);

        return $data;
    }

    /**
     * @param UuidInterface $uuid
     *
     * @return JsonResponse
     */
    protected function response(UuidInterface $uuid): JsonResponse
    {
        $response = $this->handleQuery(FetchResponse::create($uuid->toString()));

        return new JsonResponse($response->getData(), $response->getStatusCode());
    }
}
