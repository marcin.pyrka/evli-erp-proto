<?php declare(strict_types=1);

namespace AppBundle\UI\Http\CustomerConnector\Note;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\Infrastructure\Helper\Json;
use AppBundle\UI\Http\CustomerConnector\HttpHelperTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CreateNoteHttp
{
    use HttpHelperTrait;

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        $uuid = $this->uuid();
        $this->request($uuid, HttpEndpoint::CREATE_NOTE(), [], Json::decode($request->getContent()));

        return $this->response($uuid);
    }
}
