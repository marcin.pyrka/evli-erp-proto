<?php declare(strict_types=1);

namespace AppBundle\UI\Http\CustomerConnector\Note;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\UI\Http\CustomerConnector\HttpHelperTrait;
use Symfony\Component\HttpFoundation\Response;

class DeleteNoteHttp
{
    use HttpHelperTrait;

    /**
     * @param string $noteId
     *
     * @return Response
     */
    public function __invoke(string $noteId): Response
    {
        $uuid = $this->uuid();
        $this->request($uuid, HttpEndpoint::DELETE_NOTE(), ['noteId' => $noteId]);

        return $this->response($uuid);
    }
}
