<?php declare(strict_types=1);

namespace AppBundle\UI\Http\CustomerConnector\Note;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\UI\Http\CustomerConnector\HttpHelperTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GetNoteListHttp
{
    use HttpHelperTrait;

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        $uuid = $this->uuid();
        $this->request($uuid, HttpEndpoint::GET_NOTE_LIST(), $request->query->all());

        return $this->response($uuid);
    }
}
