<?php declare(strict_types=1);

namespace AppBundle\UI\Http\CustomerConnector\Note;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\Infrastructure\Helper\Json;
use AppBundle\UI\Http\CustomerConnector\HttpHelperTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UpdateNoteHttp
{
    use HttpHelperTrait;

    /**
     * @param Request $request
     * @param string $noteId
     *
     * @return Response
     */
    public function __invoke(Request $request, string $noteId): Response
    {
        $uuid = $this->uuid();
        $this->request($uuid, HttpEndpoint::UPDATE_NOTE(), ['noteId' => $noteId], Json::decode($request->getContent()));

        return $this->response($uuid);
    }
}
