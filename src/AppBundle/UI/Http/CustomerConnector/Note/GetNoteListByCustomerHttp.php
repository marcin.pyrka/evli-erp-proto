<?php declare(strict_types=1);

namespace AppBundle\UI\Http\CustomerConnector\Note;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\UI\Http\CustomerConnector\HttpHelperTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GetNoteListByCustomerHttp
{
    use HttpHelperTrait;

    /**
     * @param Request $request
     * @param string $customerId
     *
     * @return Response
     */
    public function __invoke(Request $request, string $customerId): Response
    {
        $uuid = $this->uuid();
        $this->request($uuid, HttpEndpoint::GET_NOTE_LIST_BY_CUSTOMER(), array_merge($request->query->all(), [
            'customerId' => $customerId,
        ]));

        return $this->response($uuid);
    }
}
