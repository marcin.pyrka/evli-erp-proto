<?php declare(strict_types=1);

namespace AppBundle\UI\Cli\CustomerConnector;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\Application\CustomerConnector\UseCase\InsertRequest\InsertRequest;
use Persist\BusBundle\CommandBus\CommandBusAwareTrait;
use Persist\BusBundle\CommandBus\CommandBusInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InsertRequestCli extends Command
{
    use CommandBusAwareTrait;

    /**
     * @inheritDoc
     */
    public function __construct(CommandBusInterface $commandBus)
    {
        $this->setCommandBus($commandBus);
        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('customer-connector:insert-request');
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->handleCommand(InsertRequest::create(Uuid::uuid1(), HttpEndpoint::CREATE_CUSTOMER(), [], $this->data()));
    }

    /**
     * @return array
     */
    private function data(): array
    {
        return [
            'external_id' => '482',
            'gender' => 1,
            'first_name' => 'Peter',
            'last_name' => 'Meier',
            'company_name' => 'Some Company',
            'email' => 'demo1234@demo.ch',
            'mobile' => '+41772758274',
            'birth_date' => '1981-02-24',
            'language' => 'de',
        ];
    }
}
