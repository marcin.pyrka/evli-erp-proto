<?php declare(strict_types=1);

namespace AppBundle\UI\Cli\CustomerConnector;

use Persist\BusBundle\QueryBus\QueryBusAwareTrait;
use Persist\BusBundle\QueryBus\QueryBusInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Application\CustomerConnector\UseCase\FetchResponse\FetchResponse;

class FetchResponseCli extends Command
{
    use QueryBusAwareTrait;

    /**
     * @inheritDoc
     */
    public function __construct(QueryBusInterface $queryBus)
    {
        $this->setQueryBus($queryBus);
        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('customer-connector:fetch-response');
        $this->addArgument('id', InputArgument::REQUIRED);
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->handleQuery(FetchResponse::create($input->getArgument('id')));
    }
}
