<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use Elvi\ImportExportBundle\Importer\AbstractImporter;
use Elvi\ImportExportBundle\Resolver\ResourceResolverInterface;
use Sylius\Component\Product\Model\ProductOptionInterface;
use Sylius\Component\Product\Model\ProductOptionValueInterface;
use Sylius\Component\Product\Repository\ProductOptionRepositoryInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Webmozart\Assert\Assert;

final class ProductOptionValueImporter extends AbstractImporter implements ProductOptionValueImporterInterface
{
    /** @var ProductOptionRepositoryInterface */
    private $productOptionRepository;

    /** @var ResourceResolverInterface */
    private $productOptionValueResourceResolver;

    /** @var array */
    private $availableLocales;

    public function __construct(
        ProductOptionRepositoryInterface $productOptionRepository,
        ResourceResolverInterface $productOptionValueResourceResolver,
        array $availableLocales
    ) {
        $this->productOptionRepository = $productOptionRepository;
        $this->productOptionValueResourceResolver = $productOptionValueResourceResolver;
        $this->availableLocales = $availableLocales;
    }

    public function import(array $row): ?ResourceInterface
    {
        $optionCode = $this->getColumnValue(self::OPTION_CODE_COLUMN, $row);
        /** @var ProductOptionInterface $productOption */
        $productOption = $this->productOptionRepository->findOneBy(['code' => $optionCode]);
        Assert::notNull($productOption, sprintf(
            'Product option with %s code does not exist.',
            $optionCode
        ));

        $code = $this->getColumnValue(self::CODE_COLUMN, $row);
        /** @var ProductOptionValueInterface $productOptionValue */
        $productOptionValue = $this->productOptionValueResourceResolver->resolveResource($code);

        $productOptionValue->setCode($code);
        $productOptionValue->setOption($productOption);

        foreach ($this->availableLocales as $locale) {
            $productOptionValue->setCurrentLocale($locale);
            $productOptionValue->setFallbackLocale($locale);
            $productOptionValue->setValue($this->getColumnValue(self::VALUE_COLUMN, $row));
        }

        return $productOptionValue;
    }

    public function getResourceCode(): string
    {
        return 'product_option_value';
    }
}
