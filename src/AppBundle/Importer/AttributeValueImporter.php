<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use AppBundle\Entity\ProductAttributeValueInterface;
use Elvi\ImportExportBundle\Importer\AbstractImporter;
use Sylius\Component\Product\Model\ProductAttributeInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

final class AttributeValueImporter extends AbstractImporter implements AttributeValueImporterInterface
{
    /** @var RepositoryInterface */
    private $productAttributeRepository;

    public function __construct(RepositoryInterface $productAttributeRepository)
    {
        $this->productAttributeRepository = $productAttributeRepository;
    }

    public function import(array $row): ?ResourceInterface
    {
        $code = $this->getColumnValue(self::CODE_COLUMN, $row);
        $attributeCode = $this->getColumnValue(self::ATTRIBUTE_CODE_COLUMN, $row);
        /** @var ProductAttributeValueInterface $productAttributeValue */
        $productAttributeValue = $this->productAttributeRepository->findOneBy(['code' => $code]);
        /** @var ProductAttributeInterface $productAttribute */
        $productAttribute = $this->productAttributeRepository->findOneBy(['code' => $attributeCode]);

        if (null === $productAttributeValue || null === $productAttribute) {
            return null;
        }

        $productAttributeValue->setCode($code);
        $productAttributeValue->setAttribute($productAttribute);

        foreach ($this->getAvailableLocales([self::TEXT_VALUE_COLUMN], array_keys($row)) as $locale) {
            $textValue = $this->getTranslatableColumnValue(self::TEXT_VALUE_COLUMN, $locale, $row);

            $productAttributeValue->setValue($textValue);
            $productAttributeValue->setLocaleCode($locale);
        }

        return $productAttributeValue;
    }

    public function getResourceCode(): string
    {
        return 'attribute_value';
    }
}
