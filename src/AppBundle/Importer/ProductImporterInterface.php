<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use Elvi\ImportExportBundle\Importer\ImporterInterface;

interface ProductImporterInterface extends ImporterInterface
{
    public const CODE_COLUMN = 'ID';
    public const PRODUCT_GROUP_CODE_COLUMN = 'PRODUCT_GROUP_ID';
    public const ENABLED_COLUMN = 'ACTIVE';
    public const PRICE_COLUMN = 'PRICE';
    public const ORIGINAL_PRICE_COLUMN = 'PRICE_SALE';
    public const QUANTITY_COLUMN = 'QUANTITY';
    public const UNIT_COLUMN = 'UNIT';
    public const SAMPLE_COLUMN = 'TRIAL';
    public const IMAGE_COLUMN = 'IMAGE';
}
