<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use Elvi\ImportExportBundle\Importer\ImporterInterface;

interface TaxonImporterInterface extends ImporterInterface
{
    public const CODE_COLUMN = 'ID';
    public const NAME_COLUMN = 'TEXT__locale__';
}
