<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use Elvi\ImportExportBundle\Importer\ImporterInterface;

interface AttributeValueImporterInterface extends ImporterInterface
{
    public const CODE_COLUMN = 'ID';
    public const ATTRIBUTE_CODE_COLUMN = 'KEY_ID';
    public const TEXT_VALUE_COLUMN = 'TEXT__locale__';
}
