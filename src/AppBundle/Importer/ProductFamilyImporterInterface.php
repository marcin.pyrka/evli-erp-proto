<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use Elvi\ImportExportBundle\Importer\ImporterInterface;

interface ProductFamilyImporterInterface extends ImporterInterface
{
    public const CODE_COLUMN = 'ID';
    public const NAME_COLUMN = 'TITLE';
    public const BRAND_CODE_COLUMN = 'BRAND_ID';
}
