<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use Elvi\ImportExportBundle\Importer\ImporterInterface;

interface ProductAttributeValueImporterInterface extends ImporterInterface
{
    public const PRODUCT_ATTRIBUTE_VALUE_CODE_COLUMN = 'ATTRIBUTE_ID';
    public const PRODUCT_CODE_COLUMN = 'PRODUCT_PARENT_ID';
}
