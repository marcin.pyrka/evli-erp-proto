<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use AppBundle\Entity\ProductInterface;
use Elvi\ImportExportBundle\Importer\AbstractImporter;
use Elvi\ImportExportBundle\Resolver\ResourceResolverInterface;
use Sylius\Component\Product\Model\ProductAttributeInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Webmozart\Assert\Assert;

final class ProductsToAttributesImporter extends AbstractImporter implements ProductsToAttributesImporterInterface
{
    /** @var ResourceResolverInterface */
    private $productResourceResolver;

    /** @var ResourceResolverInterface */
    private $attributeResourceResolver;

    public function __construct(
        ResourceResolverInterface $productResourceResolver,
        ResourceResolverInterface $attributeResourceResolver
    ) {
        $this->productResourceResolver = $productResourceResolver;
        $this->attributeResourceResolver = $attributeResourceResolver;
    }

    public function import(array $row): ?ResourceInterface
    {
        /** @var ProductInterface $product */
        $product = $this->productResourceResolver->resolveResource($this->getColumnValue(self::PRODUCT_CODE_COLUMN, $row));
        /** @var ProductAttributeInterface $attribute */
        $attribute = $this->productResourceResolver->resolveResource($this->getColumnValue(self::ATTRIBUTE_CODE_COLUMN, $row));

        Assert::isInstanceOf($product, ProductInterface::class);
        Assert::isInstanceOf($attribute, ProductAttributeInterface::class);

        $product->addAttribute($attribute);

        return $product;
    }

    public function getResourceCode(): string
    {
        return 'products_to_attributes';
    }
}
