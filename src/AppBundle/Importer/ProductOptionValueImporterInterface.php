<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use Elvi\ImportExportBundle\Importer\ImporterInterface;

interface ProductOptionValueImporterInterface extends ImporterInterface
{
    public const CODE_COLUMN = 'ID';
    public const OPTION_CODE_COLUMN = 'KEY_ID';
    public const VALUE_COLUMN = 'VALUE';
}
