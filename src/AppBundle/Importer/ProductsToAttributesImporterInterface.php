<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use Elvi\ImportExportBundle\Importer\ImporterInterface;

interface ProductsToAttributesImporterInterface extends ImporterInterface
{
    public const PRODUCT_CODE_COLUMN = 'product_code';
    public const ATTRIBUTE_CODE_COLUMN = 'attribute_code';
}
