<?php

declare(strict_types=1);

namespace AppBundle\Importer;

interface OrderItemImporterInterface
{
    public const ID_COLUMN = 'id';
    public const ORDER_NUMBER_COLUMN = 'order_number';
    public const VARIANT_CODE_COLUMN = 'variant_code';
    public const QUANTITY_COLUMN = 'quantity';
    public const UNIT_PRICE_COLUMN = 'unit_price';
    public const UNITS_TOTAL_COLUMN = 'units_total';
    public const ADJUSTMENT_TOTAL_COLUMN = 'adjustments_total';
    public const ADJUSTMENTS_TOTAL_COLUMN = 'adjustments_total';
    public const TOTAL_COLUMN = 'total';
    public const PRODUCT_NAME_COLUMN = 'product_name';
    public const VARIANT_NAME_COLUMN = 'variant_name';
    public const POSITION_COLUMN = 'position';
}
