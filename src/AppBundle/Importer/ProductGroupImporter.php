<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use AppBundle\Entity\ProductFamilyInterface;
use AppBundle\Entity\ProductGroupInterface;
use Elvi\ImportExportBundle\Importer\AbstractImporter;
use Elvi\ImportExportBundle\Resolver\ResourceResolverInterface;
use Sylius\Component\Resource\Model\ResourceInterface;

final class ProductGroupImporter extends AbstractImporter implements ProductGroupImporterInterface
{
    /** @var ResourceResolverInterface */
    private $productGroupResourceResolver;

    /** @var ResourceResolverInterface */
    private $productFamilyResourceResolver;

    public function __construct(
        ResourceResolverInterface $productGroupResourceResolver,
        ResourceResolverInterface $productFamilyResourceResolver
    ) {
        $this->productGroupResourceResolver = $productGroupResourceResolver;
        $this->productFamilyResourceResolver = $productFamilyResourceResolver;
    }

    public function import(array $row): ?ResourceInterface
    {
        $code = $this->getColumnValue(self::CODE_COLUMN, $row);

        /** @var ProductGroupInterface $productGroup */
        $productGroup = $this->productGroupResourceResolver->resolveResource($code);

        $productFamilyCode = $this->getColumnValue(self::PRODUCT_FAMILY_CODE_COLUMN, $row);

        if (null !== $productFamilyCode){
            /** @var ProductFamilyInterface $productFamily */
            $productFamily = $this->productFamilyResourceResolver->resolveResource($productFamilyCode);
            $productGroup->setFamily($productFamily);
        }

        $productGroup->setCode($code);
        $productGroup->setName($this->getColumnValue(self::NAME_COLUMN, $row));

        return $productGroup;
    }

    public function getResourceCode(): string
    {
        return 'product_group';
    }
}
