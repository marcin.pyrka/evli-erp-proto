<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use Elvi\ImportExportBundle\Importer\AbstractImporter;
use Elvi\ImportExportBundle\Resolver\ResourceResolverInterface;
use Sylius\Component\Order\Model\OrderInterface;
use Sylius\Component\Resource\Model\ResourceInterface;

final class OrderImporter extends AbstractImporter implements OrderImporterInterface
{
    /** @var ResourceResolverInterface */
    private $orderResourceResolver;

    public function __construct(ResourceResolverInterface $orderResourceResolver)
    {
        $this->orderResourceResolver = $orderResourceResolver;
    }

    public function import(array $row): ResourceInterface
    {
        /** @var OrderInterface $order */
        $order = $this->orderResourceResolver->resolveResource((string) $this->getColumnValue(self::ID_COLUMN, $row));

        $order->setNumber($this->getColumnValue(self::ORDER_NUMBER_COLUMN_COLUMN, $row));
        $order->setState($this->getColumnValue(self::CHECKOUT_STATE_COLUMN, $row));
        $order->setNotes($this->getColumnValue(self::NOTES_COLUMN, $row));
        $order->setCreatedAt(new \DateTime($this->getColumnValue(self::CREATED_AT_COLUMN, $row)));
        $order->setUpdatedAt(new \DateTime($this->getColumnValue(self::UPDATED_AT_COLUMN, $row)));

        return $order;
    }

    public function getResourceCode(): string
    {
        return 'order';
    }
}
