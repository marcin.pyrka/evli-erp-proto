<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use Elvi\ImportExportBundle\Importer\AbstractImporter;
use Elvi\ImportExportBundle\Resolver\ResourceResolverInterface;
use Sylius\Component\Attribute\AttributeType\TextAttributeType;
use Sylius\Component\Product\Model\ProductAttributeInterface;
use Sylius\Component\Resource\Model\ResourceInterface;

final class ProductAttributeImporter extends AbstractImporter implements ProductAttributeImporterInterface
{
    /** @var ResourceResolverInterface */
    private $productAttributeResourceResolver;

    public function __construct(ResourceResolverInterface $productAttributeResourceResolver)
    {
        $this->productAttributeResourceResolver = $productAttributeResourceResolver;
    }

    public function import(array $row): ?ResourceInterface
    {
        $code = $this->getColumnValue(self::CODE_COLUMN, $row);
        /** @var ProductAttributeInterface $productAttribute */
        $productAttribute = $this->productAttributeResourceResolver->resolveResource($code);

        $productAttribute->setCode($code);
        $productAttribute->setPosition((int) $this->getColumnValue(self::POSITION_COLUMN, $row));
        $productAttribute->setStorageType(TextAttributeType::TYPE);

        foreach ($this->getAvailableLocales([self::NAME_COLUMN], array_keys($row)) as $locale) {
            $name = $this->getTranslatableColumnValue(self::NAME_COLUMN, $locale, $row);

            $productAttribute->setCurrentLocale($locale);
            $productAttribute->setFallbackLocale($locale);
            $productAttribute->setName($name);
        }

        return $productAttribute;
    }

    public function getResourceCode(): string
    {
        return 'product_attribute';
    }
}
