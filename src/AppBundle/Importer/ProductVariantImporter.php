<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use AppBundle\Entity\ProductInterface;
use AppBundle\Entity\ProductVariantInterface;
use Elvi\ImportExportBundle\Importer\AbstractImporter;
use Elvi\ImportExportBundle\Resolver\ResourceResolverInterface;
use Sylius\Component\Resource\Model\ResourceInterface;

final class ProductVariantImporter extends AbstractImporter implements ProductVariantImporterInterface
{
    /** @var ResourceResolverInterface */
    private $productVariantResourceResolver;

    /** @var ResourceResolverInterface */
    private $productResourceResolver;

    public function __construct(
        ResourceResolverInterface $productVariantResourceResolver,
        ResourceResolverInterface $productResourceResolver
    ) {
        $this->productVariantResourceResolver = $productVariantResourceResolver;
        $this->productResourceResolver = $productResourceResolver;
    }

    public function import(array $row): ?ResourceInterface
    {
        $code = $this->getColumnValue(self::CODE_COLUMN, $row);
        $productCode = $this->getColumnValue(self::PRODUCT_CODE_COLUMN, $row);
        /** @var ProductVariantInterface $productVariant */
        $productVariant = $this->productVariantResourceResolver->resolveResource($code);
        /** @var ProductInterface $product */
        $product = $this->productResourceResolver->resolveResource($productCode);

        $productVariant->setCode($code);
        $productVariant->setProduct($product);
        $productVariant->setEan($this->getColumnValue(self::EAN_COLUMN, $row));
        $productVariant->setCreatedAt(new \DateTime($this->getColumnValue(self::CREATED_AT_COLUMN, $row)));
        $productVariant->setPosition(1);

        return $productVariant;
    }

    public function getResourceCode(): string
    {
        return 'product_variant';
    }
}
