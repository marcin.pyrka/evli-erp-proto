<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use AppBundle\Entity\OrderItemInterface;
use AppBundle\Entity\ProductVariantInterface;
use Elvi\ImportExportBundle\Importer\AbstractImporter;
use Elvi\ImportExportBundle\Resolver\ResourceResolverInterface;
use Sylius\Component\Order\Model\OrderInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

final class OrderItemImporter extends AbstractImporter implements OrderItemImporterInterface
{
    /** @var ResourceResolverInterface */
    private $orderItemResourceResolver;

    /** @var RepositoryInterface */
    private $orderRepository;

    /** @var RepositoryInterface */
    private $productVariantRepository;

    public function __construct(
        ResourceResolverInterface $orderItemResourceResolver,
        RepositoryInterface $orderRepository,
        RepositoryInterface $productVariantRepository
    ) {
        $this->orderItemResourceResolver = $orderItemResourceResolver;
        $this->orderRepository = $orderRepository;
        $this->productVariantRepository = $productVariantRepository;
    }

    public function import(array $row): ResourceInterface
    {
        $id = $this->getColumnValue(self::ID_COLUMN, $row);
        $orderNumber = $this->getColumnValue(self::ORDER_NUMBER_COLUMN, $row);
        $variantCode = $this->getColumnValue(self::VARIANT_CODE_COLUMN, $row);
        /** @var ProductVariantInterface $variant */
        $variant = $this->productVariantRepository->findOneBy(['code' => $variantCode]);
        /** @var OrderItemInterface $orderItem */
        $orderItem = $this->orderItemResourceResolver->resolveResource((string) $id);
        /** @var OrderInterface $order */
        $order = $this->orderRepository->findOneBy(['number' => $orderNumber]);

        $orderItem->setOrder($order);
        $orderItem->setVariant($variant);
        $orderItem->setUnitPrice((int) $this->getColumnValue(self::UNIT_PRICE_COLUMN, $row));

        return $orderItem;
    }

    public function getResourceCode(): string
    {
        return 'order_item';
    }
}
