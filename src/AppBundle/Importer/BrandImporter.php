<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use AppBundle\Entity\BrandInterface;
use Elvi\ImportExportBundle\Importer\AbstractImporter;
use Elvi\ImportExportBundle\Resolver\ResourceResolverInterface;
use Sylius\Component\Resource\Model\ResourceInterface;

final class BrandImporter extends AbstractImporter implements BrandImporterInterface
{
    /** @var ResourceResolverInterface */
    private $brandResourceResolver;

    public function __construct(ResourceResolverInterface $brandResourceResolver)
    {
        $this->brandResourceResolver = $brandResourceResolver;
    }

    public function import(array $row): ResourceInterface
    {
        $code = $this->getColumnValue(self::CODE_COLUMN, $row);
        /** @var BrandInterface $brand */
        $brand = $this->brandResourceResolver->resolveResource($code);

        $brand->setCode($code);
        $brand->setName($this->getColumnValue(self::NAME_COLUMN, $row));

        return $brand;
    }

    public function getResourceCode(): string
    {
        return 'brand';
    }
}
