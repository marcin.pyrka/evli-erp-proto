<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use Elvi\ImportExportBundle\Importer\ImporterInterface;

interface ProductVariantImporterInterface extends ImporterInterface
{
    public const CODE_COLUMN = 'ID';
    public const PRODUCT_CODE_COLUMN = 'PRODUCT_PARENT_ID';
    public const EAN_COLUMN = 'EAN';
    public const CREATED_AT_COLUMN = 'CREATED_AT';
}
