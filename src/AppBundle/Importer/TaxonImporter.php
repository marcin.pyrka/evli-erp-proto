<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use AppBundle\Generator\SlugGeneratorInterface;
use Elvi\ImportExportBundle\Importer\AbstractImporter;
use Elvi\ImportExportBundle\Resolver\ResourceResolverInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Taxonomy\Model\TaxonInterface;

final class TaxonImporter extends AbstractImporter implements TaxonImporterInterface
{
    /** @var ResourceResolverInterface */
    private $taxonResourceResolver;

    /** @var SlugGeneratorInterface */
    private $slugGenerator;

    public function __construct(
        ResourceResolverInterface $taxonResourceResolver,
        SlugGeneratorInterface $slugGenerator
    ) {
        $this->taxonResourceResolver = $taxonResourceResolver;
        $this->slugGenerator = $slugGenerator;
    }

    public function import(array $row): ?ResourceInterface
    {
        $code = $this->getColumnValue(self::CODE_COLUMN, $row);
        /** @var TaxonInterface $taxon */
        $taxon = $this->taxonResourceResolver->resolveResource($code);

        $taxon->setCode($code);
        $taxon->setPosition(1);
        $taxon->setLeft(1);
        $taxon->setRight(1);
        $taxon->setLevel(1);

        foreach ($this->getAvailableLocales([self::NAME_COLUMN], array_keys($row)) as $locale) {
            $name = $this->getTranslatableColumnValue(self::NAME_COLUMN, $locale, $row);

            $taxon->setCurrentLocale($locale);
            $taxon->setFallbackLocale($locale);
            $taxon->setName($name);
            //@TODO: a lot of duplicates slugs (MySQL unique constraints errors)
            $taxon->setSlug($this->slugGenerator->generate($name));
        }

        return $taxon;
    }

    public function getResourceCode(): string
    {
        return 'taxon';
    }
}
