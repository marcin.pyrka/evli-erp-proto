<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use Elvi\ImportExportBundle\Importer\ImporterInterface;

interface ProductVariantOptionValueImporterInterface extends ImporterInterface
{
    public const PRODUCT_VARIANT_CODE_COLUMN = 'PRODUCT_VARIANT_ID';
    public const PRODUCT_OPTION_VALUE_CODE_COLUMN = 'PARAMETER_ID';
}
