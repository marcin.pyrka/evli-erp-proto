<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use Elvi\ImportExportBundle\Importer\AbstractImporter;
use Elvi\ImportExportBundle\Resolver\ResourceResolverInterface;
use Sylius\Component\Product\Model\ProductOptionInterface;
use Sylius\Component\Resource\Model\ResourceInterface;

final class ProductOptionImporter extends AbstractImporter implements ProductOptionImporterInterface
{
    /** @var ResourceResolverInterface */
    private $productOptionResourceResolver;

    public function __construct(ResourceResolverInterface $productOptionResourceResolver)
    {
        $this->productOptionResourceResolver = $productOptionResourceResolver;
    }

    public function import(array $row): ?ResourceInterface
    {
        $code = $this->getColumnValue(self::CODE_COLUMN, $row);
        /** @var ProductOptionInterface $productOption */
        $productOption = $this->productOptionResourceResolver->resolveResource($code);

        $productOption->setCode($code);
        $productOption->setPosition((int) $this->getColumnValue(self::POSITION_COLUMN, $row));

        foreach ($this->getAvailableLocales([self::NAME_COLUMN], array_keys($row)) as $locale) {
            $name = $this->getTranslatableColumnValue(self::NAME_COLUMN, $locale, $row);

            $productOption->setCurrentLocale($locale);
            $productOption->setName($name);
        }

        return $productOption;
    }

    public function getResourceCode(): string
    {
        return 'product_option';
    }
}
