<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use AppBundle\Entity\ProductAttributeValueInterface;
use AppBundle\Entity\ProductInterface;
use AppBundle\Doctrine\ORM\Repository\ProductRepositoryInterface;
use Elvi\ImportExportBundle\Importer\AbstractImporter;
use Elvi\ImportExportBundle\Resolver\ResourceResolverInterface;
use Sylius\Component\Locale\Context\LocaleContextInterface;
use Sylius\Component\Product\Model\ProductAttributeInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

final class ProductAttributeValueImporter extends AbstractImporter implements ProductAttributeValueImporterInterface
{
    /** @var ResourceResolverInterface */
    private $productAttributeValueResourceResolver;

    /** @var ProductRepositoryInterface */
    private $productRepository;

    /** @var RepositoryInterface */
    private $productAttributeRepository;

    /** @var LocaleContextInterface */
    private $localeContext;

    public function __construct(
        ResourceResolverInterface $productAttributeValueResourceResolver,
        ProductRepositoryInterface $productRepository,
        RepositoryInterface $productAttributeRepository,
        LocaleContextInterface $localeContext
    ) {
        $this->productAttributeValueResourceResolver = $productAttributeValueResourceResolver;
        $this->productRepository = $productRepository;
        $this->productAttributeRepository = $productAttributeRepository;
        $this->localeContext = $localeContext;
    }

    public function import(array $row): ?ResourceInterface
    {
        $code = $this->getColumnValue(self::PRODUCT_ATTRIBUTE_VALUE_CODE_COLUMN, $row);
        /** @var ProductAttributeValueInterface $productAttributeValue */
        $productAttributeValue = $this->productAttributeValueResourceResolver->resolveResource($code);
        /** @var ProductInterface $product */
        $product = $this->productRepository->findOneBy(['code' => $this->getColumnValue(self::PRODUCT_CODE_COLUMN, $row)]);
        /** @var ProductAttributeInterface $productAttribute */
        $productAttribute = $this->productAttributeRepository->findOneBy([]);

        if (null === $product || null === $productAttribute) {
            return null;
        }

        //@TODO: Code(ATTRIBUTE_ID) should be unique? A lot of duplicates into .csv  (MySQL unique constraints errors)
        $productAttributeValue->setCode($code);
        $productAttributeValue->setProduct($product);
        $productAttributeValue->setLocaleCode($this->localeContext->getLocaleCode());
        $productAttributeValue->setAttribute($productAttribute);

        return $productAttributeValue;
    }

    public function getResourceCode(): string
    {
        return 'product_attribute_value';
    }
}
