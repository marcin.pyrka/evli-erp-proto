<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use Elvi\ImportExportBundle\Importer\ImporterInterface;

interface OrderImporterInterface extends ImporterInterface
{
    public const ID_COLUMN = 'id';
    public const ORDER_NUMBER_COLUMN_COLUMN = 'order_number';
    public const SHIPPING_ADDRESS_ID_COLUMN = 'shipping_order_id';
    public const BILLING_ADDRESS_ID_COLUMN = 'billing_order_id';
    public const CUSTOMER_EMAIL_COLUMN = 'customer_email';
    public const CHECKOUT_STATE_COLUMN = 'checkout_state';
    public const PAYMENT_STATE_COLUMN = 'payment_state';
    public const SHIPPING_STATE_COLUMN = 'shipping_state';
    public const NOTES_COLUMN = 'notes';
    public const CREATED_AT_COLUMN = 'created_at';
    public const UPDATED_AT_COLUMN = 'updated_at';
}
