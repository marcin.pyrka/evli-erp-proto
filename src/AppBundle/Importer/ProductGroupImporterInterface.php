<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use Elvi\ImportExportBundle\Importer\ImporterInterface;

interface ProductGroupImporterInterface extends ImporterInterface
{
    public const CODE_COLUMN = 'ID';
    public const NAME_COLUMN = 'TITLE';
    public const PRODUCT_FAMILY_CODE_COLUMN = 'PRODUCT_FAMILY_ID';
}
