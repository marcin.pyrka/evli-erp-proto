<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use AppBundle\Entity\BrandInterface;
use AppBundle\Entity\ProductFamilyInterface;
use Elvi\ImportExportBundle\Importer\AbstractImporter;
use Elvi\ImportExportBundle\Resolver\ResourceResolverInterface;
use Sylius\Component\Resource\Model\ResourceInterface;

final class ProductFamilyImporter extends AbstractImporter implements ProductFamilyImporterInterface
{
    /** @var ResourceResolverInterface */
    private $productFamilyResourceResolver;

    /** @var ResourceResolverInterface */
    private $brandResourceResolver;

    public function __construct(
        ResourceResolverInterface $productFamilyResourceResolver,
        ResourceResolverInterface $brandResourceResolver
    ) {
        $this->productFamilyResourceResolver = $productFamilyResourceResolver;
        $this->brandResourceResolver = $brandResourceResolver;
    }

    public function import(array $row): ?ResourceInterface
    {
        $code = $this->getColumnValue(self::CODE_COLUMN, $row);
        $brandCode = $this->getColumnValue(self::BRAND_CODE_COLUMN, $row);
        /** @var ProductFamilyInterface $productFamily */
        $productFamily = $this->productFamilyResourceResolver->resolveResource($code);
        /** @var BrandInterface $brand */
        $brand = $this->brandResourceResolver->resolveResource($brandCode);

        $productFamily->setCode($code);
        $productFamily->setBrand($brand);
        $productFamily->setName($this->getColumnValue(self::NAME_COLUMN, $row));

        return $productFamily;
    }

    public function getResourceCode(): string
    {
        return 'product_family';
    }
}
