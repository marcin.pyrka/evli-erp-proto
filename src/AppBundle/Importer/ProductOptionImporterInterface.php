<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use Elvi\ImportExportBundle\Importer\ImporterInterface;

interface ProductOptionImporterInterface extends ImporterInterface
{
    public const CODE_COLUMN = 'ID';
    public const POSITION_COLUMN = 'SORT';
    public const NAME_COLUMN = 'TITLE__locale__';
}
