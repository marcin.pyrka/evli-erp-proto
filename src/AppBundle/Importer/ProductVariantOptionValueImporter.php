<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use AppBundle\Entity\ProductInterface;
use AppBundle\Entity\ProductVariantInterface;
use Elvi\ImportExportBundle\Importer\AbstractImporter;
use Sylius\Component\Product\Model\ProductOptionValueInterface;
use Sylius\Component\Product\Repository\ProductVariantRepositoryInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

final class ProductVariantOptionValueImporter extends AbstractImporter implements ProductVariantOptionValueImporterInterface
{
    /** @var ProductVariantRepositoryInterface */
    private $productVariantRepository;

    /** @var RepositoryInterface */
    private $productOptionValueRepository;

    public function __construct(
        ProductVariantRepositoryInterface $productVariantRepository,
        RepositoryInterface $productOptionValueRepository
    ) {
        $this->productVariantRepository = $productVariantRepository;
        $this->productOptionValueRepository = $productOptionValueRepository;
    }

    public function import(array $row): ?ResourceInterface
    {
        $variantCode = $this->getColumnValue(self::PRODUCT_VARIANT_CODE_COLUMN, $row);
        $optionValueCode = $this->getColumnValue(self::PRODUCT_OPTION_VALUE_CODE_COLUMN, $row);

        /** @var ProductVariantInterface $productVariant */
        $productVariant = $this->productVariantRepository->findOneBy(['code' => $variantCode]);
        /** @var ProductOptionValueInterface $productOptionValue */
        $productOptionValue = $this->productOptionValueRepository->findOneBy(['code' => $optionValueCode]);

        if (null === $productVariant || null === $productOptionValue) {
            return null;
        }

        /** @var ProductInterface $product */
        $product = $productVariant->getProduct();

        $product->addOption($productOptionValue->getOption());
        $product->addOptionValue($productOptionValue);
        $productVariant->addOptionValue($productOptionValue);

        return $productVariant;
    }

    public function getResourceCode(): string
    {
        return 'product_variant_option_value';
    }
}
