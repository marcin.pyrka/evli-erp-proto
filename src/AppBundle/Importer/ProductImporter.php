<?php

declare(strict_types=1);

namespace AppBundle\Importer;

use AppBundle\Entity\ProductGroupInterface;
use AppBundle\Entity\ProductInterface;
use AppBundle\Generator\ProductNameGeneratorInterface;
use AppBundle\Generator\SlugGeneratorInterface;
use Elvi\ImportExportBundle\Importer\AbstractImporter;
use Elvi\ImportExportBundle\Resolver\ResourceResolverInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

final class ProductImporter extends AbstractImporter implements ProductImporterInterface
{
    /** @var ResourceResolverInterface */
    private $productResourceResolver;

    /** @var RepositoryInterface */
    private $productGroupRepository;

    /** @var ProductNameGeneratorInterface */
    private $productNameGenerator;

    /** @var SlugGeneratorInterface */
    private $slugGenerator;

    /** @var array */
    private $availableLocales;

    public function __construct(
        ResourceResolverInterface $productResourceResolver,
        RepositoryInterface $productGroupRepository,
        ProductNameGeneratorInterface $productNameGenerator,
        SlugGeneratorInterface $slugGenerator,
        array $availableLocales
    ) {
        $this->productResourceResolver = $productResourceResolver;
        $this->productGroupRepository = $productGroupRepository;
        $this->productNameGenerator = $productNameGenerator;
        $this->slugGenerator = $slugGenerator;
        $this->availableLocales = $availableLocales;
    }

    public function import(array $row): ?ResourceInterface
    {
        $code = $this->getColumnValue(self::CODE_COLUMN, $row);
        $productGroupCode = $this->getColumnValue(self::PRODUCT_GROUP_CODE_COLUMN, $row);
        /** @var ProductInterface $product */
        $product = $this->productResourceResolver->resolveResource($code);
        /** @var ProductGroupInterface $productGroup */
        $productGroup = $this->productGroupRepository->findOneBy(['code' => $productGroupCode]);

        $product->setCode($code);
        $product->setGroup($productGroup);
        $product->setEnabled((bool) $this->getColumnValue(self::ENABLED_COLUMN, $row));
        $product->setPrice((int) $this->getColumnValue(self::PRICE_COLUMN, $row) * 100);
        $product->setOriginalPrice((int) $this->getColumnValue(self::ORIGINAL_PRICE_COLUMN, $row) * 100);
        $product->setQuantity((int) $this->getColumnValue(self::QUANTITY_COLUMN, $row));
        $product->setUnit($this->getColumnValue(self::UNIT_COLUMN, $row));
        $product->setTrial((bool) $this->getColumnValue(self::SAMPLE_COLUMN, $row));

        $name = $this->productNameGenerator->generate($product);
        $slug = $this->slugGenerator->generate($name);

        foreach ($this->availableLocales as $locale) {
            $product->setCurrentLocale($locale);
            $product->setFallbackLocale($locale);
            $product->setName($name);
            //@TODO: a lot of duplicates slugs (MySQL unique constraints errors)
            $product->setSlug($slug);
        }

        return $product;
    }

    public function getResourceCode(): string
    {
        return 'product';
    }
}
