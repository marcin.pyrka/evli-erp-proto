<?php

declare(strict_types=1);

namespace AppBundle\Fixture;

use AppBundle\Doctrine\ORM\Repository\FulFillerRepositoryInterface;
use AppBundle\Doctrine\ORM\Repository\OrderRepositoryInterface;
use AppBundle\Entity\Order;
use AppBundle\Entity\ValueObject\OrderAppId;
use AppBundle\Entity\ValueObject\OrderUuid;
use Sylius\Bundle\FixturesBundle\Fixture\AbstractFixture;
use Sylius\Bundle\FixturesBundle\Fixture\FixtureInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

final class OrderFixture extends AbstractFixture implements FixtureInterface
{
    private const DEFAULT_ORDER_APP_ID = '78acd390-0fef-11e9-881a-0a0027000004';
    private const DEFAULT_ORDER_UUID = 'f18b2c20-0389-11e9-8eb2-f2801f1b9fd1';

    private const DATE_FORMAT = 'Y-m-d :H:i:s';

    /** @var OrderRepositoryInterface */
    private $orderRepository;

    /** @var FulFillerRepositoryInterface */
    private $fulFillerRepository;

    public function __construct(OrderRepositoryInterface $repository, FulFillerRepositoryInterface $fulFillerRepository)
    {
        $this->orderRepository = $repository;
        $this->fulFillerRepository = $fulFillerRepository;
    }

    public function getName(): string
    {
        return 'order';
    }

    public function load(array $options): void
    {
        foreach ($options as $order) {
            $fulFiller = $this->fulFillerRepository->findOneById($order['fulfiller_id']);

            $entity = new Order(
                new OrderUuid($order['order_uuid']),
                new OrderAppId($order['app_id']),
                $fulFiller,
                \DateTimeImmutable::createFromFormat(self::DATE_FORMAT, $order['checkout_completed_at'])
            );
            $this->orderRepository->save($entity);
        }
    }

    protected function configureOptionsNode(ArrayNodeDefinition $optionsNode): void
    {
        $optionsNode
            ->children()
            ->arrayNode('order')
            ->performNoDeepMerging()
            ->defaultValue(
                [
                    'app_id' => self::DEFAULT_ORDER_APP_ID,
                    'fulfiller_id' => FulFillerFixture::DEFAULT_FUL_FILLER_ID,
                    'order_uuid' => self::DEFAULT_ORDER_UUID,
                    'checkout_completed_at' => (new \DateTime())->format(self::DATE_FORMAT),
                ]
            )
            ->prototype('scalar');
    }
}
