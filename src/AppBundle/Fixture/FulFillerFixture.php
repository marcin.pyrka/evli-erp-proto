<?php

declare(strict_types=1);

namespace AppBundle\Fixture;

use AppBundle\Doctrine\ORM\Repository\FulFillerRepositoryInterface;
use AppBundle\Entity\Fulfiller\Fulfiller;
use Sylius\Bundle\FixturesBundle\Fixture\AbstractFixture;
use Sylius\Bundle\FixturesBundle\Fixture\FixtureInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

final class FulFillerFixture extends AbstractFixture implements FixtureInterface
{
    const DEFAULT_FUL_FILLER_ID = 'lensvision';

    /**
     * @var FulFillerRepositoryInterface
     */
    private $repository;

    public function __construct(FulFillerRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function getName(): string
    {
        return 'fulfiller';
    }

    public function load(array $options): void
    {
        foreach ($options as $fulFiller) {
            if (null === $this->repository->findOneById($fulFiller['id'])) {
                $entity = new Fulfiller();
                $entity->setId($fulFiller['id']);
                $entity->setTitle($fulFiller['title']);
                $entity->setTotalProductsCount($fulFiller['total_product_count']);
                $entity->setMatchedProductsCount($fulFiller['matched_product_count']);
                $this->repository->save($entity);
            }
        }
    }

    protected function configureOptionsNode(ArrayNodeDefinition $optionsNode): void
    {
        $optionsNode
            ->children()
            ->arrayNode('fulfiller')
            ->performNoDeepMerging()
            ->defaultValue(
                [
                    'id' => self::DEFAULT_FUL_FILLER_ID,
                    'title' => 'LensVision',
                    'total_product_count' => 0,
                    'matched_product_count' => 0,
                ]
            )
            ->prototype('scalar')
        ;
    }
}
