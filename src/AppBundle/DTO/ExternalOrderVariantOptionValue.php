<?php

declare(strict_types=1);

namespace AppBundle\DTO;

final class ExternalOrderVariantOptionValue implements \JsonSerializable
{
    /** @var string */
    private $value;

    /** @var string */
    private $ident;

    public function __construct(string $ident, string $value)
    {
        $this->value = $value;
        $this->ident = $ident;
    }

    public function value(): string
    {
        return $this->value;
    }

    public function ident(): string
    {
        return $this->ident;
    }

    public function jsonSerialize(): array
    {
        return [
           $this->ident => $this->value,
        ];
    }
}
