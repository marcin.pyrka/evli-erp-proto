<?php

declare(strict_types=1);

namespace AppBundle\DTO;

final class OrderShippingDetail implements \JsonSerializable
{
    /** @var string */
    private $tracking;

    public function __construct(string $tracking)
    {
        $this->tracking = $tracking;
    }

    public static function fromAppOrderShippingDetail(array $shippingDetail): self
    {
        return new self($shippingDetail['tracking']);
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}
