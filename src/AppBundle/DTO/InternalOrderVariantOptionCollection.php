<?php

declare(strict_types=1);

namespace AppBundle\DTO;

use ElviERP\Common\Collection\AbstractCollection;

final class InternalOrderVariantOptionCollection extends AbstractCollection
{
    public function __construct(array $options = [])
    {
        foreach ($options as $optionName => $option) {
            $this->add(new InternalOrderVariantOption($optionName, $option['label']));
        }
    }

    public function add(InternalOrderVariantOption $option): void
    {
        $this->doAdd($option);
    }

    public function current(): InternalOrderVariantOption
    {
        /** @var InternalOrderVariantOption $current */
        $current = $this->returnCurrent();

        return $current;
    }
}
