<?php

declare(strict_types=1);

namespace AppBundle\DTO;

final class OrderCoupon implements \JsonSerializable
{
    /** @var string */
    private $title;

    /** @var string */
    private $label;

    /** @var float */
    private $value;

    /** @var string */
    private $currency;

    public function __construct(string $title, string $label, float $value, string $currency)
    {
        $this->title = $title;
        $this->label = $label;
        $this->value = $value;
        $this->currency = $currency;
    }

    public static function fromAppOrderCoupon(array $orderCoupon): self
    {
        return new self($orderCoupon['title'], $orderCoupon['label'], $orderCoupon['value'], $orderCoupon['currency']);
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}
