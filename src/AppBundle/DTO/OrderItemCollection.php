<?php

declare(strict_types=1);

namespace AppBundle\DTO;

use ElviERP\Common\Collection\AbstractCollection;

final class OrderItemCollection extends AbstractCollection implements \JsonSerializable
{
    public function add(OrderItem $orderItem): void
    {
        $this->doAdd($orderItem);
    }

    public function jsonSerialize(): array
    {
        return iterator_to_array($this);
    }

    public function current(): OrderItem
    {
        /** @var OrderItem $current */
        $current = $this->returnCurrent();

        return $current;
    }
}
