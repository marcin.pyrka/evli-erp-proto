<?php

declare(strict_types=1);

namespace AppBundle\DTO;

use AppBundle\Query\Model\FulFillerOrderProductVariantOption;

final class OrderItem implements \JsonSerializable
{
    /** @var FulFillerOrderProductVariantOption */
    private $variant;

    /** @var int */
    private $quantity;

    /** @var OrderItemTotal */
    private $total;

    public function __construct(?FulFillerOrderProductVariantOption $variant, int $quantity, OrderItemTotal $total)
    {
        $this->variant = $variant;
        $this->quantity = $quantity;
        $this->total = $total;
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}
