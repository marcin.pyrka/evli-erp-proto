<?php

declare(strict_types=1);

namespace AppBundle\DTO;

final class OrderItemTotal implements \JsonSerializable
{
    /** @var string */
    private $label;

    /** @var float */
    private $value;

    /** @var string */
    private $currency;

    public function __construct(string $label, string $value, string $currency)
    {
        $this->label = $label;
        $this->value = $value;
        $this->currency = $currency;
    }

    public static function fromVariantTotal(array $variantTotal): self
    {
        return new self($variantTotal['label'], (string) $variantTotal['value'], $variantTotal['currency']);
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}
