<?php

declare(strict_types=1);

namespace AppBundle\DTO;

final class OrderBillingAddress implements \JsonSerializable
{
    /** @var string */
    private $firstName;

    /** @var string */
    private $lastName;

    /** @var string */
    private $companyName;

    /** @var string */
    private $mobile;

    /** @var string */
    private $street;

    /** @var string */
    private $postcode;

    /** @var string */
    private $city;

    /** @var string */
    private $countryCode;

    public function __construct(
        string $firstName,
        string $lastName,
        string $companyName,
        string $mobile,
        string $street,
        string $postcode,
        string $city,
        string $countryCode
    ) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->companyName = $companyName;
        $this->mobile = $mobile;
        $this->street = $street;
        $this->postcode = $postcode;
        $this->city = $city;
        $this->countryCode = $countryCode;
    }

    public static function fromAppOrderBillingAddress(array $billingAddress): self
    {
        return new self(
            $billingAddress['first_name'],
            $billingAddress['last_name'],
            $billingAddress['company_name'],
            $billingAddress['mobile'],
            $billingAddress['street'],
            $billingAddress['postcode'],
            $billingAddress['city'],
            $billingAddress['country_code']
        );
    }

    public function jsonSerialize(): array
    {
        return [
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'company_name' => $this->companyName,
            'mobile' => $this->mobile,
            'postcode' => $this->postcode,
            'city' => $this->city,
            'country_code' => $this->countryCode,
            'street' => $this->street,
        ];
    }
}
