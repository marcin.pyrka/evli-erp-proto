<?php

declare(strict_types=1);

namespace AppBundle\DTO;

final class OrderShippingAddress implements \JsonSerializable
{
    /** @var string */
    private $firstName;

    /** @var string */
    private $lastName;

    /** @var string */
    private $companyName;

    /** @var string */
    private $mobile;

    /** @var string */
    private $street;

    /** @var string */
    private $postcode;

    /** @var string */
    private $city;

    /** @var string */
    private $countryCode;

    /** @var string */
    private $zipCode;

    public function __construct(
        string $firstName,
        string $lastName,
        string $companyName,
        string $mobile,
        string $street,
        string $postcode,
        string $city,
        string $countryCode,
        string $zipCode
    ) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->companyName = $companyName;
        $this->mobile = $mobile;
        $this->street = $street;
        $this->postcode = $postcode;
        $this->city = $city;
        $this->countryCode = $countryCode;
        $this->zipCode = $zipCode;
    }

    public static function fromAppOrderShippingAddress(array $shippingAddress): self
    {
        return new self(
            $shippingAddress['first_name'],
            $shippingAddress['last_name'],
            $shippingAddress['company_name'],
            $shippingAddress['mobile'],
            $shippingAddress['street'],
            $shippingAddress['postcode'],
            $shippingAddress['city'],
            $shippingAddress['country_code'],
            $shippingAddress['zip_code']
        );
    }

    public function jsonSerialize(): array
    {
        return [
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'company_name' => $this->companyName,
            'mobile' => $this->mobile,
            'street' => $this->street,
            'postcode' => $this->postcode,
            'city' => $this->city,
            'country_code' => $this->countryCode,
            'zip_code' => $this->zipCode,
        ];
    }
}
