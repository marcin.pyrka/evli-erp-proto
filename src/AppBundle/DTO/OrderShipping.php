<?php

declare(strict_types=1);

namespace AppBundle\DTO;

final class OrderShipping implements \JsonSerializable
{
    /** @var string */
    private $carrier;

    /** @var string */
    private $option;

    /** @var string */
    private $status;

    /** @var OrderShippingDetail */
    private $detail;

    public function __construct(string $carrier, string $option, string $status, OrderShippingDetail $detail)
    {
        $this->carrier = $carrier;
        $this->option = $option;
        $this->status = $status;
        $this->detail = $detail;
    }

    public static function fromAppOrderShipping(array $orderShipping): self
    {
        $orderDetail = OrderShippingDetail::fromAppOrderShippingDetail($orderShipping['detail']);

        return new self($orderShipping['carrier'], $orderShipping['option'], $orderShipping['status'], $orderDetail);
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}
