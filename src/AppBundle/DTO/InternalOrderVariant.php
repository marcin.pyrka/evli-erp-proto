<?php

declare(strict_types=1);

namespace AppBundle\DTO;

final class InternalOrderVariant
{
    /** @var string */
    private $code;

    /** @var string */
    private $unit;

    /** @var int */
    private $content;

    /** @var InternalOrderVariantOptionCollection */
    private $option;

    public function __construct(
        string $code,
        string $unit,
        int $content,
        InternalOrderVariantOptionCollection $option
    ) {
        $this->code = $code;
        $this->unit = $unit;
        $this->content = $content;
        $this->option = $option;
    }

    public static function fromProductData(array $product, array $options): self
    {
        $optionCollection = new InternalOrderVariantOptionCollection($options);

        return new self(
            (string) $product['id'],
            $product['unit'],
            $product['content'],
            $optionCollection
        );
    }

    public function code(): string
    {
        return $this->code;
    }

    public function unit(): string
    {
        return $this->unit;
    }

    public function content(): int
    {
        return $this->content;
    }

    public function option(): InternalOrderVariantOptionCollection
    {
        return $this->option;
    }
}
