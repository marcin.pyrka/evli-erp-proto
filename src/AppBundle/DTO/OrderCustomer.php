<?php

declare(strict_types=1);

namespace AppBundle\DTO;

final class OrderCustomer implements \JsonSerializable
{
    /** @var string */
    private $id;

    /** @var int */
    private $gender;

    /** @var string */
    private $firstName;

    /** @var string */
    private $lastName;

    /** @var string */
    private $companyName;

    /** @var string */
    private $email;

    /** @var string */
    private $mobile;

    /** @var string */
    private $birthDate;

    /** @var string */
    private $language;

    /** @var string */
    private $externalId;

    public function __construct(
        string $id,
        int $gender,
        string $firstName,
        string $lastName,
        string $companyName,
        string $email,
        string $mobile,
        string $birthDate,
        string $language,
        ?string $externalId
    ) {
        $this->id = $id;
        $this->gender = $gender;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->companyName = $companyName;
        $this->email = $email;
        $this->mobile = $mobile;
        $this->birthDate = $birthDate;
        $this->language = $language;
        $this->externalId = $externalId;
    }

    public static function fromAppOrderCustomer(array $orderCustomer, ?string $externalId): self
    {
        return new self(
            $orderCustomer['id'],
            $orderCustomer['gender'],
            $orderCustomer['first_name'],
            $orderCustomer['last_name'],
            $orderCustomer['company_name'],
            $orderCustomer['email'],
            $orderCustomer['mobile'],
            $orderCustomer['birth_date'],
            $orderCustomer['language'],
            $externalId
        );
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'gender' => $this->gender,
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'company_name' => $this->companyName,
            'email' => $this->email,
            'mobile' => $this->mobile,
            'birth_date' => $this->birthDate,
            'language' => $this->language,
            'external_id' => $this->externalId,
        ];
    }
}
