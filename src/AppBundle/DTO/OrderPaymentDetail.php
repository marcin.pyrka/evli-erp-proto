<?php

declare(strict_types=1);

namespace AppBundle\DTO;

final class OrderPaymentDetail implements \JsonSerializable
{
    /** @var string */
    private $sender;

    /** @var string */
    private $collector;

    /** @var string */
    private $paymentId;

    public function __construct(string $sender, string $collector, string $paymentId)
    {
        $this->sender = $sender;
        $this->collector = $collector;
        $this->paymentId = $paymentId;
    }

    public static function fromAppOrderPaymentDetails(array $paymentDetails): self
    {
        return new self($paymentDetails['sender'], $paymentDetails['collector'], $paymentDetails['payment_id']);
    }

    public function jsonSerialize(): array
    {
        return [
            'sender' => $this->sender,
            'collector' => $this->collector,
            'payment_id' => $this->paymentId,
        ];
    }
}
