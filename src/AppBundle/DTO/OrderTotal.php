<?php

declare(strict_types=1);

namespace AppBundle\DTO;

final class OrderTotal implements \JsonSerializable
{
    /** @var string */
    private $label;

    /** @var float */
    private $value;

    /** @var string */
    private $currency;

    public function __construct(string $label, float $value, string $currency)
    {
        $this->label = $label;
        $this->value = $value;
        $this->currency = $currency;
    }

    public static function fromAppOrderTotal(array $orderTotal): self
    {
        return new self($orderTotal['label'], (float) $orderTotal['value'], $orderTotal['currency']);
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}
