<?php

declare(strict_types=1);

namespace AppBundle\DTO;

use AppBundle\Query\Model\Order as OrderModel;

final class Order implements \JsonSerializable
{
    /** @var string */
    private $orderUuid;

    /** @var string */
    private $fulFillerOrderId;

    /** @var string */
    private $number;

    /** @var OrderItemCollection */
    private $item;

    /** @var string */
    private $orderDate;

    /** @var string */
    private $status;

    /** @var string */
    private $createdAt;

    /** @var string */
    private $updatedAt;

    /** @var OrderDeliveryCost */
    private $deliveryCost;

    /** @var OrderCoupon */
    private $coupon;

    /** @var OrderTotal */
    private $total;

    /** @var OrderPayment */
    private $payment;

    /** @var OrderShipping */
    private $shipping;

    /** @var OrderCustomer */
    private $customer;

    /** @var OrderBillingAddress */
    private $billingAddress;

    /** @var OrderShippingAddress */
    private $shippingAddress;

    public function __construct(
        string $orderUuid,
        ?string $fulFillerOrderId,
        string $number,
        OrderItemCollection $item,
        string $orderDate,
        string $status,
        string $createdAt,
        string $updatedAt,
        OrderDeliveryCost $deliveryCost,
        OrderCoupon $coupon,
        OrderTotal $total,
        OrderPayment $payment,
        OrderShipping $shipping,
        OrderCustomer $customer,
        OrderBillingAddress $billingAddress,
        OrderShippingAddress $shippingAddress
    ) {
        $this->orderUuid = $orderUuid;
        $this->fulFillerOrderId = $fulFillerOrderId;
        $this->number = $number;
        $this->item = $item;
        $this->orderDate = $orderDate;
        $this->status = $status;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->deliveryCost = $deliveryCost;
        $this->coupon = $coupon;
        $this->total = $total;
        $this->payment = $payment;
        $this->shipping = $shipping;
        $this->customer = $customer;
        $this->billingAddress = $billingAddress;
        $this->shippingAddress = $shippingAddress;
    }

    public static function fromAppResponseAndOrderViewItemCollection(
        OrderModel $orderView,
        array $appOrder,
        OrderItemCollection $itemCollection
    ): self {
        return new self(
            $orderView->id(),
            $orderView->fulFillerOrderId(),
            $appOrder['order_nr'],
            $itemCollection,
            $appOrder['order_date'],
            $appOrder['status'],
            $appOrder['created_at'],
            $appOrder['updated_at'],
            OrderDeliveryCost::fromAppOrderVariantDeliveryCost($appOrder['delivery_cost']),
            OrderCoupon::fromAppOrderCoupon($appOrder['coupon']),
            OrderTotal::fromAppOrderTotal($appOrder['order_total']),
            OrderPayment::fromAppOrderPayment($appOrder['payment']),
            OrderShipping::fromAppOrderShipping($appOrder['shipping']),
            OrderCustomer::fromAppOrderCustomer($appOrder['customer'], null),
            OrderBillingAddress::fromAppOrderBillingAddress($appOrder['billing_address']),
            OrderShippingAddress::fromAppOrderShippingAddress($appOrder['shipping_address'])
        );
    }

    public function jsonSerialize(): array
    {
        return [
            'order_id' => $this->orderUuid,
            'external_id' => $this->fulFillerOrderId,
            'order_date' => $this->orderDate,
            'order_nr' => $this->number,
            'item' => $this->item,
            'delivery_cost' => $this->deliveryCost,
            'coupon' => $this->coupon,
            'order_total' => $this->total,
            'status' => $this->status,
            'payment' => $this->payment,
            'shipping' => $this->shipping,
            'customer' => $this->customer,
            'billing_address' => $this->billingAddress,
            'shipping_address' => $this->shippingAddress,
            'created_at' => $this->createdAt,
            'updated_at' => $this->updatedAt,
        ];
    }
}
