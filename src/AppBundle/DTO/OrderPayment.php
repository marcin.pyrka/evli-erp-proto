<?php

declare(strict_types=1);

namespace AppBundle\DTO;

final class OrderPayment implements \JsonSerializable
{
    /** @var string */
    private $option;

    /** @var string */
    private $status;

    /** @var OrderPaymentDetail */
    private $detail;

    public function __construct(string $option, string $status, OrderPaymentDetail $detail)
    {
        $this->option = $option;
        $this->status = $status;
        $this->detail = $detail;
    }

    public static function fromAppOrderPayment(array $orderPayment): self
    {
        return new self(
            $orderPayment['option'],
            $orderPayment['status'],
            OrderPaymentDetail::fromAppOrderPaymentDetails($orderPayment['detail'])
        );
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}
