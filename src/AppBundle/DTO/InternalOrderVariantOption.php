<?php

declare(strict_types=1);

namespace AppBundle\DTO;

final class InternalOrderVariantOption
{
    /** @var string */
    private $optionName;

    /** @var string */
    private $optionValue;

    public function __construct(string $optionName, string $optionValue)
    {
        $this->optionName = $optionName;
        $this->optionValue = $optionValue;
    }

    public function optionName(): string
    {
        return $this->optionName;
    }

    public function optionValue(): string
    {
        return $this->optionValue;
    }
}
