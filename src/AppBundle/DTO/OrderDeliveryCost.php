<?php

declare(strict_types=1);

namespace AppBundle\DTO;

final class OrderDeliveryCost implements \JsonSerializable
{
    /** @var string */
    private $label;

    /** @var float */
    private $value;

    /** @var string */
    private $currency;

    public function __construct(string $label, float $value, string $currency)
    {
        $this->label = $label;
        $this->value = $value;
        $this->currency = $currency;
    }

    public static function fromAppOrderVariantDeliveryCost(array $deliveryCost): self
    {
        return new self($deliveryCost['label'], $deliveryCost['value'], $deliveryCost['currency']);
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}
