<?php

declare(strict_types=1);

namespace AppBundle\Transformer;

use AppBundle\DTO\InternalOrderVariant;
use AppBundle\Query\FindFulFillerProductOptionValueQueryInterface;
use AppBundle\Query\Model\FulFillerOrderProductVariantOption;

final class OrderProductOptionValueTransformer implements OrderProductOptionValueTransformerInterface
{
    /** @var FindFulFillerProductOptionValueQueryInterface */
    private $productQuery;

    public function __construct(FindFulFillerProductOptionValueQueryInterface $productQuery)
    {
        $this->productQuery = $productQuery;
    }

    public function __invoke(InternalOrderVariant $internalOrderVariant): ?FulFillerOrderProductVariantOption
    {
        if ($internalOrderVariant->option()->count()) {
            $product = $options = [];
            foreach ($internalOrderVariant->option() as $option) {
                $productExternal = $this->productQuery->__invoke(
                    $internalOrderVariant->code(),
                    $option->optionName(),
                    $option->optionValue()
                );
                $product = [
                    'unit' => $internalOrderVariant->unit(),
                    'content' => $internalOrderVariant->content(),
                    'external_product_id' => $productExternal['external_product_id'],
                    'title' => $productExternal['product_title'],
                ];

                $options[] = [
                    $productExternal['fulfiller_option'] => $productExternal['fulfiller_option_value'],
                ];
            }

            return FulFillerOrderProductVariantOption::fromArray($product, $options);
        }

        return null;
    }
}
