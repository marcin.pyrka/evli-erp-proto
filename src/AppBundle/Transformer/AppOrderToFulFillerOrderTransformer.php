<?php

declare(strict_types=1);

namespace AppBundle\Transformer;

use AppBundle\DTO\InternalOrderVariant;
use AppBundle\DTO\Order;
use AppBundle\DTO\OrderItem;
use AppBundle\DTO\OrderItemCollection;
use AppBundle\DTO\OrderItemTotal;
use AppBundle\Query\FindOrderByOrderIdFulFillerIdQueryInterface;
use AppBundle\Service\GetOrderByOrderAppIdHttpRequestInterface;

final class AppOrderToFulFillerOrderTransformer implements AppOrderToFulFillerOrderTransformerInterface
{
    /** @var FindOrderByOrderIdFulFillerIdQueryInterface */
    private $erpOrderQuery;

    /** @var GetOrderByOrderAppIdHttpRequestInterface */
    private $appOrder;

    /** @var OrderProductOptionValueTransformerInterface */
    private $variantTransformer;

    public function __construct(
        FindOrderByOrderIdFulFillerIdQueryInterface $internalOrderQuery,
        GetOrderByOrderAppIdHttpRequestInterface $orderAppClient,
        OrderProductOptionValueTransformerInterface $variantTransformer
    ) {
        $this->erpOrderQuery = $internalOrderQuery;
        $this->appOrder = $orderAppClient;
        $this->variantTransformer = $variantTransformer;
    }

    public function __invoke(string $appOrderId, string $fulFillerId): Order
    {
        $erpOrder = $this->erpOrderQuery->__invoke($appOrderId, $fulFillerId);
        $appOrder = $this->appOrder->__invoke($erpOrder->appId());

        $itemCollection = new OrderItemCollection();
        foreach ($appOrder['item'] as $item) {
            $externalVariant = $this->variantTransformer->__invoke(
                InternalOrderVariant::fromProductData(
                    $item['variant']['product'],
                    $item['variant']['option']
                )
            );
            $orderItemTotal = OrderItemTotal::fromVariantTotal($item['total']);
            $itemCollection->add(new OrderItem($externalVariant, $item['quantity'], $orderItemTotal));
        }

        return Order::fromAppResponseAndOrderViewItemCollection($erpOrder, $appOrder, $itemCollection);
    }
}
