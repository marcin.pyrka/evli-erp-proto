<?php

declare(strict_types=1);

namespace AppBundle\Transformer;

use AppBundle\DTO\Order;

interface AppOrderToFulFillerOrderTransformerInterface
{
    public function __invoke(string $appOrderId, string $fulFillerId): Order;
}
