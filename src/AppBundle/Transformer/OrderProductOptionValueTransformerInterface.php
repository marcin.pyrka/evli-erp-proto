<?php

declare(strict_types=1);

namespace AppBundle\Transformer;

use AppBundle\DTO\InternalOrderVariant;
use AppBundle\Query\Model\FulFillerOrderProductVariantOption;

interface OrderProductOptionValueTransformerInterface
{
    public function __invoke(InternalOrderVariant $internalOrderVariant): ?FulFillerOrderProductVariantOption;
}
