<?php

declare(strict_types=1);

namespace AppBundle\Exception;

final class FulFillerIdException extends \DomainException
{
    public static function onEmpty(): self
    {
        return new self('Full filler Id can not be empty!');
    }

    public static function onInvalidUuid(): self
    {
        return new self('Full filler Id is not an UUID string!');
    }
}
