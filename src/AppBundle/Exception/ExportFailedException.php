<?php

declare(strict_types=1);

namespace AppBundle\Exception;

final class ExportFailedException extends \RuntimeException
{
    public function __construct(string $message)
    {
        parent::__construct(sprintf('Export failed. Exception message: %s', $message));
    }
}
