<?php

declare(strict_types=1);

namespace AppBundle\Exception;

final class OrderAppIdException extends \DomainException
{
    public static function onEmpty(): self
    {
        return new self('Order App ID can not be empty!');
    }
}
