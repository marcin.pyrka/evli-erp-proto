<?php

declare(strict_types=1);

namespace AppBundle\Exception;

final class OrderProductOptionValueNotFoundException extends \InvalidArgumentException
{
    public static function create(string $code, string $optionName, string $optionValue): self
    {
        return new self(
            sprintf(
                'Order can not match option and option value. Product with id: "%s", option name "%s" and option value "%s" not found!',
                $code,
                $optionName,
                $optionValue
            )
        );
    }
}
