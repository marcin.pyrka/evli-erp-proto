<?php

declare(strict_types=1);

namespace AppBundle\Exception;

final class OrderNotFoundException extends \RuntimeException
{
    public static function withUuidFulFillerId(string $uuid, string $fulFillerId): self
    {
        return new self(sprintf('Order by id: "%s" not found for given ful filler id: "%s"!', $uuid, $fulFillerId));
    }
}
