<?php

declare(strict_types=1);

namespace AppBundle\Exception;

final class FulFillerOrderIdException extends \DomainException
{
    public static function onEmpty(): self
    {
        return new self('Order external id can not be empty!');
    }
}
