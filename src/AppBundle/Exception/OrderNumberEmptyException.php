<?php

declare(strict_types=1);

namespace AppBundle\Exception;

final class OrderNumberEmptyException extends \DomainException
{
    public static function create(): self
    {
        return new self('Order number can not be empty!');
    }
}
