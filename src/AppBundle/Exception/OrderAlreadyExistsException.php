<?php

declare(strict_types=1);

namespace AppBundle\Exception;

final class OrderAlreadyExistsException extends \DomainException
{
    public static function withOrderUuid(string $uuid): self
    {
        return new self(sprintf('Order with uuid: "%s" exists!', $uuid));
    }
}
