<?php

declare(strict_types=1);

namespace AppBundle\Exception;

final class OrderTokenException extends \DomainException
{
    public static function onEmpty(): self
    {
        return new self('Token can not be empty');
    }

    public static function onInvalidUuid(): self
    {
        return new self('Order Token it is not an UUID string!');
    }
}
