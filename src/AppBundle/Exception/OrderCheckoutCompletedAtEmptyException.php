<?php

declare(strict_types=1);

namespace AppBundle\Exception;

final class OrderCheckoutCompletedAtEmptyException extends \DomainException
{
    public static function create(): self
    {
        return new self('Order checkout completed at can not be empty!');
    }
}
