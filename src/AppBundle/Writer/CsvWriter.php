<?php

declare(strict_types=1);

namespace AppBundle\Writer;

use League\Csv\Writer;

final class CsvWriter implements WriterInterface
{
    public function write(array $data, string $filePath): void
    {
        $writer = Writer::createFromPath($filePath, 'w+');

        $writer->insertAll($data);
    }

    public function getExtension(): string
    {
        return 'csv';
    }
}
