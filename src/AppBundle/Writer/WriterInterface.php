<?php

declare(strict_types=1);

namespace AppBundle\Writer;

interface WriterInterface
{
    public function write(array $data, string $filePath): void;

    public function getExtension(): string;
}
