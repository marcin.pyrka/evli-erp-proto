<?php declare(strict_types=1);

namespace AppBundle\Application\SharedKernel\UseCase\DefineClient;

class DefineClient
{
    /** @var string */
    private $clientName;

    /**
     * @param string $clientName
     *
     * @return self
     */
    public static function create(string $clientName): self
    {
        $self = new self();
        $self->clientName = $clientName;

        return $self;
    }

    /**
     * @return string
     */
    public function getClientName(): string
    {
        return $this->clientName;
    }

    private function __construct() { }
}
