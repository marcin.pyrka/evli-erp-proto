<?php declare(strict_types=1);

namespace AppBundle\Application\SharedKernel\UseCase\DefineClient;

use AppBundle\Application\SharedKernel\Service\Client\ClientService;

class DefineClientHandler
{
    /** @var ClientService */
    private $clientService;

    /**
     * @param ClientService $clientService
     */
    public function __construct(ClientService $clientService)
    {
        $this->clientService = $clientService;
    }

    /**
     * @param DefineClient $command
     */
    public function __invoke(DefineClient $command): void
    {
        $this->clientService->setClientName($command->getClientName());
    }
}
