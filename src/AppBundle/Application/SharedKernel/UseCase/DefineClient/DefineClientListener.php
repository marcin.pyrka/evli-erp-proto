<?php declare(strict_types=1);

namespace AppBundle\Application\SharedKernel\UseCase\DefineClient;

use AppBundle\Domain\SharedKernel\Client;
use Persist\BusBundle\CommandBus\CommandBusAwareTrait;
use Persist\BusBundle\CommandBus\CommandBusInterface;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Console\Event\ConsoleCommandEvent;

class DefineClientListener
{
    use CommandBusAwareTrait;

    /**
     * @param CommandBusInterface $commandBus
     */
    public function __construct(CommandBusInterface $commandBus)
    {
        $this->setCommandBus($commandBus);
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event): void
    {
        $this->handle($this->parseHttp($event->getRequest())->getValue());
    }

    /**
     * @param Request $request
     *
     * @return Client
     */
    private function parseHttp(Request $request): Client
    {
        foreach (Client::getEnumerators() as $client) {
            if (stripos($request->getHttpHost(), sprintf('%s.elvi', $client->getValue())) !== false) {
                return $client;
            }
        }

        return Client::ELVI();
    }

    /**
     * @param ConsoleCommandEvent $event
     */
    public function onConsoleCommand(ConsoleCommandEvent $event): void
    {
        $command = $event->getCommand();
        $command->getDefinition()->addOption(new InputOption('client', null, InputOption::VALUE_OPTIONAL));
        $command->mergeApplicationDefinition();

        $input = new ArgvInput();
        $input->bind($command->getDefinition());

        $client = $this->parseCli($input->getOption('client'));
        $this->handle($client->getValue());

        $event->getOutput()->writeln(sprintf('<question>Setting client to "%s"</question>', $client->getValue()));
    }

    /**
     * @param mixed $client
     *
     * @return Client
     */
    private function parseCli($client): Client
    {
        return ! is_null($client) ? Client::byValue($client) : Client::ELVI();
    }

    /**
     * @param string $clientName
     */
    private function handle(string $clientName): void
    {
        $this->handleCommand(DefineClient::create($clientName));
    }
}
