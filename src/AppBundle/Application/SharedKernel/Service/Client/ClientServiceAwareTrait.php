<?php declare(strict_types=1);

namespace AppBundle\Application\SharedKernel\Service\Client;

trait ClientServiceAwareTrait
{
    /** @var ClientServiceInterface */
    protected $clientService;

    /**
     * @param ClientServiceInterface $clientService
     */
    public function setClientService(ClientServiceInterface $clientService): void
    {
        $this->clientService = $clientService;
    }
}
