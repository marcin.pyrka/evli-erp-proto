<?php declare(strict_types=1);

namespace AppBundle\Application\SharedKernel\Service\Client;

use AppBundle\Domain\SharedKernel\Client;

interface ClientServiceInterface
{
    /**
     * @return Client
     */
    public function get(): Client;
}
