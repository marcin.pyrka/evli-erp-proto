<?php declare(strict_types=1);

namespace AppBundle\Application\SharedKernel\Service\Client;

use AppBundle\Domain\SharedKernel\Client;

class ClientService implements ClientServiceInterface
{
    /** @var string */
    private $clientName;

    /**
     * @param string $clientName
     */
    public function setClientName(string $clientName): void
    {
        $this->clientName = $clientName;
    }

    /**
     * @inheritDoc
     */
    public function get(): Client
    {
        return Client::get($this->clientName);
    }
}
