<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Model;

class HttpResponse
{
    /** @var int */
    private $statusCode;

    /** @var array */
    private $data;

    /**
     * @param int $statusCode
     * @param array $data
     *
     * @return self
     */
    public static function create(int $statusCode, array $data): self
    {
        $self = new self();
        $self->statusCode = $statusCode;
        $self->data = $data;

        return $self;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    private function __construct() { }
}
