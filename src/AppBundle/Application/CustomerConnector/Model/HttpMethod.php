<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Model;

use MabeEnum\Enum;

/**
 * @method static HttpMethod HTTP_GET()
 * @method static HttpMethod HTTP_POST()
 * @method static HttpMethod HTTP_PATCH()
 * @method static HttpMethod HTTP_DELETE()
 */
class HttpMethod extends Enum
{
    public const HTTP_GET = 'get';
    public const HTTP_POST = 'post';
    public const HTTP_PATCH = 'patch';
    public const HTTP_DELETE = 'delete';
}
