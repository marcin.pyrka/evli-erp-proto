<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Model;

class HttpResource
{
    /** @var HttpEndpoint */
    private $endpoint;

    /** @var array */
    private $urlParams;

    /** @var array */
    private $data;

    /**
     * @param HttpEndpoint $endpoint
     * @param array $urlParams
     * @param array $data
     *
     * @return self
     */
    public static function create(HttpEndpoint $endpoint, array $urlParams, array $data): self
    {
        $self = new self();
        $self->endpoint = $endpoint;
        $self->urlParams = $urlParams;
        $self->data = $data;

        return $self;
    }

    /**
     * @return HttpEndpoint
     */
    public function getEndpoint(): HttpEndpoint
    {
        return $this->endpoint;
    }

    /**
     * @return array
     */
    public function getUrlParams(): array
    {
        return $this->urlParams;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    private function __construct() { }
}
