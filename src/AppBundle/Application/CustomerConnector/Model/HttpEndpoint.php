<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Model;

use MabeEnum\Enum;

/**
 * @method static HttpEndpoint GET_CUSTOMER()
 * @method static HttpEndpoint GET_CUSTOMER_LIST()
 * @method static HttpEndpoint CREATE_CUSTOMER()
 * @method static HttpEndpoint UPDATE_CUSTOMER()
 * @method static HttpEndpoint DELETE_CUSTOMER()
 *
 * @method static HttpEndpoint GET_NOTE()
 * @method static HttpEndpoint GET_NOTE_LIST()
 * @method static HttpEndpoint GET_NOTE_LIST_BY_CUSTOMER()
 * @method static HttpEndpoint CREATE_NOTE()
 * @method static HttpEndpoint UPDATE_NOTE()
 * @method static HttpEndpoint DELETE_NOTE()
 *
 * @method static HttpEndpoint GET_MEASUREMENT()
 * @method static HttpEndpoint GET_MEASUREMENT_LIST()
 * @method static HttpEndpoint GET_MEASUREMENT_LIST_BY_CUSTOMER()
 * @method static HttpEndpoint CREATE_MEASUREMENT()
 * @method static HttpEndpoint UPDATE_MEASUREMENT()
 * @method static HttpEndpoint DELETE_MEASUREMENT()
 */
class HttpEndpoint extends Enum
{
    public const GET_CUSTOMER = 'get_customer';
    public const GET_CUSTOMER_LIST = 'get_customer_list';
    public const CREATE_CUSTOMER = 'create_customer';
    public const UPDATE_CUSTOMER = 'update_customer';
    public const DELETE_CUSTOMER = 'delete_customer';

    public const GET_NOTE = 'get_note';
    public const GET_NOTE_LIST = 'get_note_list';
    public const GET_NOTE_LIST_BY_CUSTOMER = 'get_note_list_by_customer';
    public const CREATE_NOTE = 'create_note';
    public const UPDATE_NOTE = 'update_note';
    public const DELETE_NOTE = 'delete_note';

    public const GET_MEASUREMENT = 'get_measurement';
    public const GET_MEASUREMENT_LIST = 'get_measurement_list';
    public const GET_MEASUREMENT_LIST_BY_CUSTOMER = 'get_measurement_list_by_customer';
    public const CREATE_MEASUREMENT = 'create_measurement';
    public const UPDATE_MEASUREMENT = 'update_measurement';
    public const DELETE_MEASUREMENT = 'delete_measurement';
}
