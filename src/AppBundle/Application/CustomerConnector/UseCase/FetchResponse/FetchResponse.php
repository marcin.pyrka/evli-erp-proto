<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\UseCase\FetchResponse;

class FetchResponse
{
    /** @var string */
    private $requestId;

    /**
     * @param string $requestId
     *
     * @return self
     */
    public static function create(string $requestId): self
    {
        $self = new self();
        $self->requestId = $requestId;

        return $self;
    }

    /**
     * @return string
     */
    public function getRequestId(): string
    {
        return $this->requestId;
    }

    private function __construct() { }
}
