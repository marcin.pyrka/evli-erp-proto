<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\UseCase\FetchResponse;

use AppBundle\Application\CustomerConnector\Model\HttpResponse;
use AppBundle\Application\CustomerConnector\Service\Request\RequestService;
use AppBundle\Domain\CustomerConnector\Request;
use AppBundle\Domain\CustomerConnector\Status;
use AppBundle\Infrastructure\Persistence\CustomerConnector\RequestRepositoryInterface;

class FetchResponseHandler
{
    /** @var RequestRepositoryInterface */
    private $requestRepository;

    /** @var RequestService */
    private $requestService;

    /**
     * @param RequestRepositoryInterface $requestRepository
     * @param RequestService $requestService
     */
    public function __construct(RequestRepositoryInterface $requestRepository, RequestService $requestService)
    {
        $this->requestRepository = $requestRepository;
        $this->requestService = $requestService;
    }

    /**
     * @param FetchResponse $query
     *
     * @return HttpResponse
     */
    public function __invoke(FetchResponse $query): HttpResponse
    {
        $request = $this->requestRepository->get($query->getRequestId());

        try {
            $this->requestService->handle($request);
            $this->updateRequest($request);
        } catch (\Exception $exception) {
            $this->updateRequestOnException($request, $exception);

            throw $exception;
        }

        return HttpResponse::create($this->requestService->getStatusCode(), $this->requestService->getResponse());
    }

    /**
     * @param Request $request
     */
    private function updateRequest(Request $request): void
    {
        $request->setStatus($this->requestService->hasError() ? Status::FAILED() : Status::DONE());
        $request->setModifiedData($this->requestService->getData());
        $request->setResponseData($this->requestService->getResponse());

        $this->requestRepository->save($request);
    }

    /**
     * @param Request $request
     * @param \Exception $exception
     */
    private function updateRequestOnException(Request $request, \Exception $exception): void
    {
        $request->setStatus(Status::FAILED());
        $request->setModifiedData($this->requestService->getData());
        $request->setResponseData(['erp exception' => get_class($exception), 'message' => $exception->getMessage()]);

        $this->requestRepository->save($request);
    }
}
