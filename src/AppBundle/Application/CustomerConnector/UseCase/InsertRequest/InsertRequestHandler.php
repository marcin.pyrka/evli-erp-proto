<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\UseCase\InsertRequest;

use AppBundle\Application\SharedKernel\Service\Client\ClientServiceAwareTrait;
use AppBundle\Application\SharedKernel\Service\Client\ClientServiceInterface;
use AppBundle\Domain\CustomerConnector\Request;
use AppBundle\Infrastructure\Helper\ImmutableDateTrait;
use AppBundle\Infrastructure\Persistence\CustomerConnector\RequestRepositoryInterface;

class InsertRequestHandler
{
    use ImmutableDateTrait;
    use ClientServiceAwareTrait;

    /** @var RequestRepositoryInterface */
    private $repository;

    /**
     * @param RequestRepositoryInterface $repository
     * @param ClientServiceInterface $clientService
     */
    public function __construct(RequestRepositoryInterface $repository, ClientServiceInterface $clientService)
    {
        $this->repository = $repository;
        $this->setClientService($clientService);
    }

    /**
     * @param InsertRequest $command
     *
     * @throws \Exception
     */
    public function __invoke(InsertRequest $command): void
    {
        $request = Request::create(
            $command->getUuid(),
            $this->clientService->get(),
            $command->getResource(),
            $this->getImmutableDateTime()
        );

        $this->repository->store($request);
    }
}
