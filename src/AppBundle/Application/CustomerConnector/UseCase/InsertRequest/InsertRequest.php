<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\UseCase\InsertRequest;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\Application\CustomerConnector\Model\HttpResource;
use Ramsey\Uuid\UuidInterface;

class InsertRequest
{
    /** @var string */
    private $uuid;

    /** @var HttpEndpoint */
    private $endpoint;

    /** @var array */
    private $urlParams;

    /** @var array */
    private $data;

    /**
     * @param UuidInterface $uuid
     * @param HttpEndpoint $endpoint
     * @param array $urlParams
     * @param array $data
     *
     * @return self
     */
    public static function create(UuidInterface $uuid, HttpEndpoint $endpoint, array $urlParams, array $data): self
    {
        $self = new self();
        $self->uuid = $uuid->toString();
        $self->endpoint = $endpoint;
        $self->urlParams = $urlParams;
        $self->data = $data;

        return $self;
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @return HttpResource
     */
    public function getResource(): HttpResource
    {
        return HttpResource::create($this->endpoint, $this->urlParams, $this->data);
    }

    private function __construct() { }
}
