<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Service\IdParser;

class MeasurementIdParser extends AbstractIdParser
{
    /**
     * @inheritDoc
     */
    protected function field(): string
    {
        return 'external_measurement_id';
    }
}
