<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Service\IdParser;

class IdParser extends AbstractIdParser
{
    /**
     * @inheritDoc
     */
    protected function field(): string
    {
        return 'external_id';
    }
}
