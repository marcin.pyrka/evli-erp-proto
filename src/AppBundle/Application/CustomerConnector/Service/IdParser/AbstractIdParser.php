<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Service\IdParser;

use Assert\Assertion;

abstract class AbstractIdParser implements IdParserInterface
{
    /**
     * @return string
     */
    abstract protected function field(): string;

    /**
     * @inheritDoc
     */
    public function parse(array $data): string
    {
        $externalId = (string)($data[$this->field()] ?? null);
        Assertion::notEmpty($externalId, sprintf('Field "%s" should be set', $this->field()));

        return $externalId;
    }
}
