<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Service\IdParser;

class NoteIdParser extends AbstractIdParser
{
    /**
     * @inheritDoc
     */
    protected function field(): string
    {
        return 'external_note_id';
    }
}
