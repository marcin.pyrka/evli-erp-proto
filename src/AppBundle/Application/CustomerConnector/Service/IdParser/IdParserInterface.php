<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Service\IdParser;

use Assert\InvalidArgumentException;

interface IdParserInterface
{
    /**
     * @param array $data
     *
     * @return string
     * @throws InvalidArgumentException
     */
    public function parse(array $data): string;
}
