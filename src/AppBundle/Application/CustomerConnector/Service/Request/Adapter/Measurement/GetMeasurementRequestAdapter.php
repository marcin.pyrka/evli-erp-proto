<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Service\Request\Adapter\Measurement;

use AppBundle\Application\CustomerConnector\Model\HttpMethod;
use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\Application\CustomerConnector\Service\Request\AbstractRequestAdapter;
use AppBundle\Infrastructure\Persistence\CustomerConnector\MappingRepositoryInterface;
use Twig\Environment;

class GetMeasurementRequestAdapter extends AbstractRequestAdapter
{
    use MeasurementHelperTrait;

    /** @var MappingRepositoryInterface */
    private $mappingRepository;

    /** @var Environment */
    private $twig;

    /**
     * @param MappingRepositoryInterface $mappingRepository
     * @param Environment $twig
     */
    public function __construct(MappingRepositoryInterface $mappingRepository, Environment $twig)
    {
        $this->mappingRepository = $mappingRepository;
        $this->twig = $twig;
    }

    /**
     * @inheritDoc
     */
    public function method(): HttpMethod
    {
        return HttpMethod::HTTP_GET();
    }

    /**
     * @inheritDoc
     */
    protected function endpoint(): HttpEndpoint
    {
        return HttpEndpoint::GET_MEASUREMENT();
    }

    /**
     * @inheritDoc
     */
    public function modifyResponse(array $response): array
    {
        return [
            'meta' => $response['meta'] ?? [],
            'result' => $this->modifyMeasurement($response['result'] ?? []),
        ];
    }

    /**
     * @inheritDoc
     */
    public function uri(): string
    {
        return sprintf('measurement/%s', $this->getMeasurementId());
    }

    /**
     * @inheritDoc
     */
    protected function getMappingRepository(): MappingRepositoryInterface
    {
        return $this->mappingRepository;
    }

    /**
     * @inheritDoc
     */
    protected function getTwig(): Environment
    {
        return $this->twig;
    }
}
