<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Service\Request\Adapter\Measurement;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\Application\CustomerConnector\Model\HttpMethod;
use AppBundle\Application\CustomerConnector\Service\Request\AbstractRequestAdapter;
use AppBundle\Infrastructure\Persistence\CustomerConnector\MappingRepositoryInterface;
use Twig\Environment;

class UpdateMeasurementRequestAdapter extends AbstractRequestAdapter
{
    use MeasurementHelperTrait;

    /** @var MappingRepositoryInterface */
    private $mappingRepository;

    /**
     * @param MappingRepositoryInterface $mappingRepository
     */
    public function __construct(MappingRepositoryInterface $mappingRepository)
    {
        $this->mappingRepository = $mappingRepository;
    }

    /**
     * @inheritDoc
     */
    public function method(): HttpMethod
    {
        return HttpMethod::HTTP_PATCH();
    }

    /**
     * @inheritDoc
     */
    protected function endpoint(): HttpEndpoint
    {
        return HttpEndpoint::UPDATE_MEASUREMENT();
    }

    /**
     * @inheritDoc
     */
    public function uri(): string
    {
        return sprintf('measurement/%s', $this->getMeasurementId());
    }

    /**
     * @inheritDoc
     */
    protected function getMappingRepository(): MappingRepositoryInterface
    {
        return $this->mappingRepository;
    }

    /**
     * @inheritDoc
     */
    protected function getTwig(): Environment
    {
        throw new \LogicException();
    }
}
