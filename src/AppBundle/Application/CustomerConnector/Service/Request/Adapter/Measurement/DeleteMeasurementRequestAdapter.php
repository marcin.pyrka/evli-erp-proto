<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Service\Request\Adapter\Measurement;

use AppBundle\Application\CustomerConnector\Model\HttpMethod;
use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\Application\CustomerConnector\Service\Request\AbstractRequestAdapter;
use AppBundle\Infrastructure\Persistence\CustomerConnector\MappingRepositoryInterface;
use Twig\Environment;

class DeleteMeasurementRequestAdapter extends AbstractRequestAdapter
{
    use MeasurementHelperTrait;

    /**
     * @inheritDoc
     */
    public function method(): HttpMethod
    {
        return HttpMethod::HTTP_DELETE();
    }

    /**
     * @inheritDoc
     */
    protected function endpoint(): HttpEndpoint
    {
        return HttpEndpoint::DELETE_MEASUREMENT();
    }

    /**
     * @inheritDoc
     */
    public function uri(): string
    {
        return sprintf('measurement/%s', $this->getMeasurementId());
    }

    /**
     * @inheritDoc
     */
    protected function getMappingRepository(): MappingRepositoryInterface
    {
        throw new \LogicException();
    }

    /**
     * @inheritDoc
     */
    protected function getTwig(): Environment
    {
        throw new \LogicException();
    }
}
