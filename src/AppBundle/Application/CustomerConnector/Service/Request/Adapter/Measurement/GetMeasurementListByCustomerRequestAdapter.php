<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Service\Request\Adapter\Measurement;

use AppBundle\Application\CustomerConnector\Model\HttpMethod;
use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\Application\CustomerConnector\Service\Request\AbstractRequestAdapter;
use AppBundle\Application\CustomerConnector\Service\Request\Adapter\Customer\CustomerHelperTrait;
use AppBundle\Infrastructure\Persistence\CustomerConnector\MappingRepositoryInterface;
use Twig\Environment;

class GetMeasurementListByCustomerRequestAdapter extends AbstractRequestAdapter
{
    use CustomerHelperTrait;
    use MeasurementHelperTrait;

    /** @var MappingRepositoryInterface */
    private $mappingRepository;

    /**
     * @param MappingRepositoryInterface $mappingRepository
     */
    public function __construct(MappingRepositoryInterface $mappingRepository)
    {
        $this->mappingRepository = $mappingRepository;
    }

    /**
     * @inheritDoc
     */
    public function method(): HttpMethod
    {
        return HttpMethod::HTTP_GET();
    }

    /**
     * @inheritDoc
     */
    protected function endpoint(): HttpEndpoint
    {
        return HttpEndpoint::GET_MEASUREMENT_LIST_BY_CUSTOMER();
    }

    /**
     * @inheritDoc
     */
    public function modifyResponse(array $response): array
    {
        $res = [
            'meta' => $response['meta'] ?? [],
            'result' => $this->modifyMeasurementList($response['result'] ?? []),
        ];

        return $res;
    }

    /**
     * @inheritDoc
     */
    public function uri(): string
    {
        return sprintf(
            'customer/%s/measurement?%s',
            $this->getCustomerId(),
            http_build_query($this->request->getUrlParams())
        );
    }

    /**
     * @inheritDoc
     */
    protected function getMappingRepository(): MappingRepositoryInterface
    {
        return $this->mappingRepository;
    }

    /**
     * @inheritDoc
     */
    protected function getTwig(): Environment
    {
        throw new \LogicException();
    }
}
