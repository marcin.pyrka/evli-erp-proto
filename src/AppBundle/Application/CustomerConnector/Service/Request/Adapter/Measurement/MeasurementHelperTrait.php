<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Service\Request\Adapter\Measurement;

use AppBundle\Domain\CustomerConnector\Request;
use AppBundle\Infrastructure\Persistence\CustomerConnector\MappingNotFoundException;
use AppBundle\Infrastructure\Persistence\CustomerConnector\MappingRepositoryInterface;
use Assert\Assert;
use Twig\Environment;

trait MeasurementHelperTrait
{
    /**
     * @return Request
     */
    abstract protected function getRequest(): Request;

    /**
     * @return MappingRepositoryInterface
     */
    abstract protected function getMappingRepository(): MappingRepositoryInterface;

    /**
     * @return Environment
     */
    abstract protected function getTwig(): Environment;

    /**
     * @return string
     */
    protected function getMeasurementId(): string
    {
        $noteId = $this->getRequest()->getUrlParams()['measurementId'] ?? '';

        $message = 'measurementId has to be set';
        Assert::that($noteId)->string($message)->notEmpty($message);

        return $noteId;
    }

    /**
     * @param array $measurementList
     *
     * @return array
     */
    protected function modifyMeasurementList(array $measurementList): array
    {
        $results = [];

        foreach ($measurementList as $measurement) {
            $results[] = $this->modifyMeasurement($measurement);
        }

        return $results;
    }

    /**
     * @param array $measurement
     *
     * @return array|string
     * @throws MappingNotFoundException
     */
    protected function modifyMeasurement(array $measurement)
    {
        if (isset($measurement['uuid']) && is_string($measurement['uuid'])) {
            $measurement['measurement_id'] = $measurement['uuid'];
            try {
                $measurement['external_measurement_id'] = $this
                    ->getMappingRepository()
                    ->get($measurement['uuid'])
                    ->getExternalId();
            } catch (MappingNotFoundException $exception) {
                // do nothing
            }
        }

        if (isset($measurement['customer_uuid']) && is_string($measurement['customer_uuid'])) {
            $measurement['customer_id'] = $measurement['customer_uuid'];
            try {
                $measurement['external_customer_id'] = $this
                    ->getMappingRepository()
                    ->get($measurement['customer_uuid'])
                    ->getExternalId();
            } catch (MappingNotFoundException $exception) {
                // do nothing
            }
        }

        unset($measurement['id'], $measurement['uuid'], $measurement['customer_uuid']);

        return ($this->getRequest()->getUrlParams()['format'] ?? null) === 'note'
            ? $this->modifyMeasurementAsNote($measurement)
            : $measurement;
    }

    /**
     * @param array $measurement
     *
     * @return string
     */
    protected function modifyMeasurementAsNote(array $measurement): string
    {
        $measurement['created_at'] = new \DateTimeImmutable($measurement['created_at']);
        $measurement['updated_at'] = new \DateTimeImmutable($measurement['updated_at']);

        $measurement['created_by'] = 'Dominik Müller';
        $measurement['updated_by'] = 'Sandro Imeichen';

        $measurement['right'] = $this->modifyEyeData($measurement['right'] ?? []);
        $measurement['left'] = $this->modifyEyeData($measurement['left'] ?? []);

        return $this->getTwig()->render('@tpl/customer_connector/measurement_as_note.html.twig', [
            'measurement' =>$measurement
        ]);
    }

    /**
     * @param array $eye
     *
     * @return string
     */
    private function modifyEyeData(array $eye): string
    {
        $results = [];
        foreach ($eye as $key => $value) {
            $results[] = sprintf('%s: %s', $key, $value);
        }

        return implode(', ', $results);
    }
}
