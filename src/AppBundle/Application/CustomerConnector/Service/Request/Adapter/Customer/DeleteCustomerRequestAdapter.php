<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Service\Request\Adapter\Customer;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\Application\CustomerConnector\Model\HttpMethod;
use AppBundle\Application\CustomerConnector\Service\Request\AbstractRequestAdapter;

class DeleteCustomerRequestAdapter extends AbstractRequestAdapter
{
    use CustomerHelperTrait;

    /**
     * @inheritDoc
     */
    public function method(): HttpMethod
    {
        return HttpMethod::HTTP_DELETE();
    }

    /**
     * @inheritDoc
     */
    protected function endpoint(): HttpEndpoint
    {
        return HttpEndpoint::DELETE_CUSTOMER();
    }

    /**
     * @inheritDoc
     */
    public function uri(): string
    {
        return sprintf('customer/%s', $this->getCustomerId());
    }
}
