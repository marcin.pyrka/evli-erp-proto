<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Service\Request\Adapter\Customer;

use AppBundle\Domain\CustomerConnector\Request;
use Assert\Assert;

trait CustomerHelperTrait
{
    /**
     * @return Request
     */
    abstract protected function getRequest(): Request;

    /**
     * @param array $customer
     *
     * @return array
     */
    protected function modifyCustomer(array $customer): array
    {
        $customer['id'] = $customer['uuid'] ?? null;
        unset($customer['uuid']);

        return $customer;
    }

    /**
     * @param array $customerList
     *
     * @return array
     */
    protected function modifyCustomerList(array $customerList): array
    {
        $results = [];

        foreach ($customerList as $customer) {
            $results[] = $this->modifyCustomer($customer);
        }

        return $results;
    }

    /**
     * @return string
     */
    protected function getCustomerId(): string
    {
        $customerId = $this->getRequest()->getUrlParams()['customerId'] ?? '';

        $message = 'customerId has to be set';
        Assert::that($customerId)->string($message)->notEmpty($message);

        return $customerId;
    }
}
