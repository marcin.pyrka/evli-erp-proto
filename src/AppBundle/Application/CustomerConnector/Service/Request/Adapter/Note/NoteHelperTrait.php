<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Service\Request\Adapter\Note;

use AppBundle\Domain\CustomerConnector\Request;
use AppBundle\Infrastructure\Persistence\CustomerConnector\MappingNotFoundException;
use AppBundle\Infrastructure\Persistence\CustomerConnector\MappingRepositoryInterface;
use Assert\Assert;

trait NoteHelperTrait
{
    /**
     * @return Request
     */
    abstract protected function getRequest(): Request;

    /**
     * @return MappingRepositoryInterface
     */
    abstract protected function getMappingRepository(): MappingRepositoryInterface;

    /**
     * @param array $note
     *
     * @return array
     */
    protected function modifyNote(array $note): array
    {
        $note['id'] = $note['uuid'] ?? null;
        $note['customer_id'] = $note['customer_uuid'] ?? null;

        if (is_string($note['customer_id'])) {
            try {
                $note['external_customer_id'] = $this
                    ->getMappingRepository()
                    ->get($note['customer_id'])
                    ->getExternalId();
            } catch (MappingNotFoundException $exception) {
                // do nothing
            }
        }

        unset($note['uuid']);
        unset($note['customer_uuid']);

        return $note;
    }

    /**
     * @param array $noteList
     *
     * @return array
     */
    protected function modifyNoteList(array $noteList): array
    {
        $results = [];

        foreach ($noteList as $note) {
            $results[] = $this->modifyNote($note);
        }

        return $results;
    }

    /**
     * @return string
     */
    protected function getNoteId(): string
    {
        $noteId = $this->getRequest()->getUrlParams()['noteId'] ?? '';

        $message = 'noteId has to be set';
        Assert::that($noteId)->string($message)->notEmpty($message);

        return $noteId;
    }
}
