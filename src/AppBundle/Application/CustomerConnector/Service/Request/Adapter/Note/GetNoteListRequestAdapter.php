<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Service\Request\Adapter\Note;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\Application\CustomerConnector\Model\HttpMethod;
use AppBundle\Application\CustomerConnector\Service\Request\AbstractRequestAdapter;
use AppBundle\Application\SharedKernel\Service\Client\ClientServiceAwareTrait;
use AppBundle\Application\SharedKernel\Service\Client\ClientServiceInterface;
use AppBundle\Domain\CustomerConnector\MappingType;
use AppBundle\Infrastructure\Persistence\CustomerConnector\MappingRepositoryInterface;

class GetNoteListRequestAdapter extends AbstractRequestAdapter
{
    use NoteHelperTrait;
    use ClientServiceAwareTrait;

    /** @var MappingRepositoryInterface */
    private $repository;

    /**
     * @param MappingRepositoryInterface $repository
     * @param ClientServiceInterface $clientService
     */
    public function __construct(MappingRepositoryInterface $repository, ClientServiceInterface $clientService)
    {
        $this->repository = $repository;
        $this->clientService = $clientService;
    }

    /**
     * @inheritDoc
     */
    public function method(): HttpMethod
    {
        return HttpMethod::HTTP_GET();
    }

    /**
     * @inheritDoc
     */
    protected function endpoint(): HttpEndpoint
    {
        return HttpEndpoint::GET_NOTE_LIST();
    }

    /**
     * @inheritDoc
     */
    public function uri(): string
    {
        $params = $this->request->getUrlParams();

        if (isset($params['external_id']) && is_string($params['external_id'])) {
            $params['uuid'] = $this->repository->getByExternalId(
                $this->clientService->get(),
                MappingType::NOTE(),
                $params['external_id']
            )->getId();

            unset($params['external_id']);
        }

        return 'note?' . http_build_query($params);
    }

    /**
     * @inheritDoc
     */
    public function modifyResponse(array $response): array
    {
        return [
            'meta' => $response['meta'] ?? [],
            'result' => $this->modifyNoteList($response['result'] ?? []),
        ];
    }

    /**
     * @inheritDoc
     */
    protected function getMappingRepository(): MappingRepositoryInterface
    {
        return $this->repository;
    }
}
