<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Service\Request\Adapter\Note;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\Application\CustomerConnector\Model\HttpMethod;
use AppBundle\Application\CustomerConnector\Service\Request\AbstractRequestAdapter;
use AppBundle\Infrastructure\Persistence\CustomerConnector\MappingRepositoryInterface;

class DeleteNoteRequestAdapter extends AbstractRequestAdapter
{
    use NoteHelperTrait;

    /** @var MappingRepositoryInterface */
    private $repository;

    /**
     * @param MappingRepositoryInterface $repository
     */
    public function __construct(MappingRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @inheritDoc
     */
    public function method(): HttpMethod
    {
        return HttpMethod::HTTP_DELETE();
    }

    /**
     * @inheritDoc
     */
    protected function endpoint(): HttpEndpoint
    {
        return HttpEndpoint::DELETE_NOTE();
    }

    /**
     * @inheritDoc
     */
    public function uri(): string
    {
        return sprintf('note/%s', $this->getNoteId());
    }

    /**
     * @inheritDoc
     */
    protected function getMappingRepository(): MappingRepositoryInterface
    {
        return $this->repository;
    }
}
