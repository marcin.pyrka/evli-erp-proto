<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Service\Request\Adapter\Note;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\Application\CustomerConnector\Model\HttpMethod;
use AppBundle\Application\CustomerConnector\Service\Request\AbstractRequestAdapter;
use AppBundle\Infrastructure\Persistence\CustomerConnector\MappingRepositoryInterface;

class UpdateNoteRequestAdapter extends AbstractRequestAdapter
{
    use NoteHelperTrait;

    /** @var MappingRepositoryInterface */
    private $mappingRepository;

    /**
     * @param MappingRepositoryInterface $mappingRepository
     */
    public function __construct(MappingRepositoryInterface $mappingRepository)
    {
        $this->mappingRepository = $mappingRepository;
    }

    /**
     * @inheritDoc
     */
    public function method(): HttpMethod
    {
        return HttpMethod::HTTP_PATCH();
    }

    /**
     * @inheritDoc
     */
    protected function endpoint(): HttpEndpoint
    {
        return HttpEndpoint::UPDATE_NOTE();
    }

    /**
     * @inheritDoc
     */
    public function uri(): string
    {
        return sprintf('note/%s', $this->getNoteId());
    }

    /**
     * @inheritDoc
     */
    protected function getMappingRepository(): MappingRepositoryInterface
    {
        return $this->mappingRepository;
    }
}
