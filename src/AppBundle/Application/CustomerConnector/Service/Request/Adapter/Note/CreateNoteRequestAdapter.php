<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Service\Request\Adapter\Note;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\Application\CustomerConnector\Model\HttpMethod;
use AppBundle\Application\CustomerConnector\Service\IdParser\IdParserInterface;
use AppBundle\Application\CustomerConnector\Service\Request\AbstractRequestAdapter;
use AppBundle\Application\CustomerConnector\Service\Request\Adapter\AdapterHelperTrait;
use AppBundle\Application\SharedKernel\Service\Client\ClientServiceInterface;
use AppBundle\Domain\CustomerConnector\MappingType;
use AppBundle\Infrastructure\Persistence\CustomerConnector\MappingRepositoryInterface;

class CreateNoteRequestAdapter extends AbstractRequestAdapter
{
    use AdapterHelperTrait;

    /** @var MappingRepositoryInterface */
    private $mappingRepository;

    /** @var IdParserInterface */
    private $idParser;

    /** @var ClientServiceInterface */
    private $clientService;

    /**
     * @param MappingRepositoryInterface $mappingRepository
     * @param IdParserInterface $idParser
     * @param ClientServiceInterface $clientService
     */
    public function __construct(
        MappingRepositoryInterface $mappingRepository,
        IdParserInterface $idParser,
        ClientServiceInterface $clientService
    ) {
        $this->mappingRepository = $mappingRepository;
        $this->idParser = $idParser;
        $this->clientService = $clientService;
    }

    /**
     * @inheritDoc
     */
    public function method(): HttpMethod
    {
        return HttpMethod::HTTP_POST();
    }

    /**
     * @inheritDoc
     */
    protected function endpoint(): HttpEndpoint
    {
        return HttpEndpoint::CREATE_NOTE();
    }

    /**
     * @inheritDoc
     */
    public function data(): array
    {
        $data = $this->request->getRequestData();
        $data['uuid'] = $this->getMappedId(MappingType::NOTE(), $data);

        unset($data['external_id']);

        return $data;
    }

    /**
     * @inheritDoc
     */
    public function uri(): string
    {
        return 'note';
    }

    /**
     * @inheritDoc
     */
    protected function getIdParser(): IdParserInterface
    {
        return $this->idParser;
    }

    /**
     * @inheritDoc
     */
    protected function getClientService(): ClientServiceInterface
    {
        return $this->clientService;
    }

    /**
     * @inheritDoc
     */
    protected function getMappingRepository(): MappingRepositoryInterface
    {
        return $this->mappingRepository;
    }
}
