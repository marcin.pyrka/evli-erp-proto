<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Service\Request\Adapter\Note;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\Application\CustomerConnector\Model\HttpMethod;
use AppBundle\Application\CustomerConnector\Service\Request\AbstractRequestAdapter;
use AppBundle\Infrastructure\Persistence\CustomerConnector\MappingRepositoryInterface;

class GetNoteRequestAdapter extends AbstractRequestAdapter
{
    use NoteHelperTrait;

    /** @var MappingRepositoryInterface */
    private $repository;

    /**
     * @param MappingRepositoryInterface $repository
     */
    public function __construct(MappingRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @inheritDoc
     */
    public function method(): HttpMethod
    {
        return HttpMethod::HTTP_GET();
    }

    /**
     * @inheritDoc
     */
    protected function endpoint(): HttpEndpoint
    {
        return HttpEndpoint::GET_NOTE();
    }

    /**
     * @inheritDoc
     */
    public function uri(): string
    {
        return sprintf('note/%s', $this->getNoteId());
    }

    /**
     * @inheritDoc
     */
    public function modifyResponse(array $response): array
    {
        return [
            'meta' => $response['meta'] ?? [],
            'result' => $this->modifyNote($response['result'] ?? []),
        ];
    }

    /**
     * @inheritDoc
     */
    protected function getMappingRepository(): MappingRepositoryInterface
    {
        return $this->repository;
    }
}
