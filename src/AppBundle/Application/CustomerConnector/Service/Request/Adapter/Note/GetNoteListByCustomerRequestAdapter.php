<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Service\Request\Adapter\Note;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\Application\CustomerConnector\Model\HttpMethod;
use AppBundle\Application\CustomerConnector\Service\Request\AbstractRequestAdapter;
use AppBundle\Application\CustomerConnector\Service\Request\Adapter\Customer\CustomerHelperTrait;
use AppBundle\Application\SharedKernel\Service\Client\ClientServiceAwareTrait;
use AppBundle\Infrastructure\Persistence\CustomerConnector\MappingRepositoryInterface;

class GetNoteListByCustomerRequestAdapter extends AbstractRequestAdapter
{
    use NoteHelperTrait;
    use CustomerHelperTrait;
    use ClientServiceAwareTrait;

    /** @var MappingRepositoryInterface */
    private $repository;

    /**
     * @param MappingRepositoryInterface $repository
     */
    public function __construct(MappingRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @inheritDoc
     */
    public function method(): HttpMethod
    {
        return HttpMethod::HTTP_GET();
    }

    /**
     * @inheritDoc
     */
    protected function endpoint(): HttpEndpoint
    {
        return HttpEndpoint::GET_NOTE_LIST_BY_CUSTOMER();
    }

    /**
     * @inheritDoc
     */
    public function uri(): string
    {
        return sprintf(
            'customer/%s/note?%s',
            $this->getCustomerId(),
            http_build_query($this->request->getUrlParams())
        );
    }

    /**
     * @inheritDoc
     */
    public function modifyResponse(array $response): array
    {
        return [
            'meta' => $response['meta'] ?? [],
            'result' => $this->modifyNoteList($response['result'] ?? []),
        ];
    }

    /**
     * @inheritDoc
     */
    protected function getMappingRepository(): MappingRepositoryInterface
    {
        return $this->repository;
    }
}
