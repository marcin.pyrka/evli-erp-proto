<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Service\Request\Adapter;

use AppBundle\Application\CustomerConnector\Service\IdParser\IdParserInterface;
use AppBundle\Application\SharedKernel\Service\Client\ClientServiceInterface;
use AppBundle\Domain\CustomerConnector\Mapping;
use AppBundle\Domain\CustomerConnector\MappingType;
use AppBundle\Infrastructure\Persistence\CustomerConnector\MappingNotFoundException;
use AppBundle\Infrastructure\Persistence\CustomerConnector\MappingRepositoryInterface;
use Ramsey\Uuid\Uuid;

trait AdapterHelperTrait
{
    /**
     * @return IdParserInterface
     */
    abstract protected function getIdParser(): IdParserInterface;

    /**
     * @return ClientServiceInterface
     */
    abstract protected function getClientService(): ClientServiceInterface;

    /**
     * @return MappingRepositoryInterface
     */
    abstract protected function getMappingRepository(): MappingRepositoryInterface;

    /**
     * @param MappingType $type
     * @param array $data
     *
     * @return string
     */
    protected function getMappedId(MappingType $type, array $data): string
    {
        $externalId = $this->getIdParser()->parse($data);
        $client = $this->getClientService()->get();

        try {
            $mapping = $this->getMappingRepository()->getByExternalId($client, $type, $externalId);
        } catch (MappingNotFoundException $exception) {
            $mapping = Mapping::create(Uuid::uuid1(), $externalId, $client, $type);
            $this->getMappingRepository()->store($mapping);
        }

        return $mapping->getId();
    }
}
