<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Service\Request;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\Domain\CustomerConnector\Request;
use AppBundle\Infrastructure\Helper\Json;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\RequestOptions;

class RequestService
{
    /** @var Client */
    private $client;

    /** @var AbstractRequestAdapter[] */
    private $adapters = [];

    /** @var array */
    private $data = [];

    /** @var AbstractRequestAdapter */
    private $adapter;

    /** @var Response */
    private $response;

    /** @var string */
    private $contents;

    /** @var bool */
    private $exception;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param Request $request
     */
    public function handle(Request $request): void
    {
        $this->setUp();

        $this->adapter = $this->getAdapter($request->getEndpoint());
        $this->adapter->setRequest($request);

        $this->call();
    }

    /**
     *
     */
    private function setUp(): void
    {
        $this->data = [];
        $this->adapter = null;
        $this->response = null;
        $this->contents = null;
        $this->exception = false;
    }

    /**
     * @param HttpEndpoint $endpoint
     *
     * @return AbstractRequestAdapter
     */
    private function getAdapter(HttpEndpoint $endpoint): AbstractRequestAdapter
    {
        $adapters = array_filter($this->adapters, function (AbstractRequestAdapter $adapter) use ($endpoint) {
            return $adapter->supports($endpoint);
        });

        if (count($adapters) !== 1) {
            throw new \LogicException();
        }

        return reset($adapters);
    }

    /**
     *
     */
    private function call(): void
    {
        try {
            $this->response = $this->client->request(
                $this->adapter->method()->getValue(),
                $this->adapter->uri(),
                [RequestOptions::JSON => $this->data = $this->adapter->data()]
            );
        } catch (ClientException|ServerException $exception) {
            $this->exception = true;
            $this->response = $exception->getResponse();
        }

        $this->contents = $this->response->getBody()->getContents();
    }

    /**
     * @return bool
     */
    public function hasError(): bool
    {
        return $this->exception;
    }

    /**
     * @return array
     */
    public function getResponse(): array
    {
        return $this->adapter->modifyResponse(Json::decode($this->contents));
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->response->getStatusCode();
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param AbstractRequestAdapter $requestAdapter
     */
    public function addRequestAdapter(AbstractRequestAdapter $requestAdapter): void
    {
        $this->adapters[] = $requestAdapter;
    }
}
