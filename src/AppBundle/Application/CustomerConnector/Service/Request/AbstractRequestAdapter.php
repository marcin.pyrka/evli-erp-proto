<?php declare(strict_types=1);

namespace AppBundle\Application\CustomerConnector\Service\Request;

use AppBundle\Application\CustomerConnector\Model\HttpEndpoint;
use AppBundle\Application\CustomerConnector\Model\HttpMethod;
use AppBundle\Domain\CustomerConnector\Request;

abstract class AbstractRequestAdapter
{
    /** @var Request */
    protected $request;

    /**
     * @return string
     */
    abstract public function uri(): string;

    /**
     * @return HttpMethod
     */
    abstract public function method(): HttpMethod;

    /**
     * @return HttpEndpoint
     */
    abstract protected function endpoint(): HttpEndpoint;

    /**
     * @param Request $request
     */
    public function setRequest(Request $request): void
    {
        $this->request = $request;
    }

    /**
     * @param HttpEndpoint $endpoint
     *
     * @return bool
     */
    public function supports(HttpEndpoint $endpoint): bool
    {
        return $this->endpoint()->is($endpoint);
    }

    /**
     * @return array
     */
    public function data(): array
    {
        return $this->request->getRequestData();
    }

    /**
     * @param array $response
     *
     * @return array
     */
    public function modifyResponse(array $response): array
    {
        return $response;
    }

    /**
     * @inheritDoc
     */
    protected function getRequest(): Request
    {
        return $this->request;
    }
}
