<?php

declare(strict_types=1);

namespace AppBundle\Generator;

interface CodeGeneratorInterface
{
    public function generate(string $value): string;
}
