<?php

declare(strict_types=1);

namespace AppBundle\Generator;

use AppBundle\Entity\ProductInterface;

final class ProductNameGenerator implements ProductNameGeneratorInterface
{
    public function generate(ProductInterface $product): string
    {
        $group = $product->getGroup();

        if (null === $group || null === $family = $group->getFamily()) {
            return '';
        }

        return $family->getName() . ' ' . $group->getName() . ' ' . $product->getQuantity() . ' ' . $product->getUnit();
    }
}
