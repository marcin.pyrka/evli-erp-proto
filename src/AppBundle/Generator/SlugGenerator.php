<?php

declare(strict_types=1);

namespace AppBundle\Generator;

use AppBundle\Doctrine\ORM\Repository\ProductRepositoryInterface;
use AppBundle\Doctrine\ORM\Repository\TaxonRepositoryInterface;
use Behat\Transliterator\Transliterator;
use Sylius\Component\Resource\Repository\RepositoryInterface;

final class SlugGenerator implements SlugGeneratorInterface
{
    /** @var ProductRepositoryInterface|TaxonRepositoryInterface */
    private $repository;

    /** @var int */
    private $suffix = 0;

    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function generate(string $name): string
    {
        $name = strtolower(str_replace('\'', '-', $name));
        $slug = Transliterator::transliterate($name);
        $product = $this->repository->findOneBySlugOnly($slug);

        if (null !== $product && null !== $product->getId()) {
            ++$this->suffix;

            return $this->generate($slug . '-' . $this->suffix);
        }

        return $slug;
    }
}
