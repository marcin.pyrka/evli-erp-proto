<?php

declare(strict_types=1);

namespace AppBundle\Generator;

use Behat\Transliterator\Transliterator;

final class CodeGenerator implements CodeGeneratorInterface
{
    public function generate(string $value): string
    {
        return str_replace('-', '_', Transliterator::transliterate($value));
    }
}
