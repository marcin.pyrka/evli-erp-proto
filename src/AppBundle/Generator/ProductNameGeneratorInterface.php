<?php

declare(strict_types=1);

namespace AppBundle\Generator;

use AppBundle\Entity\ProductInterface;

interface ProductNameGeneratorInterface
{
    public function generate(ProductInterface $product): string;
}
