<?php

declare(strict_types=1);

namespace AppBundle\Generator;

interface SlugGeneratorInterface
{
    public function generate(string $name): string;
}
