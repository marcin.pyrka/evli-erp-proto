<?php

declare(strict_types=1);

namespace AppBundle\Doctrine\DBAL\Query;

use AppBundle\Exception\OrderProductOptionValueNotFoundException;
use AppBundle\Query\FindFulFillerProductOptionValueQueryInterface;
use Doctrine\DBAL\Connection;

final class FindFulFillerProductOptionValueView implements FindFulFillerProductOptionValueQueryInterface
{
    /** @var Connection */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function __invoke(string $code, string $optionName, string $optionValue): array
    {
        $query = <<<SQL
SELECT
  fulfiller_raw_product.external_id AS external_product_id,
  fulfiller_raw_product.title AS product_title,
  fulfiller_raw_option.title AS fulfiller_option,
  fulfiller_raw_option_value.value AS fulfiller_option_value
FROM 
  sylius_product
LEFT JOIN app_product_option_values
  ON (app_product_option_values.product_id = sylius_product.id)
LEFT JOIN sylius_product_option_value
  ON (sylius_product_option_value.id = app_product_option_values.option_value_id)
LEFT JOIN sylius_product_option
  ON (sylius_product_option.id = sylius_product_option_value.option_id)
LEFT JOIN fulfiller_to_elvi_option_value_map 
  ON (fulfiller_to_elvi_option_value_map.elvi_product_id = sylius_product.id
   AND fulfiller_to_elvi_option_value_map.elvi_option_id = sylius_product_option.id
   AND fulfiller_to_elvi_option_value_map.elvi_option_value_id = sylius_product_option_value.id)
LEFT JOIN fulfiller_raw_product
  ON fulfiller_raw_product.id = fulfiller_to_elvi_option_value_map.fulfiller_product_id
LEFT JOIN fulfiller_raw_option
  ON fulfiller_raw_option.id = fulfiller_to_elvi_option_value_map.fulfiller_option_id
LEFT JOIN fulfiller_raw_option_value
  ON (fulfiller_raw_option_value.id = fulfiller_to_elvi_option_value_map.fulfiller_option_value_id
   AND fulfiller_raw_option_value.option_id = fulfiller_to_elvi_option_value_map.fulfiller_option_id)
WHERE 1
  AND sylius_product.code = :productCode
  AND sylius_product_option.ident = :optionName
  AND sylius_product_option_value.raw_value = :optionValue  
SQL;
        $results = $this->connection->fetchAssoc(
            $query,
            [
                'productCode' => $code,
                'optionName'  => $optionName,
                'optionValue' => $optionValue,
            ]
        );

        if (false === $results) {
            throw OrderProductOptionValueNotFoundException::create($code, $optionName, $optionValue);
        }

        return $results;
    }
}
