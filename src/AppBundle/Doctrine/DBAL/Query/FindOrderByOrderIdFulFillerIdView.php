<?php

declare(strict_types=1);

namespace AppBundle\Doctrine\DBAL\Query;

use AppBundle\Exception\OrderNotFoundException;
use AppBundle\Query\FindOrderByOrderIdFulFillerIdQueryInterface;
use AppBundle\Query\Model\Order;
use Doctrine\DBAL\Connection;

final class FindOrderByOrderIdFulFillerIdView implements FindOrderByOrderIdFulFillerIdQueryInterface
{
    /** @var Connection */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function __invoke(string $orderUuid, string $fulFillerId): Order
    {
        $result = $this->connection->fetchAssoc(
            'SELECT id, app_id, fulfiller_order_id FROM erp_order WHERE uuid = :orderUuid AND fulfiller_id=:fulFillerId',
            [
                'orderUuid'   => $orderUuid,
                'fulFillerId' => $fulFillerId,
            ]
        );

        if (!$result) {
            throw OrderNotFoundException::withUuidFulFillerId($orderUuid, $fulFillerId);
        }

        return new Order($result['id'], $result['app_id'], $result['fulfiller_order_id']);
    }
}
