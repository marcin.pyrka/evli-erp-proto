<?php

declare(strict_types=1);

namespace AppBundle\Doctrine\ORM\Repository;

use AppBundle\Entity\FulFiller\Fulfiller;

interface FulFillerRepositoryInterface
{
    public function save(Fulfiller $fulFiller): void;

    public function findOneById(string $id): ?Fulfiller;
}
