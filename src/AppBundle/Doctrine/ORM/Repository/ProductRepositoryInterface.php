<?php

declare(strict_types=1);

namespace AppBundle\Doctrine\ORM\Repository;

use AppBundle\Entity\ProductInterface;
use Sylius\Component\Product\Repository\ProductRepositoryInterface as BaseProductRepositoryInterface;

interface ProductRepositoryInterface extends BaseProductRepositoryInterface
{
    public function findOneBySlugOnly(string $slug): ?ProductInterface;
}
