<?php

declare(strict_types=1);

namespace AppBundle\Doctrine\ORM\Repository;

use AppBundle\Entity\Fulfiller\Fulfiller;
use Doctrine\ORM\EntityManagerInterface;

final class FulFillerRepository implements FulFillerRepositoryInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function findOneById(string $id): ?Fulfiller
    {
        $entity = $this->entityManager->getRepository(Fulfiller::class)->find($id);

        return $entity;
    }

    public function save(Fulfiller $fulFiller): void
    {
        $this->entityManager->persist($fulFiller);
        $this->entityManager->flush();
    }
}
