<?php

declare(strict_types=1);

namespace AppBundle\Doctrine\ORM\Repository;

use Sylius\Component\Taxonomy\Model\TaxonInterface;

interface TaxonRepositoryInterface
{
    public function findOneBySlugOnly(string $slug): ?TaxonInterface;
}
