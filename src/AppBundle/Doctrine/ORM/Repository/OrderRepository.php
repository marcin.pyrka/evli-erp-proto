<?php

declare(strict_types=1);

namespace AppBundle\Doctrine\ORM\Repository;

use AppBundle\Entity\Order;
use AppBundle\Entity\ValueObject\OrderUuid;
use Doctrine\ORM\EntityManagerInterface;

final class OrderRepository implements OrderRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function save(Order $order): void
    {
        $this->entityManager->persist($order);
        $this->entityManager->flush();
    }

    public function findOneByUuid(OrderUuid $uuid): ?Order
    {
        $order = $this->entityManager->getRepository(Order::class)->findOneBy(['orderUuid.value' => (string) $uuid]);

        return $order;
    }
}
