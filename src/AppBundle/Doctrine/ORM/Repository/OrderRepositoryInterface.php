<?php

declare(strict_types=1);

namespace AppBundle\Doctrine\ORM\Repository;

use AppBundle\Entity\Order;
use AppBundle\Entity\ValueObject\OrderUuid;

interface OrderRepositoryInterface
{
    public function save(Order $order): void;

    public function findOneByUuid(OrderUuid $uuid): ?Order;
}
