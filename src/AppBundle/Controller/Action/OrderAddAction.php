<?php

declare(strict_types=1);

namespace AppBundle\Controller\Action;

use AppBundle\Command\AddOrderCommand;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandlerInterface;
use League\Tactician\CommandBus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Webmozart\Assert\Assert;

final class OrderAddAction
{
    /** @var CommandBus */
    private $commandBus;

    /** @var ViewHandlerInterface */
    private $viewHandler;

    public function __construct(CommandBus $commandBus, ViewHandlerInterface $viewHandler)
    {
        $this->commandBus = $commandBus;
        $this->viewHandler = $viewHandler;
    }

    public function __invoke(Request $request): Response
    {
        Assert::eq(Request::METHOD_POST, $request->getMethod());

        $content = json_decode($request->getContent(), true);

        Assert::notEmpty($content);
        Assert::notEmpty($content['app_id']);
        Assert::notEmpty($content['order_uuid']);
        Assert::notEmpty($content['fulfiller_id']);
        Assert::notEmpty($content['checkout_completed_at']);

        try {
            $command = AddOrderCommand::create(
                $content['app_id'],
                $content['order_uuid'],
                $content['fulfiller_id'],
                $content['checkout_completed_at']
            );
            $this->commandBus->handle($command);
        } catch (\DomainException $exception) {
            return $this->viewHandler->handle(View::create(['error' => $exception->getMessage()], Response::HTTP_BAD_REQUEST));
        }

        return $this->viewHandler->handle(View::create(['message' => 'created'], Response::HTTP_CREATED));
    }
}
