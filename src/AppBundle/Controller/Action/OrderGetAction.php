<?php

declare(strict_types=1);

namespace AppBundle\Controller\Action;

use AppBundle\Transformer\AppOrderToFulFillerOrderTransformerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Webmozart\Assert\Assert;

final class OrderGetAction
{
    /** @var AppOrderToFulFillerOrderTransformerInterface */
    private $orderTransformer;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(AppOrderToFulFillerOrderTransformerInterface $orderTransformer, LoggerInterface $logger)
    {
        $this->orderTransformer = $orderTransformer;
        $this->logger = $logger;
    }

    public function __invoke(Request $request): JsonResponse
    {
        $orderId = $request->attributes->get('orderId');
        $fulFillerId = $request->attributes->get('fulFillerId');

        Assert::notEmpty($orderId);
        Assert::notEmpty($fulFillerId);

        $data = $this->orderTransformer->__invoke($orderId, $fulFillerId);

        return new JsonResponse(
            [
                'result' => $data,
                'meta'   => [
                    'status'  => Response::HTTP_OK,
                    'message' => 'Success',
                ],
            ]
        );
    }
}
