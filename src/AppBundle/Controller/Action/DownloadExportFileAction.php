<?php

declare(strict_types=1);

namespace AppBundle\Controller\Action;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

final class DownloadExportFileAction
{
    /** @var string */
    private $csvFilesDirectory;

    public function __construct(string $csvFilesDirectory)
    {
        $this->csvFilesDirectory = $csvFilesDirectory;
    }

    public function __invoke(Request $request): BinaryFileResponse
    {
        $resourceCode = $request->get('resourceCode');
        $fileName = $resourceCode . '.csv';
        $response = new BinaryFileResponse($this->csvFilesDirectory . DIRECTORY_SEPARATOR . $fileName);

        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        $response->headers->set('Content-Type', 'text/csv');

        return $response;
    }
}
