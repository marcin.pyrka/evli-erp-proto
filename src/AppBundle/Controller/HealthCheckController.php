<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class HealthCheckController
{
    public function __construct()
    {
    }

    public function __invoke(Request $request): Response
    {
    }
}
