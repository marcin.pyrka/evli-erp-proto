<?php

declare(strict_types=1);

namespace AppBundle\Service;

interface ElviAppHttpClientInterface
{
    public function get(string $path): string;
}
