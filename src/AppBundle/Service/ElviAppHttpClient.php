<?php

declare(strict_types=1);

namespace AppBundle\Service;

use GuzzleHttp\ClientInterface;

final class ElviAppHttpClient implements ElviAppHttpClientInterface
{
    private const REQUEST_URI_PREFIX = 'internal';

    /** @var ClientInterface */
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function get(string $path): string
    {
        $response = $this->client->request('GET', sprintf('%s/%s', self::REQUEST_URI_PREFIX, $path));
        $content = $response->getBody()->getContents();

        return $content;
    }
}
