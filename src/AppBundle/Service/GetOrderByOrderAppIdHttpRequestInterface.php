<?php

declare(strict_types=1);

namespace AppBundle\Service;

interface GetOrderByOrderAppIdHttpRequestInterface
{
    public function __invoke(string $orderAppId): array;
}
