<?php

declare(strict_types=1);

namespace AppBundle\Service;

final class GetOrderByOrderOrderAppIdHttpRequest implements GetOrderByOrderAppIdHttpRequestInterface
{
    /** @var ElviAppHttpClientInterface */
    private $client;

    public function __construct(ElviAppHttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function __invoke(string $orderAppId): array
    {
        $response = json_decode($this->client->get(sprintf('order/%s', $orderAppId)), true);
        if (! $response || ! isset($response['result'])) {
            throw new \InvalidArgumentException(sprintf('Order not found by id %s!', $orderAppId));
        }

        $order = $response['result'];

        return $order;
    }
}
