imports:
    - { resource: parameters.yml }
    - { resource: security.yml }

    - { resource: '@AppBundle/Resources/config/config.yml' }
    - { resource: '@AppBundle/Infrastructure/Resources/config/config.yml' }

parameters:
    locale: en
    available_locales: [en, de]
    elvi_import_export.export_files_directory: "%kernel.project_dir%/var/exported"
    elvi_import_export.enable_resource_producer_event: false
    elvi_import_export.resource_producer_event_models: []
    elvi_import_export.resource_producer_service: "old_sound_rabbit_mq.resource_producer"

framework:
    secret: '%secret%'
    router:
        resource: '%kernel.project_dir%/app/config/routing.yml'
        strict_requirements: ~
    form: ~
    csrf_protection: ~
    validation: { enable_annotations: true }
    default_locale: '%locale%'
    trusted_hosts: ~
    session:
        handler_id: session.handler.native_file
        save_path: '%kernel.project_dir%/var/sessions/%kernel.environment%'
    fragments: ~
    http_method_override: true
    assets: ~
    php_errors:
        log: true
    templating:
        engines: ['twig']

twig:
    debug: '%kernel.debug%'
    strict_variables: '%kernel.debug%'
    paths:
        '%kernel.project_dir%/src/AppBundle/UI/Http/Resources/templates': tpl

doctrine:
    dbal:
        driver: pdo_mysql
        host: '%database_host%'
        port: '%database_port%'
        dbname: '%database_name%'
        user: '%database_user%'
        password: '%database_password%'
        charset: UTF8

    orm:
        auto_generate_proxy_classes: '%kernel.debug%'
        naming_strategy: doctrine.orm.naming_strategy.underscore
        auto_mapping: true
        mappings:
            Main:
                prefix: Main
                type: xml
                dir: '%kernel.project_dir%/src/AppBundle/Resources/config/doctrine'
            Domain:
                prefix: "AppBundle\\Domain"
                type: xml
                dir: '%kernel.project_dir%/src/AppBundle/Infrastructure/Resources/config/doctrine'
                is_bundle: false

swiftmailer:
    transport: '%mailer_transport%'
    host: '%mailer_host%'
    username: '%mailer_user%'
    password: '%mailer_password%'
    spool: { type: memory }

sensio_framework_extra:
   router:
        annotations: false

stof_doctrine_extensions:
    orm:
        default:
            sluggable: true
            timestampable: true

fos_rest:
    view:
        formats:
            json: true
        empty_content: 204
    format_listener:
        rules:
            - { path: '^/', priorities: ['json'], fallback_format: json, prefer_extension: true }

jms_serializer:
    metadata:
        directories:
            app:
                namespace_prefix: "AppBundle"
                path: "%kernel.project_dir%/src/AppBundle/Resources/config/serializer"

sentry:
    dsn: '%env(SENTRY_DSN)%'
    client: Persist\LoggerBundle\Logger\SymfonySentryLogger
    options:
        curl_method: async
    skip_capture:
        - 'Symfony\Component\HttpKernel\Exception\HttpExceptionInterface'

old_sound_rabbit_mq:
    connections:
        default:
            url: '%env(resolve:RABBITMQ_URL)%'
    producers:
        resource:
            connection: default
            exchange_options:
                name: 'resource'
                type: direct
    consumers:
        resource:
            connection: default
            exchange_options:
                name: 'resource'
                type: direct
            queue_options:
                name: 'resource'
            callback: elvi_import_export.consumer.resource
            enable_logger: true

# Library documentation: http://tactician.thephpleague.com/
# Bundle documentation: https://github.com/thephpleague/tactician-bundle/blob/v1.0/README.md
tactician:
    method_inflector: tactician.handler.method_name_inflector.invoke
    default_bus: command

    commandbus:
        command:
            middleware:
                - tactician.middleware.command_handler
        query:
            middleware:
                - tactician.middleware.command_handler
