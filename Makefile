fix-code:
	php bin/ecs check src/ --fix --config easy-coding-standard.yml

server-start:
	bin/console server:run -d web

fixture-load:
	bin/console s:f:load -n

fixture-load-test:
	bin/console s:f:load fulfiller_product -n --env=test

clear-scheme-test:
	bin/console d:s:d -n --force --env=test
	bin/console d:s:c -n --env=test

cache-clear:
	bin/console c:clear

cache-rm:
	rm -rf var/cache/*
